UI_HOME=/home/dtduong/battle-fi-tour-management
UI_HOST_PORT=8444

build:
	sudo docker build -f docker/Dockerfile-dev -t battle-fi-tour-management/dev .

install:
	cd $(UI_HOME)/src && sudo npm install --force

run:
	sudo docker run -it \
		-v $(UI_HOME)/src/:/opt/www/ \
		-p $(UI_HOST_PORT):3000 \
		battle-fi-tour-management/dev \
		npm run dev

