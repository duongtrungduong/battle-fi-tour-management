import React, {Component} from 'react';
import UserService from 'app-services/userServices'
class View extends Component {
    componentWillMount() {
        this.props.action.load()
    }

    render() {
        var _this = this;
        return (
            <div className="page-wrapper">
                <div className="page-content" style={{backgroundColor: "#fff", height:"100vh"}}>
                       {this.props.children}
                </div>
            </div>
        )
    }
}

export default View;
