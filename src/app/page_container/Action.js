import UserService from 'app-services/userServices'

class Action {
    constructor(dispatcher, id='page-container') {
        this.dispatcher = dispatcher;
        this.id = id;
    }
    setMenu(menu) {
        var _this = this
    	this.dispatcher.dispatch({
    		type:    _this.id + '/set-menu',
    		menuSelected:    menu
    	})
    }
    
    setCategory(categorySelected) {
    	this.dispatcher.dispatch({
    		type:    this.id + '/set-category',
    		categorySelected:    categorySelected
    	})
    }
    setApplicationTitle(title) {
        this.dispatcher.dispatch({
            type:    this.id + '/set-application-title',
            title:   title
        })
    }
    load() {
        

    }
    clickProfile() {
        this.dispatcher.dispatch({
            type:    this.id + '/profile-click'
        })
    }
    logout() {
        var _this = this
        UserService.logOut("")
        window.location = "/login"

    }
}

export default Action;
