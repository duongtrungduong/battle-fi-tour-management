import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils';

class Store extends ReduceStore {
    constructor(dispatcher, id = 'page-container') {
        super(dispatcher)
        this.dispatcher = dispatcher
        this.id = id
    }

    getInitialState() {
        return Immutable.Map({
            'applicationTitle': 'Management',
            'userName':         'Anonymous',
            'menu':             Immutable.List([
                {
                    'name':  'Tournament',                       'url': '/tournament',  "key": "tournament",
                    'role': 'admin',
                    'icon_name': 'ni-trophy',
                },
                {
                    'name':  'Challenge',                       'url': '/challenge',  "key": "challenge",
                    'role': 'admin',
                    'icon_name': 'ni-laptop',
                }
            ]),
            'isProfileVisible': false,
            'menuSelected': "",
            'categorySelected':    "",
        })
    }

    reduce(state, action) {
        switch (action.type) {
            case this.id + '/set-application-title':
                state = state.set('applicationTitle', action.title)
                return state
            case this.id + '/set-menu':
                state = state.set('menuSelected', action.menuSelected)
                return state
            case this.id + '/set-category':
                state = state.set('categorySelected', action.categorySelected)
                return state
            case this.id + '/set-username':
                state = state.set('userName', action.userName)
                return state
            case this.id + '/profile-click':
                var isProfileVisible = state.get('isProfileVisible')
                isProfileVisible = !isProfileVisible
                state = state.set('isProfileVisible', isProfileVisible)
                return state
        }
        return state;
    }
}

export default Store;
