import React, {Component} from 'react';
import UserService from 'app-services/userServices'
class View extends Component {
    componentWillMount() {
        this.props.action.load()
    }
    renderFooter() {
        return (
            <footer className="footer">
                <div className="row align-items-center justify-content-xl-between">
                    <div className="col-xl-6">
                        <div className="copyright text-center text-xl-left text-muted">
                        &copy; 2018 <a href="https://battlefi.io" className="font-weight-bold ml-1" target="_blank">BattleFi</a>
                        </div>
                    </div>
                </div>
            </footer>
        )
    }
    renderPermissionName(per){
        let name = 'No role'
        if (per == 'admin'){
            name = 'Admin'
        }
        if (per == 'jury'){
            name = 'Validator'
        }
        if (per == 'mod')
            name = 'Mod'
        return name
    }
    render() {
        var _this = this;
        var profile = UserService.getProfile()

        return (
            <div>
                {_this.renderMenu()}
                <div className="main-content">
                    <nav className="navbar navbar-top navbar-expand-md navbar-dark" id="navbar-main">
                        <div className="container-fluid">
                            <a className="h4 mb-0 text-white text-uppercase d-none d-lg-inline-block" href="#">Management</a>
                            <ul className="navbar-nav align-items-center d-none d-md-flex">
                                <li className="nav-item dropdown">
                                    <a className="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div className="media align-items-center">
                                            <span className="avatar avatar-sm rounded-circle">
                                                <img style={{ width: "125%"}} alt="Image placeholder" src="/public/static/agon/img/logo-icon.png" />
                                            </span>
                                            <div className="media-body ml-2 d-none d-lg-block">
                                            <div className="top-menu">
                                                <ul className="nav navbar-nav pull-right">
                                                    <li className="dropdown dropdown-user">
                                                        <a href="javascript:;" style={{color: '#fff'}} onClick={e => this.props.action.clickProfile()} className="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                                            <span className="username username-hide-on-mobile">{  profile['name'] }</span>
                                                            <i className="fa fa-angle-down"></i>
                                                        </a>
                                                        <p style={{ fontSize: 10, fontWeight: 700, color: '#fff' }} className="lanbel-permisison username-hide-on-mobile">{ this.renderPermissionName(profile['permission']) }</p>
                                                        <ul className={(this.props.pageContainer.get('isProfileVisible')) ? "dropdown-menu dropdown-menu-default show" : "dropdown-menu dropdown-menu-default"}>
                                                            <li>
                                                                <a href="javascript:;"  onClick={e=> this.props.action.logout()}>
                                                                &nbsp;&nbsp;&nbsp;<i className="ni ni-curved-next"></i> Log Out
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div className="header bg-gradient-primary pb-3 pt-1 pt-md-8 header-color">
                    </div>
                    <div className="container-fluid mt--7">
                        {_this.props.children}
                    </div>
                </div>
            </div>
        )
    }
    
    renderMenu() {
        var _this = this
        var menuSelected = this.props.pageContainer.get("menuSelected")
        var categorySelected = this.props.pageContainer.get("categorySelected")
        return (
            <nav className="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
                <div className="container-fluid">
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <a className="navbar-brand pt-10" href="">
                    <img src="/public/static/agon/img/logo-icon.png" className="navbar-brand-img" alt="..." />
                </a>
                <div className="collapse navbar-collapse" id="sidenav-collapse-main">
                    <ul className="navbar-nav">
                        {
                            _this.props.pageContainer.get('menu').toJS().map(function(menu, index) {
                                return (
                                    <li className={(menuSelected == menu.key) ? "nav-item active" : "nav-item"} title={menu.name}>
                                        <a className={(menuSelected == menu.key) ? "nav-link active" : "nav-link"} href={menu.url}>
                                            <i className={"ni " + menu.icon_name}></i>
                                        </a>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
                </div>
            </nav>
        )
    }
}

export default View;
