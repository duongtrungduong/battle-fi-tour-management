import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Textarea } from 'library-form-textarea'
import {Modal} from 'react-bootstrap';
import "./style.css"
import {Select} from 'library-form-select'
import { Bracket, RoundProps } from 'react-brackets';

class View extends Form.View {
    constructor(props) {
        super(props)
    }

    render() {
        var _this = this;
        return (
            <div className="row">
                <div className="col-md-12">
                    <span className="badge badge-primary" style={{}}>
                        {_this.props.data.get("name")}
                    </span>&nbsp;
                    <span className="badge badge-primary" style={{}}>
                        {_this.props.data.get("game_kind_name")}
                    </span>&nbsp;
                    <span className="badge badge-primary" style={{}}>
                        {_this.props.data.get("team_size")}
                    </span>
                </div>
            </div>
        )
    }

}

export default View;
