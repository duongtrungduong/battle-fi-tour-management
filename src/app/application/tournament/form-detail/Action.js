import jQuery from 'jquery'
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {MultiSelect} from 'library-form-multiselect'
import {Checkbox} from 'library-form-checkbox'
import {UploadFile} from 'library-form-upload-file'
import moment from 'moment'
import UserService from 'app-services/userServices'
import { Record } from 'immutable'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
        }
    }

    load() {
        var _this = this
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/tournament/get?id=' + __params.query.id,
            data     : {
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatcherLoadData(
                        result.data.name,
                        result.data.game_kind_icon_url,
                        result.data.game_kind_name,
                        result.data.number_team
                    )                            
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can not load data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "")
                _this.dispatchFormLoadFail()
            }
        })
    }

    dispatcherLoadData(name, game_kind_icon_url, game_kind_name, team_size) {
        var _this = this
        this.dispatcher.dispatch({
            type:          _this.formId + "/load-data",
            name:           name,    
            game_kind_icon_url:  game_kind_icon_url,
            game_kind_name: game_kind_name,
            team_size:  team_size,

        })
    }

}

export default Action;
