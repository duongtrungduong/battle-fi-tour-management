import React, {Component} from 'react';
import {Container} from 'flux/utils';
import {Dispatcher} from 'flux';

import UserService from 'app-services/userServices'

import PageContainerView from 'app-container/View';
import PageContainerStore from 'app-container/Store';
import PageContainerAction from 'app-container/Action';
import DataTableView from './data-table-party/View';
import DataTableStore from './data-table-party/Store';
import DataTableAction from './data-table-party/Action';
import FormSearchView from './form-search-party/View';
import FormSearchStore from './form-search-party/Store';
import FormSearchAction from './form-search-party/Action';

import FormDetailView from './form-detail-request-to-join/View'
import FormDetailStore from './form-detail-request-to-join/Store'
import FormDetailAction from './form-detail-request-to-join/Action'

const _dispatcher = new Dispatcher();
const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')
const _dataTableStore = new DataTableStore(_dispatcher, 'data-table-party')
const _dataTableAction = new DataTableAction(_dispatcher, 'data-table-party')
const _formSearchStore = new FormSearchStore(_dispatcher, 'form-search-party')
const _formSearchAction = new FormSearchAction(_dispatcher, 'form-search-party')
const _formDetailStore = new FormDetailStore(_dispatcher, 'form-detail')
const _formDetailAction = new FormDetailAction(_dispatcher, 'form-detail')

_pageContainerAction.setCategory("general")
_pageContainerAction.setMenu("tournament")


class ListPartyContainer extends Component {
    static getStores() {
        return [
            _pageContainerStore,
            _dataTableStore,
            _formSearchStore,
            _formDetailStore,

        ];
    }

    static calculateState(prevState) {
        return {
            pageContainer:    _pageContainerStore.getState(),
            dataTable:        _dataTableStore.getState(),
            formSearch:       _formSearchStore.getState(),
            formDetail:       _formDetailStore.getState(),
        }
    }

    render() {
        var profile = UserService.getProfile()
        return (
            <PageContainerView pageContainer={this.state.pageContainer} action={_pageContainerAction}>
                <div className="row mt-5">
                    <div className="col-xl-12 mb-5 mb-xl-0">
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <FormSearchView
                                            form={this.state.formSearch}
                                            action={_formSearchAction}
                                            dataTableAction={_dataTableAction}
                                        />
                                    </div>
                                    <div className="col-4 text-right">
                                    </div>
                                </div>
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <FormDetailView
                                            action={_formDetailAction}
                                            data={this.state.formDetail}
                                        />
                                    </div>
                                </div>
                            </div>
                            <DataTableView
                                dataTable={this.state.dataTable}
                                action={_dataTableAction}
                            />
                        </div>
                    </div>
                </div>  
			</PageContainerView>
        )
    }
}

export default Container.create(ListPartyContainer);
