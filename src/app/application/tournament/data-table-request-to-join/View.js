import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import UserService from 'app-services/userServices'
import './style.css'

class View extends LibraryDataTable.View {
    renderTableHeader() {
        return <thead className='thead-light'>
            <tr>
                <th scope="col">Infomation</th>
                <th scope="col" width="20%" style={{ textAlign: 'center'}}>Teams</th>
                <th scope="col" width="20%">Account</th>
                <th scope="col" width="10%">Request time</th>
                <th scope="col" width="10%">Status</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
    }

    renderTableBody() {
        var _this = this;
        return (
            <tbody>
                {
                    this.props.dataTable.get('records').map(function(record, i) {

                        return(
                      
                            <tr key={"record-" + i}>
                                <td>
                                    <p className="p-info"><span className='title-info'>Ingame: </span>{record.in_game_name}</p>     
                                    <p className="p-info sub-info"><span  className='title-info'>Name: </span>{record.name}</p>
                                    <p className="p-info sub-info"><span  className='title-info'>Phone: </span>{record.phone_number}</p>
                                    <p className="p-info sub-info"><span  className='title-info'>Email: </span>{record.email}</p>                      
                                </td>
                                <td style={{ textAlign: 'center'}}>
                                    <div>
                                        <img className="team-icon" src={record.team_icon != '' ? record.team_icon : '/public/static/agon/img/logo-icon.png'} width="30px" height="30px"/>
                                        <br/>
                                        { record.team_name }
                                        <br/>
                                        {
                                            (record.team_size >= record.tour_team_size) ?
                                            <span style={{ fontSize: 10}} class="badge badge-success">{record.team_size}/{record.tour_team_size} members</span>
                                            : <span style={{ fontSize: 10}} class="btn btn-sm btn-warning">{record.team_size}/{record.tour_team_size} members</span>

                                        }
                                        
                                    </div>
                                </td>
                                <td>
                                    <img className="account-avt" src={record.account_avatar != '' ? record.account_avatar : '/public/static/agon/img/logo-icon.png'} width="30px"/>
                                    &nbsp;
                                    &nbsp;
                                    { record.account_name }
                                </td>
                                <td>
                                { (new Date(parseInt(record.timestamp*1000))).toLocaleString()}
                                </td>
                                <td>{ _this.renderStatusData(record.status) }</td>
                                <td style={{textAlign: "right"}}>
                                    {_this.renderAction(record)}
                                </td>
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }
    renderAction(record) {
        var _this = this
        var profile = UserService.getProfile()
        if (profile['permission'] != "admin" && profile['permission'] != "mod") {
            return <span>
                <a href={`/challenge?tour_id=${record.id}`} className="btn btn-sm btn-info" style={{color: "white"}}>View challenges</a>
            </span>
        }
        return <span>
            {
               record.status == 'pending' && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-success"  href="javascript:;" onClick={e => _this.props.action.responseRequestJoinTour(e, record.id, _this.props, 'accepted')}>Accept</a>
                : false
            }
            {
                record.team_size != record.tour_team_size && record.status == 'pending'    
                ? <a className="btn btn-sm btn-info"  href="javascript:;" onClick={e => _this.props.action.sendEmail(e, record.id, record.team_id, record.tournament_id)}>Send Email</a>
                : false
            }
            {
               (record.status == 'pending' || record.status == 'waiting') && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-warning"  href="javascript:;" onClick={e => _this.props.action.responseRequestJoinTour(e, record.id, _this.props, 'rejected')}>Reject</a>
                : false
            }
        </span>
    }

    renderStatusData(status) {
        var _this = this
        if (status == 'pending') {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>Pending</span>
        } else if (status == 'accepted') {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Accepted</span>
        } else if (status == 'waiting') {
            return <span className='badge badge-info' style={{fontSize: 13, fontWeight: 600}}>Waiting</span>
        } else {
            return <span className='badge badge-danger' style={{fontSize: 13, fontWeight: 600}}>Rejected</span>
        }
    }
}

export default View;
