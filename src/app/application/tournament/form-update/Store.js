import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import FormConfigStore from "./form-setting-config/StoreHelper"
import {Select} from 'library-form-select'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'name':               new Textbox.StoreHelper(this.formId, 'name'),
            'desc':               new Wysiwyg.StoreHelper(this.formId, 'desc'),
            'reward':             new Textbox.StoreHelper(this.formId, 'reward'),
            'configs':            new FormConfigStore(this.formId, 'configs'),
            // 'bracket_type':       new Select.StoreHelper(this.formId, 'bracket_type'),
            'number_team':        new Select.StoreHelper(this.formId, 'number_team'),
            'feature_url':               new Textbox.StoreHelper(this.formId, 'feature_url'),
        }
    }
}

export default Store;
