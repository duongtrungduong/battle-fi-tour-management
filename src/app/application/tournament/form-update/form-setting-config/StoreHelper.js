import Immutable from 'immutable';
import { Field } from 'library-field';

class StoreHelper extends Field.StoreHelper {
    getInitialState() {
        return Immutable.Map({
            'id':               this.fieldId,
            'value':            Immutable.List([]),
            'error':            '',
        })
    }

    reduceFieldClearValue(state) {
        return state.set('value', Immutable.List([]))
    }

    reduceFieldChangeValue(state, value) {
        return state.set('value', Immutable.List(value))
    }

    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case 'set-value':
                    state = state.set('value', Immutable.List(action.value))
                    return state
                default:
                    state = super.reduce(state, action)
            }
        }
        return state
    }
}

export default StoreHelper;
