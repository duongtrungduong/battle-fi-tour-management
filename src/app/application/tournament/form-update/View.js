import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import FormConfigView from "./form-setting-config/View"
import {Select} from 'library-form-select'

class View extends Form.View {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form className="form" onSubmit={e => e.preventDefault()}>
                    <div>
                        <div className="form-group">
                            <label className="control-label">Name<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('name')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Reward<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('reward')}
                                action={this.props.action}
                            />
                        </div>
                        {/* <div className="form-group">
                            <label className="control-label">Bracket Type<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('bracket_type')}
                                action={this.props.action}
                            />
                        </div> */}
                        <div className="form-group">
                            <label className="control-label">Number Teams<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('number_team')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Feature Image Url<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('feature_url')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <FormConfigView
                                data={this.props.form.get('fields').get('configs')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Description</label>
                            <Wysiwyg.View
                                data={this.props.form.get('fields').get('desc')}
                                action={this.props.action}
                                
                            />
                        </div>
                        
                    </div>
                    <div>
                        {
                            this.props.form.get('messages').map(function(message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                                    }
                                    return (<div className="alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>

                    <div className="form-actions">
                        <div className="row">
                            <div className="col-md-9">
                                <button className="btn btn-sm btn-primary" onClick={ e => this.props.action.submit(this.props.form, e)}>Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </BlockUi>
        )
    }
}

export default View;
