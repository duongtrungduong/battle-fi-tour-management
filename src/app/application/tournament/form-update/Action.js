import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import UserService from 'app-services/userServices'
import FormConfigAction from "./form-setting-config/ActionHelper"
import {Select} from 'library-form-select'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':                 new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'desc':                 new Wysiwyg.ActionHelper(this.dispatcher, this.formId, 'desc'),
            'reward':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'reward'),
            'configs':              new FormConfigAction(this.dispatcher, this.formId, 'configs'),
            // 'bracket_type':       new Select.ActionHelper(this.dispatcher, this.formId, 'bracket_type'),
            'number_team':        new Select.ActionHelper(this.dispatcher, this.formId, 'number_team'),
            'feature_url':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'feature_url'),
            
        }
    }

    load() {
        var _this = this
        this.helper("number_team").dispatchSetChoices([
            {"value": 4, "label": 4},
            {"value": 8, "label": 8},
            {"value": 12, "label": 12},
            {"value": 16, "label": 16},
             {"value": 32, "label": 32},
              {"value": 64, "label": 64},
        ])
        // this.helper("bracket_type").dispatchSetChoices([
        //     {"value": 1, "label": "Single Elimination"},
        //     {"value": 2, "label": "Round Robin"},
        //     {"value": 3, "label": "Double Elimination"},
        // ])
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/tournament/get?id=' + __params.query.id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.helper('name').dispatchChangeValue(result.data.name)
                    _this.helper('desc').dispatchChangeValue(result.data.desc)
                    _this.helper('reward').dispatchChangeValue(result.data.reward)
                    _this.helper('configs').dispatchChangeValue(result.data.game_configs)
                    // _this.helper('bracket_type').dispatchChangeValue(result.data.bracket_type)
                    _this.helper('number_team').dispatchChangeValue(result.data.number_team)
                    _this.helper("feature_url").dispatchChangeValue(result.data.feature_url)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load the data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (!values.name) {
            hasError = true
            this.helper('name').dispatchHasError('This field is mandatory')
        }
        // if (!values.bracket_type) {
        //     hasError = true
        //     this.helper('bracket_type').dispatchHasError('This field is mandatory')
        // }
        if (!values.number_team) {
            hasError = true
            this.helper('number_team').dispatchHasError('This field is mandatory')
        }
        if (!hasError) {
            this.dispatchFormLoadStart()
            jQuery.ajax({
                headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/tournament/update?id=' + __params.query.id,
                dataType:     'json',
                data:         JSON.stringify(values),
                success:      function(result) {
                    if (result.status == 'ok') {
                        _this.dispatchFormAddMessage("info", "The record has been created successfully!")
                        _this.dispatchFormSubmitSuccess(values)
                        _this.resetForm()
                    }
                    else {
                        if (result.data.message != undefined) {
                            _this.dispatchFormAddMessage("error", result.data.message)
                        }
                        else {
                            _this.dispatchFormAddMessage("error", "Can't update the record")
                        }
                        _this.dispatchFormSubmitFail(values)
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage("error", "Can't communicate to API")
                    _this.dispatchFormSubmitFail(values)
                }
            })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
