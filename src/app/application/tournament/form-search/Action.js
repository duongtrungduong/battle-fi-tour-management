import jQuery from 'jquery';
import { Form} from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'
import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':              new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'game_kind_id':      new Select.ActionHelper(this.dispatcher, this.formId, 'game_kind_id'),
        }
    }
    load() {
        var _this = this
        if (__params.query.name != undefined) {
            this.helper("name").dispatchChangeValue(__params.query.name)
        }
        if (__params.query.game_kind_id != undefined) {
            this.helper("game_kind_id").dispatchChangeValue(__params.query.game_kind_id)
        }

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/game_kind/list',
            data:      {
                '_sort_key': "name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var game_kind_ids = []
                    result.data.records.map(function(val, key) {
                        game_kind_ids.push({
                            "value": val.id,
                            "label": val.name
                        })
                    })
                    _this.helper("game_kind_id").dispatchSetChoices(game_kind_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

    }   

    search(props) {

        var _this = this;
        var values = this.getValues(props.form)
        var url = '?'

        if ( values.name ) {
            url += 'name=' + encodeURIComponent(values.name) + '&'
        }

        if ( values.game_kind_id ) {
            url += 'game_kind_id=' + encodeURIComponent(values.game_kind_id)
        }
        location.href = url
    }

}

export default Action;
