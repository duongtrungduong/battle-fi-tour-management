import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
class View extends Form.View {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form className="form" onSubmit={e => e.preventDefault()}>
                    <div>
                        <div className="form-group">
                            <label className="control-label">Name<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('name')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Game<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('game_kind_id')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Reward<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('reward')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <div className='row'>
                                <div className='col-md-2'>
                                    <label className="control-label">First Prize</label>
                                    <Textbox.View
                                        data={this.props.form.get('fields').get('price_1st')}
                                        action={this.props.action}
                                    />
                                </div>
                                <div className='col-md-2'>
                                    <label className="control-label">Second Prize</label>
                                    <Textbox.View
                                        data={this.props.form.get('fields').get('price_2nd')}
                                        action={this.props.action}
                                    />
                                </div>
                                <div className='col-md-2'>
                                    <label className="control-label">Bronze Prize</label>
                                    <Textbox.View
                                        data={this.props.form.get('fields').get('price_3rd')}
                                        action={this.props.action}
                                    />
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <label className="control-label">Start Date<span className="required" aria-required="true">*</span></label>
                            <DatetimePicker.View
                                data={this.props.form.get('fields').get('start_date')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Bracket Type<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('bracket_type')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Participants play each other</label>
                            <Select.View
                                data={this.props.form.get('fields').get('play_each_other')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Number Teams<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('number_team')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Host by</label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('host')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Host Icon Url</label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('host_icon_url')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Description</label>
                            <Wysiwyg.View
                                data={this.props.form.get('fields').get('desc')}
                                action={this.props.action}
                            />
                        </div>
                        
                    </div>
                    <div>
                        {
                            this.props.form.get('messages').map(function(message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                                    }
                                    return (<div className="alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>

                    <div className="form-actions">
                        <div className="row">
                            <div className="col-md-9">
                                <button className="btn btn-sm btn-primary" onClick={ e => this.props.action.submit(this.props.form, e)}>Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </BlockUi>
        )
    }
}

export default View;
