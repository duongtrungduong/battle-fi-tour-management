import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import UserService from 'app-services/userServices'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
import moment from "moment"

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'game_kind_id':       new Select.ActionHelper(this.dispatcher, this.formId, 'game_kind_id'),
            'desc':               new Wysiwyg.ActionHelper(this.dispatcher, this.formId, 'desc'),
            'start_date':         new DatetimePicker.ActionHelper(this.dispatcher, this.formId, 'start_date'),
            'bracket_type':       new Select.ActionHelper(this.dispatcher, this.formId, 'bracket_type'),
            'number_team':        new Select.ActionHelper(this.dispatcher, this.formId, 'number_team'),
            'reward':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'reward'),
            'host':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'host'),
            'host_icon_url':      new Textbox.ActionHelper(this.dispatcher, this.formId, 'host_icon_url'),
            'price_1st':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'price_1st'),
            'price_2nd':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'price_2nd'),
            'price_3rd':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'price_3rd'),
            'play_each_other':         new Select.ActionHelper(this.dispatcher, this.formId, 'play_each_other'),
        }
    }

    load() {
        var _this = this
        this.helper("number_team").dispatchSetChoices([
            {"value": 4, "label": 4},
            {"value": 8, "label": 8},
            {"value": 12, "label": 12},
            {"value": 16, "label": 16},
        ])
        this.helper("bracket_type").dispatchSetChoices([
            {"value": 1, "label": "Single Elimination"},
            {"value": 2, "label": "Round Robin"},
            {"value": 3, "label": "Double Elimination"},
        ])
        this.helper("play_each_other").dispatchSetChoices([
            {"value": 'once', "label": 'Once'},
            {"value": 'twice', "label": 'Twice'},
        ])
        this.helper('play_each_other').dispatchChangeValue('once')
        this.helper('reward').dispatchChangeValue(0)
        this.helper('price_1st').dispatchChangeValue(0)
        this.helper('price_2nd').dispatchChangeValue(0)
        this.helper('price_3rd').dispatchChangeValue(0)

        this.helper("start_date").dispatchChangeValue(new Date())
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/game_kind/list',
            data:      {
                '_sort_key': "name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var game_kind_ids = []
                    result.data.records.map(function(val, key) {
                        game_kind_ids.push({
                            "value": val.id,
                            "label": val.name
                        })
                    })
                    _this.helper("game_kind_id").dispatchSetChoices(game_kind_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (!values.name) {
            hasError = true
            this.helper('name').dispatchHasError('This field is mandatory')
        }
        if (!values.game_kind_id) {
            hasError = true
            this.helper('game_kind_id').dispatchHasError('This field is mandatory')
        }
        if (!values.bracket_type) {
            hasError = true
            this.helper('bracket_type').dispatchHasError('This field is mandatory')
        }
        if (!values.number_team) {
            hasError = true
            this.helper('number_team').dispatchHasError('This field is mandatory')
        }
        if (!values.start_date) {
            hasError = true
            this.helper('start_date').dispatchHasError('This field is mandatory')
        }
        if (!hasError) {
                var dataSubmit = {}
                dataSubmit['name'] = values.name
                dataSubmit['bracket_type'] = values.bracket_type
                dataSubmit['number_team'] = values.number_team
                dataSubmit['game_kind_id'] = values.game_kind_id
                dataSubmit['start_date'] = moment(values.start_date).format("X")
                dataSubmit['desc'] = values.desc
                dataSubmit['reward'] = values.reward
                dataSubmit['host'] = values.host
                dataSubmit['price_1st'] = values.price_1st
                dataSubmit['price_2nd'] = values.price_2nd
                dataSubmit['price_3rd'] = values.price_3rd
                dataSubmit['host_icon_url'] = values.host_icon_url
                dataSubmit['play_each_other'] = values.play_each_other

                console.log(dataSubmit)
                this.dispatchFormLoadStart()
                jQuery.ajax({
                    headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                    type:         'POST',
                    url:          __params.config['api.endpoint'] + '/tournament/create',
                    dataType:     'json',
                    data:         dataSubmit,
                    success:      function(result) {
                        if (result.status == 'ok') {
                            _this.dispatchFormAddMessage("info", "The record has been created successfully!")
                            _this.dispatchFormSubmitSuccess(values)
                            _this.resetForm()
                        }
                        else {
                            if (result.data.message != undefined) {
                                _this.dispatchFormAddMessage("error", result.data.message)
                            }
                            else {
                                _this.dispatchFormAddMessage("error", "Can't update the record")
                            }
                            _this.dispatchFormSubmitFail(values)
                        }
                    },
                    error:        function(data) {
                        _this.dispatchFormAddMessage("error", "Can't communicate to API")
                        _this.dispatchFormSubmitFail(values)
                    }
                })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
