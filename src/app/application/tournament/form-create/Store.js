import Immutable from 'immutable';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Wysiwyg} from 'library-wysiwyg'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'name':               new Textbox.StoreHelper(this.formId, 'name'),
            'game_kind_id':       new Select.StoreHelper(this.formId, 'game_kind_id'),
            'desc':               new Wysiwyg.StoreHelper(this.formId, 'desc'),
            'start_date':         new DatetimePicker.StoreHelper(this.formId, 'start_date'),
            'bracket_type':       new Select.StoreHelper(this.formId, 'bracket_type'),
            'number_team':        new Select.StoreHelper(this.formId, 'number_team'),
            'reward':             new Textbox.StoreHelper(this.formId, 'reward'),
            'host':             new Textbox.StoreHelper(this.formId, 'host'),
            'price_1st':             new Textbox.StoreHelper(this.formId, 'price_1st'),
            'price_2nd':             new Textbox.StoreHelper(this.formId, 'price_2nd'),
            'price_3rd':             new Textbox.StoreHelper(this.formId, 'price_3rd'),
            'host_icon_url':         new Textbox.StoreHelper(this.formId, 'host_icon_url'),
            'play_each_other':        new Select.StoreHelper(this.formId, 'play_each_other'),
        }
    }
}

export default Store;
