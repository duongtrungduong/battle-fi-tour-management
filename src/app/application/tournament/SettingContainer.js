import React, {Component} from 'react';
import {Container} from 'flux/utils';
import {Dispatcher} from 'flux';


import PageContainerView from 'app-container/View';
import PageContainerAction from 'app-container/Action';
import PageContainerStore from 'app-container/Store';
import FormView from './form-setting/View';
import FormStore from './form-setting/Store';
import FormAction from './form-setting/Action';


const _dispatcher = new Dispatcher();
const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')
const _formStore = new FormStore(_dispatcher, 'form')
const _formAction = new FormAction(_dispatcher, 'form')

_pageContainerAction.setCategory("general")
_pageContainerAction.setMenu("tournament")
class SettingContainer extends Component {
    static getStores() {
        return [
            _pageContainerStore,
            _formStore,
        ];
    }

    static calculateState(prevState) {
        return {
            pageContainer:    _pageContainerStore.getState(),
            form:             _formStore.getState(),
        }
    }

    render() {
        return (
            <PageContainerView pageContainer={this.state.pageContainer} action={_pageContainerAction}>
                <div className="row mt-5">
                    <div className="col-xl-12 mb-5 mb-xl-0">
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <h4 style={{ fontWeight: "600" }}>EDIT TOURNAMENT</h4>
                            </div>
                            <div className="card-body">
                                <FormView
                                    form={this.state.form}
                                    action={_formAction}
                                />
                            </div>
                        </div>
                    </div>
                </div>  
            </PageContainerView>
        )
    }
}

export default Container.create(SettingContainer);
