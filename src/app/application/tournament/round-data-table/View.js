import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import "./style.css"

class View extends LibraryDataTable.View {
    renderTableHeader() {
        return <thead>
            <tr>
                <th>Round</th>
                <th width="70%">Team</th>
                <th width="20%" style={{textAlign: "center"}}>Winner</th>
            </tr>
        </thead>
    }
    renderRoundId(round_id) {
        if (round_id == "round_1") {
            return "Round 1"
        }
        if (round_id == "round_2") {
            return "Round 2"
        }
        if (round_id == "round_3") {
            return "Round 3"
        }
        if (round_id == "round_4") {
            return "Round 4"
        }
        if (round_id == "round_5") {
            return "Round 5"
        }
        if (round_id == "round_6") {
            return "Round 6"
        }
        if (round_id == "round_7") {
            return "Round 7"
        }
        return "Round 8"
    }
    renderTableBody() {

        var _this = this;
        return (
            <tbody>
                {
                    this.props.dataTable.get('records').map(function(record, i) {

                        return(
                      
                            <tr key={"record-" + i}>
                                <td>
                                    {_this.renderRoundId(record.round_id)}
                                </td>
                                <td>
                                    <div className="row">
                                        <div className="col-md-5">
                                            <div className="col-md-12"
                                                style={{textAlign:"right"}}
                                            >   
                                            { record.team_1_name }
                                            &nbsp;
                                                <img
                                                    className="image-team"
                                                    src={record.team_1_icon}
                                                    onClick={e => _this.props.action.setWin(e, record.id, record.team_1_id, _this.props)}
                                                />
                                            </div>
                                            <div className="col-md-12"
                                                style={{textAlign:"right"}}
                                            >
                                            </div>
                                            
                                        </div>
                                        <div className="col-md-1">
                                            <img 
                                                style={{width: 30, height: 30, marginTop: 10}}
                                                src={"https://battle-fi.s3.ap-southeast-1.amazonaws.com/teams/VS.png"} 
                                                className=""/>
                                        </div>
                                        <div className="col-md-5">
                                            <img 
                                                src={record.team_2_icon} 
                                                onClick={e => _this.props.action.setWin(e, record.id, record.team_2_id, _this.props)}
                                                className="image-team"/>
                                            &nbsp;
                                            { record.team_2_name }
                                        </div>

                                    </div>
                                </td>
                                <td style={{textAlign: "center"}}>
                                    {
                                        (record.winner_id) ?
                                        <div className="row">
                                            <div className="col-md-12">
                                                <img 
                                                    src={record.winner_icon} 
                                                    className="image-team"/>
                                            </div>
                                            <div className="col-md-12">
                                                { record.winner_name }
                                            </div>
                                        </div>: false
                                    }
                                    
                                </td>
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }

    renderStatusData(status) {
        var _this = this
        if (status == 1) {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>Active</span>
        } else if (status == 2) {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Finished</span>
        } else if (status == 3) {
            return <span className='badge badge-danger' style={{fontSize: 13, fontWeight: 600}}>Cancelled</span>
        } else {
            return <span className='badge badge-secondary' style={{fontSize: 13, fontWeight: 600}}>Open</span>
        }
    }
    render() {
        return (
            <BlockUi tag="div" blocking={!this.props.dataTable.get('isReady')}>
                <div>
                    { this.renderMessages() }
                </div>
                <div>
                    { this.renderTable() }
                </div>
            </BlockUi>
        )
    }
}

export default View;
