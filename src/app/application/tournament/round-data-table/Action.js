
import LibraryDataTable from 'library-data-table';
import jQuery from 'jquery';
import UserService from 'app-services/userServices'

class Action extends LibraryDataTable.Action {
    extractTableParams(dataTable) {
        var params = {}
        params['page_number'] = (dataTable.get('currentPage') - 1) *  dataTable.get('recordPerPage');
        params['page_size'] = dataTable.get('recordPerPage');

        var filters = dataTable.get('filters');
        for (var k in filters) {
            params[k] = filters.get(k);
        }
        return params;
    }

    loadRecords(props) {
        this.dispatchLoadRecordsStart();

        var _this = this;
        var params = this.extractTableParams(props.dataTable)
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/tournament/tournament_round?id='+__params.query.id,
            data:      params,
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatchLoadRecordsSuccess(result.data.records, result.data.total_matched)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
    setWin(e, round_id, winner_id, props) {
        var _this = this
        e.preventDefault()
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/tournament/set_win?id='+round_id + "&winner_id="+ winner_id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.loadRecords(props)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
}

export default Action;
