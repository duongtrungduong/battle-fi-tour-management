import { Form} from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'
import {Password} from 'library-form-password';
import UserService from 'app-services/userServices'
import {MultiSelect} from 'library-form-multiselect'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'mods':       new MultiSelect.ActionHelper(this.dispatcher, this.formId, 'mods'),
        }
    }

    load() {
        var _this = this;
        this.dispatchFormLoadStart()
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:         'GET',
            url:          __params.config['api.endpoint'] + '/tournament_setting/get?id='+ __params.query.id,
            dataType:     'json',
            data:         {},
            async:        false,
            success:      function(result) {
                if(result.status == 'ok') {
                    _this.helper('mods').dispatchChangeValue(result.data.mods)
                    _this.dispatchFormLoadSuccess();
                }
            },
            error:        function(data) {
                _this.dispatchFormAddMessage("error", "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:         'GET',
            url:          __params.config['api.endpoint'] + '/account/list?page_size=999999&_sort_key=email&_sort_dir=asc',
            dataType:     'json',
            data:         {},
            async:        false,
            success:      function(result) {
                if(result.status == 'ok') {
                    var lst = []
                    result.data.records.map(function(val, key){
                        lst.push({
                            "label": val.email + ` (${val.name})`,
                            "value": val.id
                        })
                    })
                    _this.helper('mods').dispatchSetChoices(lst)
                    _this.dispatchFormLoadSuccess();
                }
            },
            error:        function(data) {
                _this.dispatchFormAddMessage("error", "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }
        this.load()
    }

    submit(form, e, props) {
        var jwt = UserService.getToken()
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false
        if (!hasError) {
            this.dispatchFormLoadStart()
            jQuery.ajax({
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/tournament_setting/update?id=' + __params.query.id,
                dataType:     'json',
                data:         {
                    'mods': JSON.stringify(values.mods),
                },
                headers: {
                    "TOKEN": jwt
                },
                success:      function(result) {
                    if (result.status == 'ok') {
                        _this.dispatchFormAddMessage("info", "The record has been update successfully!")
                        _this.dispatchFormSubmitSuccess(values)
                        setTimeout(() => {
                            window.location.reload()
                          }, "2000")
                    }
                    else {
                        _this.dispatchFormAddMessage("error", "Can't update the record")
                        _this.dispatchFormSubmitFail(values)
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage("error", "Can't communicate to API")
                    _this.dispatchFormSubmitFail(values)
                }
            })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
