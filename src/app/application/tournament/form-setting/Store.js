import Immutable from 'immutable';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {MultiSelect} from 'library-form-multiselect'
class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    initStoreHelpers() {
        this._storeHelpers = {
            'mods':              new MultiSelect.StoreHelper(this.formId, 'mods'),
        }
    }
}

export default Store;
