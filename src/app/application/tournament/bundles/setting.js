
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import SettingContainer from '../SettingContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <SettingContainer />,
        document.getElementById('root')
    )
});
