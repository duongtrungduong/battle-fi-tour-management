
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import ListRoundContainer from '../ListRoundContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <ListRoundContainer />,
        document.getElementById('root')
    )
});
