
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import ListPartyContainer from '../ListPartyContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <ListPartyContainer />,
        document.getElementById('root')
    )
});
