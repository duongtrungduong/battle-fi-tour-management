import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import UserService from 'app-services/userServices'


class View extends LibraryDataTable.View {
    renderTableHeader() {
        return <thead className='thead-light'>
            <tr>
                <th scope="col">Tournament</th>
                <th scope="col" width="10%">Reward</th>
                <th scope="col" width="10%">Teams</th>
                <th scope="col" width="20%">Game kind</th>
                <th scope="col" width="10%">Current Round</th>
                <th scope="col" width="10%">Status</th>
                <th scope="col" width="20%"></th>
            </tr>
        </thead>
    }
    renderRoundLabel(label) {
        var str = label.replace("_", " ")

        return str
    }
    renderTableBody() {
        var _this = this;
        return (
            <tbody>
                {
                    this.props.dataTable.get('records').map(function(record, i) {

                        return(
                      
                            <tr key={"record-" + i}>
                                <td>{ record.name }</td>
                                <td>
                                    <div style={{textAlign:"right", width: 50}}>
                                        { record.reward }
                                    </div>
                                </td>
                                <td>
                                    {record.team_joined}/{record.num_team}
                                </td>
                                <td>
                                    <img className="game-kind-icon" src={record.game_kind_url} width="20px"/>
                                    &nbsp;
                                    { record.game_kind }
                                </td>
                                <td style={{textTransform: "uppercase"}}>{_this.renderRoundLabel(record.current_round_active_label)}</td>
                                <td>{ _this.renderStatusData(record.status) }</td>
                                <td style={{textAlign: "right"}}>
                                    {_this.renderAction(record)}
                                </td>
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }
    renderAction(record) {
        var _this = this
        var profile = UserService.getProfile()
        if (profile['permission'] != "admin" && profile['permission'] != "mod") {
            return <span>
                <a href={`/challenge?tour_id=${record.id}`} className="btn btn-sm btn-info" style={{color: "white"}}>View challenges</a>
            </span>
        }
        if(record.status == 2) {
            // Finish
            return <a className="btn btn-sm btn-info" href={`/tournament/view?id=${record.id}`}>View</a>
        }
        
        return <span>
            {
               (profile['permission'] == "admin") ?
               <a className="btn btn-sm btn-info" href={`/tournament/setting?id=${record.id}`}>Setting</a>
                : false
            }
            {
               (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-success" href={`/tournament/list_of_request_to_join?id=${record.id}`}>Registered list</a>
                : false
            }
             {
               (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-info" href={`/tournament/party?id=${record.id}`}>Party list</a>
                : false
            }
            {
               [2,3].includes(record.status) == false && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-primary" href={`/tournament/update?id=${record.id}`}>Edit</a>
                : false
            }
            {
               record.status == 1 && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
                <a className="btn btn-sm btn-warning" href="javascript:;" onClick={e => _this.props.action.finishTour(e, record.id)} >Finish</a>     
                : false
            }
            &nbsp;&nbsp;
            {
                (record.status == 0) ?  // draft
                    <a className="btn btn-sm btn-success" href="javascript:;" onClick={e => _this.props.action.activeTour(e, record.id)}>Active</a>
                : 
                <a className="btn btn-sm btn-info" href={`/tournament/view?id=${record.id}`}>View</a>
            }
            {
              record.status ==  2 && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-danger" href="javascript:;" onClick={(e) => { if (window.confirm('Do you want to delete this tournament?')) _this.props.action.deleteTour(e, record.id) }}>Delete</a>
                : false
            }
        </span>
    }

    renderStatusData(status) {
        var _this = this
        if (status == 1) {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>Active</span>
        } else if (status == 2) {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Finished</span>
        } else if (status == 3) {
            return <span className='badge badge-danger' style={{fontSize: 13, fontWeight: 600}}>Cancelled</span>
        } else {
            return <span className='badge badge-secondary' style={{fontSize: 13, fontWeight: 600}}>Open</span>
        }
    }
}

export default View;
