module.exports = {
    4: {
        1: {"id": 3, "type": "win"},
        2: {"id": 3, "type": "win"},
        3: {"id": 6, "type": "win"},
        4: {"id": 5, "type": "lose"},
        5: {"id": 6, "type": "lose"},
        6: {"id": 7, "type": "lose"},       
        7: {"id": "", "type": "lose"},       
    },
    8: {
        1: {"id": 5, "type": "win"},
        2: {"id": 5, "type": "win"},
        3: {"id": 6, "type": "win"},
        4: {"id": 6, "type": "win"},
        5: {"id": 7, "type": "win"},
        6: {"id": 7, "type": "win"},       
        7: {"id": 14, "type": "win"},
        8: {"id": 10, "type": "lose"},       
        9: {"id": 11, "type": "lose"},
        10: {"id": 12, "type": "lose"},
        11: {"id": 12, "type": "lose"},
        12: {"id": 13, "type": "lose"},
        13: {"id": 14, "type": "lose"},
        14: {"id": 15, "type": "lose"},
        15: {"id": "", "type": "lose"},
    }
}