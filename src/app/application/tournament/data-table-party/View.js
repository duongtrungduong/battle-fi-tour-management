import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import UserService from 'app-services/userServices'
import './style.css'

class View extends LibraryDataTable.View {
    renderTableHeader() {
        return <thead className='thead-light'>
            <tr>
                <th scope="col" style={{ textAlign: 'left'}}>Teams</th>
                <th scope="col" width="10%" style={{ textAlign: 'center'}}>Members</th>
                <th scope="col" width="10%">Tour status</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
    }

    renderTableBody() {
        var _this = this;
        return (
            <tbody>
                {
                    this.props.dataTable.get('records').map(function(record, i) {

                        return(
                      
                            <tr key={"record-" + i}>
                                <td style={{ textAlign: 'left'}}>
                                    <div>
                                        <img className="team-icon" src={record.team_icon != '' ? record.team_icon : '/public/static/agon/img/logo-icon.png'} width="30px" height="30px"/>
                                        &nbsp;&nbsp;{ record.name }
                                    </div>
                                </td>
                                <td>
                                    {
                                        (record.team_size >= record.tour_team_size) ?
                                        <span style={{ fontSize: 10}} class="badge badge-success">{record.team_size}/{record.tour_team_size} members</span>
                                        : <span style={{ fontSize: 10}} class="btn btn-sm btn-warning">{record.team_size}/{record.tour_team_size} members</span>

                                    }
                                </td>
                                <td>
                                    {_this.renderStatusData(record.status)}
                                </td>
                                <td style={{textAlign: "right"}}>
                                    {_this.renderAction(record)}
                                </td>
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }
    renderAction(record) {
        var _this = this
        var profile = UserService.getProfile()
        return <span>
            {
               (record.status == 0 ) && (profile['permission'] == "admin" || profile['permission'] == "mod") ?
               <a className="btn btn-sm btn-warning"  href="javascript:;" onClick={e => _this.props.action.delete(e, record.id, _this.props, 'delete')}>Delete</a>
                : false
            }
        </span>
    }

    renderStatusData(status) {
        // TOURNAMENT_STATUS_DRAFT = 0
        // TOURNAMENT_STATUS_ACTIVE = 1
        // TOURNAMENT_STATUS_FINISH = 2
        // TOURNAMENT_STATUS_CANCELLED = 3
        var _this = this
        if (status == 0) {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>Pending</span>
        } else if (status == 1) {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Active</span>
        } 
        else if (status == 2) {
            return <span className='badge badge-info' style={{fontSize: 13, fontWeight: 600}}>Finish</span>
        } 
        else if (status == 3) {
            return <span className='badge badge-danger' style={{fontSize: 13, fontWeight: 600}}>Cancelled</span>
        } 
    }
}

export default View;
