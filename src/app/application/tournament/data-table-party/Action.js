
import LibraryDataTable from 'library-data-table';
import jQuery from 'jquery';
import UserService from 'app-services/userServices'

class Action extends LibraryDataTable.Action {
    extractTableParams(dataTable) {
        var params = {}
        params['page_number'] = (dataTable.get('currentPage') - 1) *  dataTable.get('recordPerPage');
        params['page_size'] = dataTable.get('recordPerPage');
        var filters = dataTable.get('filters');
        for (var k in filters) {
            params[k] = filters.get(k);
        }
        if (__params.query.keyword != undefined) {
            params['keyword'] = __params.query.keyword
        }
        if (__params.query.status != undefined) {
            params['status'] = __params.query.status
        }
        return params;
    }

    loadRecords(props) {
        this.dispatchLoadRecordsStart();
        this.dispatchClearMessage()

        var _this = this;
        var params = this.extractTableParams(props.dataTable)
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/tournament_party/list?id=' + __params.query.id,
            data:      params,
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatchLoadRecordsSuccess(result.data.records, result.data.total_matched)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
    
   
    delete(e, id, props, action) {
        e.preventDefault()
        this.dispatchClearMessage()
        var _this = this
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/tournament_party/remove?id='+id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.loadRecords(props)
                }
                else {
                    _this.dispatchLoadRecordsFail(result.data.message)
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }

   
}

export default Action;
