import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'

class View extends Form.View {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
      this.props.action.load();
    }
    render() {
        var _this = this;
        return (
            <div>
                <div className="row">
                    <div className="col-md-3">
                        <div className="form-group">
                            <label>Keyword</label>
                            <div>
                                <Textbox.View
                                    data={this.props.form.get('fields').get('keyword')}
                                    action={this.props.action}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <div className="form-group">
                            <label>Status</label>
                            <div>
                                <Select.View
                                    data={this.props.form.get('fields').get('status')}
                                    action={this.props.action}
                                />
                            </div>
                        </div>
                    </div>
                    <div className="col-md-3">
                        <label>
                            &nbsp;
                        </label>
                        <div className="form-group">
                            <a href="javascript:;"
                                style={{marginTop: '2px'}}
                                className="btn btn-sm btn-primary"
                                onClick={() => this.props.action.search(this.props)}>Search</a>
                        </div>
                    </div>
                    
                </div>
            </div>
        )

    }
}
export default View;
