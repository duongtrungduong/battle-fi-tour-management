import jQuery from 'jquery';
import { Form} from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'
import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'keyword':              new Textbox.ActionHelper(this.dispatcher, this.formId, 'keyword'),
            'status':      new Select.ActionHelper(this.dispatcher, this.formId, 'status'),
        }
    }
    load() {
        var _this = this
        if (__params.query.keyword != undefined) {
            this.helper("keyword").dispatchChangeValue(__params.query.keyword)
        }
        if (__params.query.status != undefined) {
            this.helper("status").dispatchChangeValue(__params.query.status)
        }

        _this.helper("status").dispatchSetChoices([
            {
                "value": "pending",
                "label": "Pending",
            },
            {
                "value": "accepted",
                "label": "Accepted",
            },
            {
                "value": "rejected",
                "label": "Rejected",
            },

        ])

    }   

    search(props) {

        var _this = this;
        var values = this.getValues(props.form)
        var url = '?id='+__params.query.id+'&'

        if ( values.keyword ) {
            url += 'keyword=' + encodeURIComponent(values.keyword) + '&'
        }

        if ( values.status ) {
            url += 'status=' + encodeURIComponent(values.status)
        }
        location.href = url
    }

}

export default Action;
