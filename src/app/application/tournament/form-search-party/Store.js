import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils'
import jQuery from 'jquery';
import { Form} from 'library-form';
import {Textbox} from 'library-form-textbox'
import {Select} from 'library-form-select'

class Store extends Form.Store {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initStoreHelpers() {
        this._storeHelpers = {
            'keyword':              new Textbox.StoreHelper(this.formId, 'keyword'),
        }
    }
}

export default Store;
