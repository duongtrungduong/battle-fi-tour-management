import jQuery from 'jquery';
import { Form} from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'
import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'keyword':              new Textbox.ActionHelper(this.dispatcher, this.formId, 'keyword'),
        }
    }
    load() {
        var _this = this
        if (__params.query.keyword != undefined) {
            this.helper("keyword").dispatchChangeValue(__params.query.keyword)
        }
    }   

    search(props) {

        var _this = this;
        var values = this.getValues(props.form)
        var url = '?id='+__params.query.id+'&'

        if ( values.keyword ) {
            url += 'keyword=' + encodeURIComponent(values.keyword) + '&'
        }
        location.href = url
    }

}

export default Action;
