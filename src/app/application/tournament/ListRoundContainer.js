import React, {Component} from 'react';
import {Container} from 'flux/utils';
import {Dispatcher} from 'flux';


import PageContainerView from 'app-container/View';
import PageContainerStore from 'app-container/Store';
import PageContainerAction from 'app-container/Action';

import FormDetailView from './form-detail/View'
import FormDetailStore from './form-detail/Store'
import FormDetailAction from './form-detail/Action'

import FormRoundDataView from './form-round-data/View'
import FormRoundDataStore from './form-round-data/Store'
import FormRoundDataAction from './form-round-data/Action'


const _dispatcher = new Dispatcher();
const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')

const _formDetailStore = new FormDetailStore(_dispatcher, 'form-detail')
const _formDetailAction = new FormDetailAction(_dispatcher, 'form-detail')

const _formRoundDataStore = new FormRoundDataStore(_dispatcher, 'form-round-data')
const _formRoundDataAction = new FormRoundDataAction(_dispatcher, 'form-round-data')

_pageContainerAction.setCategory("general")
_pageContainerAction.setMenu("tournament")

class ListRoundContainer extends Component {
    static getStores() {
        return [
            _pageContainerStore,
            _formDetailStore,
            _formRoundDataStore,
        ];
    }

    static calculateState(prevState) {
        return {
            pageContainer:    _pageContainerStore.getState(),
            formDetail:       _formDetailStore.getState(),
            formRoundData:    _formRoundDataStore.getState(),
        }
    }
    render() {
        return (
            <PageContainerView pageContainer={this.state.pageContainer} action={_pageContainerAction}>
                <div className="row mt-5">
                    <div className="col-xl-12 mb-5 mb-xl-0">
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <h4 style={{ fontWeight: "400" }}>Round Management</h4>
                                    </div>
                                    <div className="col-4 text-right">
                                        <a href={`/challenge?tour_id=${__params.query['id']}`} className="btn btn-sm btn-info" style={{color: "white"}}>View challenges</a>
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <FormDetailView
                                    action={_formDetailAction}
                                    data={this.state.formDetail}
                                /><br/>
                                <FormRoundDataView
                                    form={this.state.formRoundData}
                                    action={_formRoundDataAction}
                                />
                            </div>
                        </div>
                    </div>
                </div>  
            </PageContainerView>
        )
    }
}

export default Container.create(ListRoundContainer);
