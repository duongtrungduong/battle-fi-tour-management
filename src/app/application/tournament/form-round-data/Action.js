import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {Hidden} from 'library-form-hidden'
import {Wysiwyg} from 'library-wysiwyg'
import UserService from 'app-services/userServices'

import RoundFormAction from "./round_form/ActionHelper"
import RoundViewAction from "./round_view/ActionHelper"
import Constants from '../constants'
class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            "round_data": new RoundFormAction(this.dispatcher, this.formId, 'round_data'),
            'round_id': new Textbox.ActionHelper(this.dispatcher, this.formId, 'round_id'),
            'round_ids': new Select.ActionHelper(this.dispatcher, this.formId, 'round_ids'),
            "is_active": new Hidden.ActionHelper(this.dispatcher, this.formId, 'is_active'),
            "round_data_running": new RoundViewAction(this.dispatcher, this.formId, 'round_data_running'),
            "is_running": new Hidden.ActionHelper(this.dispatcher, this.formId, 'is_running'),
            "bracket_type": new Hidden.ActionHelper(this.dispatcher, this.formId, 'bracket_type'),
            
        }
    }

    changeValue(fieldId, newValue) {
        if (fieldId == 'round_ids') {
            if (newValue != '') {
                this.loadRoundData(newValue)
            }
        }
        this.helper(fieldId).dispatchChangeValue(newValue)
    }

    load() {
        var _this = this

        // Load Round List
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/tournament/round_list?id=' + __params.query.id,
            data:      {
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    const choices = result.data.records.map((data) => {
                        return { 'value': data.id, 'label': data.name }
                    })
                    _this.helper('round_ids').dispatchSetChoices(choices)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load the data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
        
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/tournament/get_round?id=' + __params.query.id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.helper("round_data").dispatchChangeValue(result.data.round_datas)
                    _this.helper("round_data").dispatchLoadTeams(result.data.teams_data)
                    _this.helper("round_id").dispatchChangeValue(result.data.round_id)
                    _this.helper("round_ids").dispatchChangeValue(result.data.round_active_id)
                    _this.helper("is_active").dispatchChangeValue(result.data.is_active)
                    _this.helper("round_data").dispatchTeamSelected(result.data.team_selected)
                    _this.helper("round_data_running").dispatchChangeValue(result.data.round_datas)
                    _this.helper("is_running").dispatchChangeValue(result.data.is_running)
                    _this.helper("bracket_type").dispatchChangeValue(result.data.bracket_type)
                    
                    if (result.data.round_active_id != 'round_1'){
                        console.log(result.data.round_id)
                        _this.loadRoundData(result.data.round_active_id)
                    }
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load the data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/account_jury/list',
            data:      {
                '_sort_key': "account__name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var account_jury_ids = []
                    result.data.records.map(function(val, key) {
                        account_jury_ids.push({
                            "value": val.account_jury_id,
                            "label": val.account_email + `(${val.account_email})`
                        })
                    })
                    _this.helper("round_data").dispatchSetJuryList(account_jury_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    loadRoundData(round_id) {
        var _this = this

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/tournament/get_round_info',
            data:      {
                'id': __params.query.id,
                'round_id': round_id
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.helper("round_data").dispatchChangeValue(result.data.round_datas)
                    _this.helper("round_data").dispatchLoadTeams(result.data.teams_data)
                    _this.helper("round_id").dispatchChangeValue(result.data.round_id)
                    _this.helper("is_active").dispatchChangeValue(result.data.is_active)
                    _this.helper("is_running").dispatchChangeValue(result.data.is_running)
                    _this.helper("round_data_running").dispatchChangeValue(result.data.round_datas)
                    _this.helper("round_data").dispatchLoadChallenges(result.data.challenge_datas)
                    _this.helper("bracket_type").dispatchChangeValue(result.data.bracket_type)
                    _this.helper("round_data").dispatchTeamSelected([])
                    _this.number_team = result.data.number_team
                    
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load the data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false
        if (!hasError) {
            this.dispatchFormLoadStart()
            jQuery.ajax({
                headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/tournament/update_round_data?id=' + __params.query.id,
                dataType:     'json',
                data:         {
                    "round_data": JSON.stringify(values.round_data.toJS()),
                    "round_id": values.round_ids
                },
                success:      function(result) {
                    if (result.status == 'ok') {
                        _this.dispatchFormAddMessage("info", "The record has been update successfully!")
                        _this.dispatchFormSubmitSuccess(values)
                        // _this.resetForm()
                        location.reload()
                    }
                    else {
                        if (result.data.message != undefined) {
                            _this.dispatchFormAddMessage("error", result.data.message)
                        }
                        else {
                            _this.dispatchFormAddMessage("error", "Can't update the record")
                        }
                        _this.dispatchFormSubmitFail(values)
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage("error", "Can't communicate to API")
                    _this.dispatchFormSubmitFail(values)
                }
            })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
    updateTeamSelected(team_selected) {
        //this.helper("round_data").dispatchTeamSelected(team_selected)
    }

    auto_arrange(e, fields) {
        e.preventDefault()
        var _this = this
        var bracket_auto_fill = Constants
        var values = this.getValues(fields)
        var round_data = values.round_data.toJS()
        round_data = round_data.sort(function (a, b) {
            if (a.challenge_id < b.challenge_id) {
              return -1;
            }
            if (a.challenge_id > b.challenge_id) {
              return 1;
            }
            return 0;
          });
        if (bracket_auto_fill[this.number_team]) {
            var match_select = bracket_auto_fill[this.number_team]
            Object.keys(match_select).map(function(match_index, key){
                var match_data = match_select[match_index]
                if (round_data[match_index-1]) {
                    var item = round_data[match_index-1]
                    console.log(item)
                    item['type_round_match']  = match_data['type']
                    if (round_data[match_data['id'] - 1]) {
                        var next_item = round_data[match_data['id'] - 1]
                        item['next_challenge_id'] = next_item['challenge_id']
                    }
                    if (match_data['id'] == "") {
                        item['next_challenge_id'] = ""
                    }
                    round_data[match_index-1] = item
                    console.log(round_data[match_index-1])
                }
            })
        }
        _this.helper("round_data").dispatchChangeValue(round_data)
        
    }
    active_round(e, form) {
        var _this = this;
        e.preventDefault()
        var values = this.getValues(form)
        var round_id = values.round_ids
        
    }
}

export default Action;
