import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Select} from 'library-form-select'
import {Hidden} from 'library-form-hidden'
import {Textarea} from 'library-form-textarea'
import RoundFormStore from "./round_form/StoreHelper"
import RoundViewStore from "./round_view/StoreHelper"
class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            "round_data": new RoundFormStore(this.formId, 'round_data'),
            'round_id': new Textbox.StoreHelper(this.formId, 'round_id'),
            'round_ids': new Select.StoreHelper(this.formId, 'round_ids'),
            'is_active': new Hidden.StoreHelper(this.formId, 'is_active'),
            "round_data_running": new RoundViewStore(this.formId, 'round_data_running'),
            "is_running": new Hidden.StoreHelper(this.formId, 'is_running'),
            "bracket_type": new Hidden.StoreHelper(this.formId, 'bracket_type'),
        }
    }
}

export default Store;
