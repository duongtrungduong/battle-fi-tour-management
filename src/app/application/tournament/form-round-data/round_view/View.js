import React, { Component } from 'react';
import Select from 'react-select';
import './style.css'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
class View extends Component {
    render() {
        var _this = this;
        return (
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th className='' style={{ color: '#333'}}> Challenge </th>
                            <th width="10%" className='' style={{ color: '#333'}}>  Start Time </th>
                            <th width="15%" className='' style={{ color: '#333'}}> Team 1</th>
                            <th width="15%" className='' style={{ color: '#333'}}> Team 2</th>
                            <th width="5%" style={{ color: '#333'}}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            _this.props.data.get('value').map(function (record, i) {
                                return (
                                    <tr key={_this.props.data.get('id') + "-row-" + i}>
                                        <td>
                                            {record.challenge_name}
                                        </td>
                                        <td>
                                            {moment(new Date(parseInt(record.start_time)*1000)).format("HH:mm D/MM/Y")}
                                        </td>
                                        <td>
                                            {_this.renderVictory(record.winner, record.team_1)}
                                            &nbsp;
                                            {record.team_1_label}
                                        </td>
                                        <td>
                                            {_this.renderVictory(record.winner, record.team_2)}
                                            &nbsp;
                                            {record.team_2_label}
                                            
                                        </td>
                                        <td>
                                            <a target="__blank" className="btn btn-sm btn-info" href={`${__params.config['ui.endpoint']}/challenge/get?id=${record.challenge_id}`}>View</a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
    renderVictory(winner_id , team_id) {
        console.log(winner_id, team_id)
        if(winner_id != "") {
            if (winner_id === team_id) {
                return (<span className="badge badge-success" style={{width: 50}}>WINNER</span>)
            }
            return (<span className="badge badge-danger" style={{width: 50}}>LOSER</span>)
        }
        return <span></span>
    }
}

export default View;
