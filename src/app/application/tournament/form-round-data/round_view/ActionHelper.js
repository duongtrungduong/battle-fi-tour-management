import { Field } from 'library-field';

class ActionHelper extends Field.ActionHelper {
    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-value',
            value:                      value
        })
    }
    dispatchLoadTeams(teams) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-teams',
            teams:                      teams
        })
    }
    dispatchTeamSelected(teams) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'selected-teams',
            teams:                      teams
        })
    }
}

export default ActionHelper;
