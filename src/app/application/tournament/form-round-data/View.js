import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {Hidden} from 'library-form-hidden'
import RoundFormView from "./round_form/View"
import RoundView from "./round_view/View"
import './styles.css'

class View extends Form.View {
    constructor(props) {
        super(props)
    }
    render() {
        var _this = this
        var fields = this.props.form.get("fields")
        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form className="form" onSubmit={e => e.preventDefault()}>
                    <div>
                        <div className="form-group">
                            <div style={{marginBottom: 10, display: "inline"}}>
                                <Select.View
                                    data={this.props.form.get('fields').get('round_ids')}
                                    action={this.props.action}
                                    style={{fontSize: 12}}
                                />
                                {
                                    (fields.get("bracket_type").get("value") == 3) ? 
                                    <button className="btn btn-sm btn-success"
                                        style={{float:"right"}} 
                                        onClick={ e => this.props.action.auto_arrange(e, _this.props.form)} title="Click here to auto update bracket">Arrange</button>                              
                                    : false
                                }           
                            </div>
                            <br/>
                            <br/>
                            {
                                (fields.get("is_running").get("value") == true) ? 
                                <RoundView
                                    data={this.props.form.get('fields').get('round_data_running')}
                                    action={this.props.action}
                                    
                                />
                                :
                                <RoundFormView
                                    data={this.props.form.get('fields').get('round_data')}
                                    action={this.props.action}
                                    
                                />
                            }
                            
                        </div>
                    </div>
                    <div>
                        {
                            this.props.form.get('messages').map(function(message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                                    }
                                    return (<div className="alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>
                    
                    {
                        (fields.get("is_running").get("value") == true) ? 
                        false
                        :
                        <div className="form-actions">
                            <div className="row">
                                <div className="col-md-9">
                                    <button className="btn btn-sm btn-primary" 
                                        onClick={ e => this.props.action.submit(this.props.form, e)}>Submit</button>
                                </div>
                            </div>
                        </div>
                    }
                    
                </form>
            </BlockUi>
        )
    }
}

export default View;
