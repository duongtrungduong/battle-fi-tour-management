import { Field } from 'library-field';

class ActionHelper extends Field.ActionHelper {
    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-value',
            value:                      value
        })
    }
    dispatchLoadTeams(teams) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-teams',
            teams:                      teams
        })
    }
    dispatchTeamSelected(teams) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'selected-teams',
            teams:                      teams
        })
    }
    dispatchSetJuryList(juries) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-juries',
            juries:                      juries
        })
    }
    dispatchLoadChallenges(challenges) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-challenges',
            challenges:                      challenges
        })
    }

    
}

export default ActionHelper;
