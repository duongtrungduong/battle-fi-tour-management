import Immutable from 'immutable';
import { Field } from 'library-field';

class StoreHelper extends Field.StoreHelper {
    getInitialState() {
        return Immutable.Map({
            'id':               this.fieldId,
            'value':            Immutable.List([]),
            'error':            '',
            'teams':            Immutable.List([]),
            'team_selected':    Immutable.List([]),
            'account_jury_list':     Immutable.List([]),
            'challenges':     Immutable.List([]),
            
        })
    }

    reduceFieldClearValue(state) {
        return state.set('value', Immutable.List([]))
    }

    reduceFieldChangeValue(state, value) {
        return state.set('value', Immutable.List(value))
    }

    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case 'set-value':
                    state = state.set('value', Immutable.List(action.value))
                    return state
                case 'set-teams':
                    state = state.set('teams', Immutable.List(action.teams))
                    return state
                case 'selected-teams':
                    state = state.set('team_selected', Immutable.List(action.teams))
                    return state
                case 'set-juries':
                    state = state.set('account_jury_list', Immutable.List(action.juries))
                    return state
                case 'set-challenges':
                    state = state.set('challenges', Immutable.List(action.challenges))
                    return state
                default:
                    state = super.reduce(state, action)
            }
        }
        return state
    }
}

export default StoreHelper;
