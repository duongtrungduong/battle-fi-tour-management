import React, { Component } from 'react';
import Select from 'react-select';
import './style.css'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
class View extends Component {
    changeField(fieldName, index, data) {
        var _this = this
        var value = this.props.data.get('value').toJS();
        value[index][fieldName] = data;
        this.props.action.changeValue(this.props.data.get('id'), value);
    }

    addNewRow() {
        var value = this.props.data.get('value').toJS();
        value.push({
            'challenge_name': '',
            'challenge_desc': '',
            'start_time': new Date(),
            'team_1': '',
            'team_2': '',
            'next_challenge_id': '',
            'type_round_match': 'win',
            'bracket_type': "1",  ///// 1 singe, 2 round robin, 3 double
            "round_name": ""
        });
        this.props.action.changeValue(this.props.data.get('id'), value);
    }
    setSelectedTeam(old_team_id, team_id, fieldName, fieldIndex) {  
        var team_selected = this.props.data.get('team_selected').toJS();
        var index = -1
        team_selected.map(function(val, key) {
            if (parseInt(val) == old_team_id) {
                index = key
            }
        })
        if (index >= 0) {
            team_selected.splice(index, 1)
        }
        if (team_id != "0") {
            team_selected.push(team_id)
        }
        this.props.action.updateTeamSelected(team_selected)
        this.changeField(fieldName, fieldIndex, team_id)

    }
    deleteRow(index) {
        var value = this.props.data.get('value').toJS();
        value.splice(index, 1)
        this.props.action.changeValue(this.props.data.get('id'), value);
    }
    onChangeTime(e, i) {
        var time = moment(e).format("X")
        const currentTimeAccept = moment(moment(new Date()).add('30', 'minutes')).format('X')
        this.props.action.dispatchFormClearMessages()
        if (currentTimeAccept < time) {
            this.changeField("start_time", i, time)
        } else {
            this.changeField("start_time", i, currentTimeAccept)
            this.props.action.dispatchFormAddMessage('error', `Time Start challenge ${i+1} must be large than current time 30 minutes`)
        }

    }
    render() {
        var _this = this;
        var teams = _this.props.data.get('teams').toJS()
        var account_jury_list = _this.props.data.get('account_jury_list').toJS()
        
        var team_selected = this.props.data.get('team_selected').toJS();
        var challenges = _this.props.data.get('challenges').toJS()
        console.log(_this.props.data.get('value').toJS())
        return (
            <div>
                <table className="table table-bordered table-custome-css">
                    <thead>
                        <tr>
                            <th width="10%" className='' style={{ color: '#333'}}> Challenge </th>
                            <th width="10%" className='' style={{ color: '#333'}}> Jury </th>
                            <th width="5%" className='' style={{ color: '#333'}}> Start Time </th>
                            <th width="10%" className='' style={{ color: '#333'}}> Team 1</th>
                            <th width="10%" className='' style={{ color: '#333'}}> Team 2</th>
                            <th width="10%" className='' style={{ color: '#333'}}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            _this.props.data.get('value').map(function (record, i) {
                                return (
                                    <tr key={_this.props.data.get('id') + "-row-" + i}>
                                        <td>
                                            <input className="form-control" id={`challenge_name-${i}`} 
                                                type="text" 
                                                value={record.challenge_name}
                                                onChange={e => _this.changeField("challenge_name", i, e.target.value)}
                                            /> 
                                        </td>
                                       
                                        <td>
                                            <select className="form-control" id={`jury_id-${i}`} 
                                                type="text" 
                                                value={record.jury_id} 
                                                onChange={e => _this.setSelectedTeam(record.jury_id, e.target.value, "jury_id", i)}
                                            >
                                                <option value='0'></option>
                                                {
                                                    account_jury_list.map(function(choice, i) {
                                                        return <option value={choice.value} key={choice.value + '-' + i}>{choice.label}</option>
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td>
                                            <div className="datepickroofier-layout">
                                                <DatePicker
                                                    id={`start_time-${i}`}
                                                    selected={new Date(parseInt(record.start_time)*1000)} 
                                                    onChange={e => _this.onChangeTime(e, i)}
                                                    showTimeSelect
                                                    timeFormat="HH:mm"
                                                    timeIntervals={15}
                                                    dateFormat='dd/MM/yyyy HH:mm'
                                                    className={"form-control"}
                                                    minDate={new Date()}
                                                    maxDate={(d => new Date(d.getFullYear() + 1, d.getMonth(), d.getDate()))(new Date)}
                                                />
                                            </div>
                                        </td>
                                        <td>
                                            <select className="form-control" id={`team_1-${i}`} 
                                                type="text" 
                                                value={record.team_1} 
                                                onChange={e => _this.setSelectedTeam(record.team_1, e.target.value, "team_1", i)}
                                            >
                                                <option value='0'></option>
                                                {
                                                    teams.map(function(choice, i) {
                                                        var isDisabled = team_selected.includes(`${choice.id}`);
                                                        return <option disabled={(isDisabled) ? "disabled" : ""} value={choice.id} key={choice.id + '-' + i}>{choice.name}</option>
                                                    })
                                                }
                                            </select>
                                        </td>                                        
                                        <td>
                                            <select className="form-control" id={`team_2-${i}`} 
                                                type="text" 
                                                value={record.team_2} 
                                                onChange={e => _this.setSelectedTeam(record.team_2, e.target.value, "team_2", i)}
                                            >
                                                <option value='0'></option>
                                                {
                                                    teams.map(function(choice, i) {
                                                        var isDisabled = team_selected.includes(`${choice.id}`);
                                                        return <option disabled={(isDisabled) ? "disabled" : ""} value={choice.id} key={choice.id + '-' + i}>{choice.name}</option>
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td>
                                            {
                                                (record.bracket_type == 3) ? 
                                                <div>
                                                    <select className="form-control" id={`type-${i}`} 
                                                        type="text" 
                                                        value={record.type_round_match} 
                                                        onChange={e => _this.setSelectedTeam(record.type_round_match, e.target.value, "type_round_match", i)}
                                                    >
                                                        <option value='win'>Win</option>
                                                        <option value='lose'>Lose</option>
                                                    
                                                    </select>
                                                    <select className="form-control" id={`next_challenge_id-${i}`} 
                                                        type="text" 
                                                        value={record.next_challenge_id} 
                                                        onChange={e => _this.setSelectedTeam(record.next_challenge_id, e.target.value, "next_challenge_id", i)}
                                                    >
                                                        <option value=''></option>
                                                        {
                                                            challenges.map(function(choice, i) {
                                                                return <option value={choice.id} key={choice.id + '-' + i}>{choice.name}</option>
                                                            })
                                                        }
                                                    </select>
                                                    <input className="form-control" id={`round_name-${i}`} 
                                                        type="text" 
                                                        defaultValue={record.round_name} 
                                                        onChange={e => _this.changeField("round_name", i, e.target.value)}
                                                    /> 
                                                </div>: false
                                            }
                                        </td>
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
            </div>
        )
    }
}

export default View;
