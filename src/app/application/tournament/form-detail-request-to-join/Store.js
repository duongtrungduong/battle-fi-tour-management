import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {Checkbox} from 'library-form-checkbox'
import {MultiSelect} from 'library-form-multiselect'
import {UploadFile} from 'library-form-upload-file'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    initStoreHelpers() {
        this._storeHelpers = {

        }
    }
    getInitialState() {
        return Immutable.Map({
            'name': "",
            "game_kind_icon_url": "",
            "game_kind_name": "",
            "team_size": "",
            "number_team": "",
            "number_team_join": "",
        })
    }
    reduce(state, action) {
        var id = this.id
        state = super.reduce(state, action)
        switch (action.type) {
            case this.id + '/load-data':
                state = state.set('name', action.name)
                state = state.set('game_kind_icon_url', action.game_kind_icon_url)
                state = state.set('game_kind_name', action.game_kind_name)
                state = state.set('number_team', action.number_team)
                state = state.set('team_size', action.team_size)
                state = state.set('number_team_join', action.number_team_join)
                return state
        }
        return state
    }
}

export default Store;
