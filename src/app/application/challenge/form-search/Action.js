import jQuery from 'jquery';
import { Form} from 'library-form'
import {Select} from 'library-form-select'
import {Textbox} from 'library-form-textbox'
import {DatetimePicker} from 'library-form-datetimepicker'
import moment from "moment"

import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':              new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'game_kind_id':      new Select.ActionHelper(this.dispatcher, this.formId, 'game_kind_id'),
            'start_time':         new DatetimePicker.ActionHelper(this.dispatcher, this.formId, 'start_time'),
            'end_time':         new DatetimePicker.ActionHelper(this.dispatcher, this.formId, 'end_time'),
            'status':      new Select.ActionHelper(this.dispatcher, this.formId, 'status'),
        }
    }
    toDateTime(secs) {
        return new Date(secs * 1000)
    }
    load() {
        var _this = this
        if (__params.query.name != undefined) {
            this.helper("name").dispatchChangeValue(__params.query.name)
        }
        if (__params.query.game_kind_id != undefined) {
            this.helper("game_kind_id").dispatchChangeValue(__params.query.game_kind_id)
        }
        if (__params.query.start_time != undefined) {
            this.helper("start_time").dispatchChangeValue( this.toDateTime(__params.query.start_time))
        }
        if (__params.query.end_time != undefined) {
            this.helper("end_time").dispatchChangeValue( this.toDateTime(__params.query.end_time))
        }
        if (__params.query.status != undefined) {
            this.helper("status").dispatchChangeValue(__params.query.status)
        }

        _this.helper("status").dispatchSetChoices([
            {"value": 0, "label": "Publish"},
            {"value": 5, "label": "Started"},
            {"value": 1, "label": "Finish"},
            {"value": 2, "label": "Canceled"},

        ])
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/game_kind/list',
            data:      {
                '_sort_key': "name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var game_kind_ids = []
                    result.data.records.map(function(val, key) {
                        game_kind_ids.push({
                            "value": val.id,
                            "label": val.name
                        })
                    })
                    _this.helper("game_kind_id").dispatchSetChoices(game_kind_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

    }   

    search(props) {

        var _this = this;
        var values = this.getValues(props.form)
        var url = '?'

        if (__params.query.tour_id != undefined) {
            url += 'tour_id=' + __params.query.tour_id + '&'
        }
        if ( values.name ) {
            url += 'name=' + encodeURIComponent(values.name) + '&'
        }

        if ( values.game_kind_id ) {
            url += 'game_kind_id=' + encodeURIComponent(values.game_kind_id)+ '&'
        }
        if ( values.status ) {
            url += 'status=' + encodeURIComponent(values.status)+ '&'
        }
        if ( values.start_time ) {
            url += 'start_time=' + encodeURIComponent(values.start_time.getTime() / 1000)+ '&'
        }
        if ( values.end_time ) {
            url += 'end_time=' + encodeURIComponent(values.end_time.getTime() / 1000)
        }
        
        location.href = url
    }

}

export default Action;
