import Immutable from 'immutable';
import {ReduceStore} from 'flux/utils'
import jQuery from 'jquery';
import { Form} from 'library-form';
import {Textbox} from 'library-form-textbox'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'

class Store extends Form.Store {
    constructor(dispatcher, formId = 'form-search') {
        super(dispatcher, formId)
    }
    initStoreHelpers() {
        this._storeHelpers = {
            'name':              new Textbox.StoreHelper(this.formId, 'name'),
            'game_kind_id':      new Select.StoreHelper(this.formId, 'game_kind_id'),
            'start_time':         new DatetimePicker.StoreHelper(this.formId, 'start_time'),
            'end_time':         new DatetimePicker.StoreHelper(this.formId, 'end_time'),
            'status':          new Select.StoreHelper(this.formId, 'status'),
        }
    }
}

export default Store;
