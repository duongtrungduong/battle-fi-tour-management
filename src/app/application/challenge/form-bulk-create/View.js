import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import ChallengeView from "./challenge/View"
class View extends Form.View {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form className="form" onSubmit={e => e.preventDefault()}>
                    <div>
                        <div className="form-group">
                            <label className="control-label">Challenges</label>
                            <ChallengeView
                                data={this.props.form.get('fields').get('challenges')}
                                action={this.props.action}
                            />
                        </div>
                    </div>
                    <div>
                        {
                            this.props.form.get('messages').map(function(message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                                    }
                                    return (<div className="alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>

                    <div className="form-actions">
                        <div className="row">
                            <div className="col-md-9">
                                <button className="btn btn-sm btn-primary" onClick={ e => this.props.action.submit(this.props.form, e)}>Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </BlockUi>
        )
    }
}

export default View;
