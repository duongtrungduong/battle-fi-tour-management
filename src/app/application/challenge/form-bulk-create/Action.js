import {Form}  from 'library-form'
import UserService from 'app-services/userServices'
import ChallengeAction from "./challenge/ActionHelper"
class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'challenges':               new ChallengeAction(this.dispatcher, this.formId, 'challenges'),
        }
    }

    load() {
        var _this = this
        _this.helper("challenges").dispatchLoadChallengeType([
            {
                "value": "public",
                "label": "Public",
            },
            {
                "value": "private",
                "label": "Private",
            },
        ])        
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/game_kind/list',
            data:      {
                '_sort_key': "name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var game_kind_ids = []
                    result.data.records.map(function(val, key) {
                        game_kind_ids.push({
                            "value": val.id,
                            "label": val.name
                        })
                    })
                    _this.helper("challenges").dispatchLoadGameKind(game_kind_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/account_jury/list',
            data:      {
                '_sort_key': "account__name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var account_ids = []
                    result.data.records.map(function(val, key) {
                        account_ids.push({
                            "value": val.account_id,
                            "label": val.account_email + ` (${val.account_name})`,
                        })
                    })
                    _this.helper("challenges").dispatchLoadAccountJury(account_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (!hasError) {
                var dataSubmit = {}
                dataSubmit['challenges'] = JSON.stringify(values.challenges.toJS())
                this.dispatchFormLoadStart()
                jQuery.ajax({
                    headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                    type:         'POST',
                    url:          __params.config['api.endpoint'] + '/challenge/bulk_create',
                    dataType:     'json',
                    data:         dataSubmit,
                    success:      function(result) {
                        if (result.status == 'ok') {
                            _this.dispatchFormAddMessage("info", "The record has been created successfully!")
                            _this.dispatchFormSubmitSuccess(values)
                            _this.resetForm()
                        }
                        else {
                            if (result.data.message != undefined) {
                                _this.dispatchFormAddMessage("error", result.data.message)
                            }
                            else {
                                _this.dispatchFormAddMessage("error", "Can't update the record")
                            }
                            _this.dispatchFormSubmitFail(values)
                        }
                    },
                    error:        function(data) {
                        _this.dispatchFormAddMessage("error", "Can't communicate to API")
                        _this.dispatchFormSubmitFail(values)
                    }
                })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
