import { Field } from 'library-field';

class ActionHelper extends Field.ActionHelper {
    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-value',
            value:                      value
        })
    }
    dispatchLoadGameKind(game_kind) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-game-kind',
            game_kind:                  game_kind
        })
    }
    dispatchLoadAccountJury(account_id) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-account-id',
            account_id:                  account_id
        })
    }
    dispatchLoadChallengeType(challenge_type) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-challenge-type',
            challenge_type:              challenge_type
        })
    }
}

export default ActionHelper;
