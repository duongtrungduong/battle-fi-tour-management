import Immutable from 'immutable';
import { Field } from 'library-field';

class StoreHelper extends Field.StoreHelper {
    getInitialState() {
        return Immutable.Map({
            'id':               this.fieldId,
            'value':            Immutable.List([]),
            'error':            '',
            'game_kind':        Immutable.List([]),
            'account_id':        Immutable.List([]),
            'challenge_type':             Immutable.List([]),
        })
    }

    reduceFieldClearValue(state) {
        return state.set('value', Immutable.List([]))
    }

    reduceFieldChangeValue(state, value) {
        return state.set('value', Immutable.List(value))
    }

    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case 'set-value':
                    state = state.set('value', Immutable.List(action.value))
                    return state
                case 'set-game-kind':
                    state = state.set('game_kind', Immutable.List(action.game_kind))
                    return state
                case 'set-account-id':
                    state = state.set('account_id', Immutable.List(action.account_id))
                    return state
                case 'set-challenge-type':
                    state = state.set('challenge_type', Immutable.List(action.challenge_type))
                    return state
                default:
                    state = super.reduce(state, action)
            }
        }
        return state
    }
}

export default StoreHelper;
