import React, { Component } from 'react';
import Select from 'react-select';
import './style.css'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import userServices from '../../../../services/userServices';
class View extends Component {
    changeField(fieldName, index, data) {
        var _this = this
        var value = this.props.data.get('value').toJS();
        value[index][fieldName] = data;
        this.props.action.changeValue(this.props.data.get('id'), value);
    }

    addNewRow(e) {
        var value = this.props.data.get('value').toJS();
        var i = value.length + 1
        value.push({
            'name': 'Challenge #'+i,
            'desc': '',
            'start_time': moment(new Date()).format("X"),
            'price_reward': '0',
            'kind_reward': 'battle_power',
            'game_kind': '',
            'challenge_type': 'public',
            'account_id': userServices.getId(),
            'price_reward': 0,
            'stream_url': '',

        });
        this.props.action.changeValue(this.props.data.get('id'), value);
    }
    deleteRow(index) {
        var value = this.props.data.get('value').toJS();
        value.splice(index, 1)
        this.props.action.changeValue(this.props.data.get('id'), value);
    }
    onChangeTime(e, i) {
        var time = moment(e).format("X")
        this.changeField("start_time", i, time)
    }
    render() {
        var _this = this;
        var game_kinds = _this.props.data.get('game_kind').toJS()
        var account_ids = _this.props.data.get('account_id').toJS()
        var challenge_type = _this.props.data.get('challenge_type').toJS()
        return (
            <div>
                <table className="table table-bordered">
                    <thead>
                        <tr>
                            <th width="10%" className='' style={{ color: '#333'}}>Name </th>
                            <th width="10%" className='' style={{ color: '#333'}}>Description </th>
                            <th width="5%" className='' style={{ color: '#333'}}>Start Time </th>
                            <th width="10%" className='' style={{ color: '#333'}}>Game Kind</th>
                            <th width="10%" className='' style={{ color: '#333'}}>Type</th>
                            <th width="10%" className='' style={{ color: '#333'}}>Price Reward ($)</th>
                            <th width="10%" className='' style={{ color: '#333'}}>Validator</th>
                            <th width="10%" className='' style={{ color: '#333'}}>Stream</th>
                            <th width="5%" className='' style={{ color: '#333'}}></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            _this.props.data.get('value').map(function (record, i) {
                                console.log(record.kind_reward)
                                return (
                                    <tr key={_this.props.data.get('id') + "-row-" + i}>
                                        <td>
                                            <input className="form-control" id={`name-${i}`} 
                                                type="text" 
                                                defaultValue={record.name} 
                                                onChange={e => _this.changeField("name", i, e.target.value)}
                                            /> 
                                        </td>
                                        <td>
                                            <textarea className="form-control" id={`desc-${i}`} 
                                                defaultValue={record.desc} 
                                                onChange={e => _this.changeField("desc", i, e.target.value)}
                                            />
                                        </td>
                                        <td>
                                            
                                            <div className="datepickroofier-layout">
                                                <DatePicker
                                                    id={`start_time-${i}`}
                                                    selected={new Date(parseInt(record.start_time)*1000)} 
                                                    onChange={e => _this.onChangeTime(e, i)}
                                                    showTimeSelect
                                                    timeFormat="HH:mm"
                                                    timeIntervals={15}
                                                    dateFormat='dd/MM/yyyy HH:mm'
                                                    className={"form-control"}
                                                />
                                            </div>
                                        </td>
                                        <td>
                                            <select className="form-control" id={`game_kind-${i}`} 
                                                type="text" 
                                                value={record.game_kind} 
                                                onChange={e => _this.changeField('game_kind', i, e.target.value)}
                                            >
                                                <option value='0'></option>
                                                {
                                                    game_kinds.map(function(choice, i) {
                                                        return <option value={choice.value} key={choice.value + '-' + i}>{choice.label}</option>
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td>
                                            <select className="form-control" id={`challenge_type-${i}`} 
                                                type="text" 
                                                value={record.challenge_type} 
                                                onChange={e => _this.changeField('challenge_type', i, e.target.value)}
                                            >
                                                {
                                                    challenge_type.map(function(choice, i) {
                                                        return <option value={choice.value} key={choice.value + '-' + i}>{choice.label}</option>
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td>
                                            <select className="form-control" id={`kind_reward-${i}`} 
                                                type="text" 
                                                value={record.kind_reward} 
                                                onChange={e => _this.changeField('kind_reward', i, e.target.value)}
                                            >
                                                <option value={"battle_power"} key={'battle_power-' + i}>Battle Power</option>
                                                <option value={"direct"} key={'direct_reward-' + i}>Direct (Token or BUSD)</option>
                                            </select>
                                            {
                                                record.kind_reward == "direct"?
                                                <input className="form-control" id={`price_reward-${i}`} 
                                                    type="text" 
                                                    defaultValue={record.price_reward} 
                                                    onChange={e => _this.changeField("price_reward", i, e.target.value)}
                                                /> : false
                                            }
                                            
                                        </td>
                                        <td>
                                            <select className="form-control" id={`account_id${i}`} 
                                                type="text" 
                                                value={record.account_ids} 
                                                onChange={e => _this.changeField('account_id', i, e.target.value)}
                                                disabled={ userServices.getPermission() == 'jury' }
                                            >
                                                <option value='0'></option>
                                                {
                                                    account_ids.map(function(choice, i) {
                                                        return <option selected={ userServices.getId() == choice.value ? true : null  } value={choice.value} key={choice.value + '-' + i}>{choice.label}</option>
                                                    })
                                                }
                                            </select>
                                        </td>
                                        <td>
                                            <input className="form-control" id={`stream_url-${i}`} 
                                                type="text" 
                                                defaultValue={record.stream_url} 
                                                onChange={e => _this.changeField("stream_url", i, e.target.value)}
                                            /> 
                                        </td>
                                        <td>
                                            <a style={{color: "#fff"}} className="btn btn-sm btn-danger" onClick={e => _this.deleteRow(i)}>Remove</a>
                                        </td>
                                    </tr>
                                )
                            })
                        }
                        <tr>
                            <td colSpan={9} style={{textAlign: "right"}}>
                                <a style={{color: "#fff"}} className="btn btn-sm btn-primary" onClick={e => _this.addNewRow(e)}>Add</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        )
    }
}

export default View;
