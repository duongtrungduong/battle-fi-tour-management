import Immutable from 'immutable';
import {Form}  from 'library-form'
import ChallengeStore from "./challenge/StoreHelper"

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'challenges':               new ChallengeStore(this.formId, 'challenges'),
        }
    }
}

export default Store;
