import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import moment from "moment"
import UserService from 'app-services/userServices'
import "./style.css"
class View extends LibraryDataTable.View {
    //jury
    checkRoleDoAction(record){
        var permission = UserService.getPermission()
        var id = UserService.getId()
        if ( permission == 'admin' || permission == 'mod')
            return true
        if ( permission == 'jury' && record.account_id == id)
            return true
        return false
    }
    renderTableHeader() {
        return <thead className='thead-light'>
            <tr>
                <th scope="col" width="10%">Challenge</th>
                <th scope="col" width="10%">Team 1</th>
                <th scope="col" width="10%">Team 2</th>
                <th scope="col" width="10%">Validator</th>
                <th scope="col" width="10%">Start Time</th>
                <th scope="col" width="10%">Status</th>
                <th scope="col" width="10%"></th>
            </tr>
        </thead>
    }
    renderLableWinLose(team_id, team_win){
        if ( team_win == '')
            return ''
        if (team_id == team_win){
            return <strong style={{fontSize: '8px',  color:'green'}}>VICTORY</strong>
        }else{
            return <strong style={{fontSize: '8px', color: 'red'}}>DEFEAT</strong>
        }
    }
    renderTableBody() {
        var _this = this;
        var permission = UserService.getPermission()
        var profile = UserService.getProfile()
        return (
            <tbody className="list">
                {
                    this.props.dataTable.get('records').map(function(record, i) {
                        var tmp = new Date(record.start_game_time*1000)
                        var start_time = moment(tmp).format("HH:mm DD/MM/Y")
                        return(
                      
                            <tr key={"record-" + i} scope="row">
                                <td>
                                <img className="game-kind-icon" src={record.game_kind_icon} width="20px"/>
                                    &nbsp;
                                    {record.is_public == false ? <i style={{color: 'black'}}className="ni ni-lock-circle-open"></i> : null}
                                    &nbsp;
                                    {record.name}
                                </td>
                                <td>{ record.team_1_name } { _this.renderLableWinLose(record.team_1_id, record.winner) }</td>
                                <td>{ record.team_2_name} { _this.renderLableWinLose(record.team_2_id, record.winner) }</td>
                                <td>{ record.jury_name }</td>
                                <td>
                                    {start_time}
                                </td>
                                <td>{ _this.renderStatusData(record.status) }</td>
                                <td style={{textAlign: "right"}}>
                                    {
                                        ([0, 3, 4].includes(record.status) && _this.checkRoleDoAction(record))?
                                        <a className="btn btn-ssm btn-primary" onClick ={e => _this.props.action.copylink(record.id, record.uuid)} href="javascript:;">Invite link</a>
                                        :false
                                    }
                                    {
                                        _this.renderButtonStatus(record, profile, permission)
                                    }
                                    {
                                        //publish stated
                                        ([0, 5].includes(record.status) && _this.checkRoleDoAction(record))?
                                        <a className="btn btn-ssm btn-primary" href={`/challenge/update?id=${record.id}`}>Edit</a>
                                        :false
                                    }
                                    {
                                        ([0, 3, 4, 5, 7].includes(record.status) && _this.checkRoleDoAction(record))?
                                        <a className="btn btn-ssm btn-dark" onClick ={e => { if (window.confirm('Do you want to cancel this challenge?'))  _this.props.action.cancel(e, record.id)}} href="javascript:;">Cancel</a>
                                        :false
                                    }
                                    {
                                        (record.status == 0 &&  _this.checkRoleDoAction(record)) ?
                                        <a className="btn btn-ssm btn-danger" onClick={(e) => { if (window.confirm('Do you want to delete this challenge?')) _this.props.action.delete(e, record.id)}} href="javascript:;">Delete</a>
                                        :false
                                    }
                                </td>
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }
    renderButtonStatus(record, profile, permission) {
        var _this = this
        if (record.status == 0 &&  _this.checkRoleDoAction(record)) {
            // start challenge
            return (
                <a className={`btn btn-ssm btn-info`}
                    style={{cursor: ` ${record.is_disabled ? "not-allowed" : "pointer"} `}}
                    onClick={ e => _this.props.action.start(e, record.id, record.is_disabled)} href="javascript:;">Start</a>
            )
        }
        if(permission == 'admin' || permission == 'mod' || profile['id'] == record.account_id) {
            // CHALLENGE_STATUS_PUBLISH = 0 
            // CHALLENGE_STATUS_FINISH = 1
            // CHALLENGE_STATUS_CANCELLED = 2
            // CHALLENGE_STATUS_DRAFT = 3
            // CHALLENGE_STATUS_OPENING = 4
            // CHALLENGE_STATUS_STARTED = 5
            // CHALLENGE_STATUS_END = 6 ## This status fire when game is end and move to voting
            // CHALLENGE_STATUS_HOLDING = 7
            if (record.status == 5 || record.status == 6) {
                // start challenge
                return (<a className="btn btn-ssm btn-success" onClick={ e => _this.props.formModalEndChallenge.openForm(e, record.id)} href="javascript:;">Finish</a>)
            }
           
        }
        return false
    }

    renderStatusData(status) {
        var _this = this
        if (status == 1) {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Finished</span>
        } else if (status == 0) {
            return <span className='badge badge-info' style={{fontSize: 13, fontWeight: 600}}>Publish</span>
        } else if (status == 2) {
            return <span className='badge badge-danger' style={{fontSize: 13, fontWeight: 600}}>Canceled</span>
        }
        else if (status == 5) {
            return <span className='badge badge-warning' style={{fontSize: 13, fontWeight: 600}}>Started</span>
        } else {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>End</span>
        }
    }

    renderGameModData(type_bo) {
        var _this = this
        if (type_bo == 'bo3') {
            return <span style={{color: "#2643e9", fontSize: 13, fontWeight: 600}}>Bo3</span>
        } else if (type_bo == 'bo5') {
            return <span style={{color: "#2643e9", fontSize: 13, fontWeight: 600}}>Bo5</span>
        } else {
            return <span style={{color: "#2643e9", fontSize: 13, fontWeight: 600}}>Bo1</span>
        }
    }
    renderBtnViewSubChallengeData(type_bo, challenge_id) {
        var _this = this
        if (type_bo == 'bo3' || type_bo == 'bo5') {
            return <a className="btn btn-ssm btn-primary" href={`/subchallenge?challenge_id=${challenge_id}`}>Detail</a>
        }
        return null
    }

}

export default View;
