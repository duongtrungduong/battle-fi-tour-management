import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Hidden} from 'library-form-hidden'
import FormConfigAction from "./form-setting-config/ActionHelper"
import UserService from 'app-services/userServices'
import {Select} from 'library-form-select'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'price_reward':       new Textbox.ActionHelper(this.dispatcher, this.formId, 'price_reward'),
            'stream_url':         new Textbox.ActionHelper(this.dispatcher, this.formId, 'stream_url'),
            'configs':            new FormConfigAction(this.dispatcher, this.formId, 'configs'),
            'config_id':          new Hidden.ActionHelper(this.dispatcher, this.formId, 'config_id'),
            'account_id':       new Select.ActionHelper(this.dispatcher, this.formId, 'account_id'),
            'type_bo':           new Select.ActionHelper(this.dispatcher, this.formId, 'type_bo'),
            'is_disabled_field':          new Hidden.ActionHelper(this.dispatcher, this.formId, 'is_disabled_field'),
        }
    }

    load() {
        var _this = this

        _this.helper("type_bo").dispatchSetChoices([
            {
                "value": "bo3",
                "label": "Bo3",
            },
            {
                "value": "bo5",
                "label": "Bo5",
            },
        ])
        
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/challenge/get?id=' + __params.query.id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.helper('name').dispatchChangeValue(result.data.name)
                    _this.helper('price_reward').dispatchChangeValue(result.data.price_reward)
                    _this.helper('stream_url').dispatchChangeValue(result.data.stream_url)
                    _this.helper('configs').dispatchChangeValue(result.data.game_config)
                    _this.helper('config_id').dispatchChangeValue(result.data.game_config_id)
                    _this.helper('account_id').dispatchChangeValue(result.data.account_id)
                    _this.helper('type_bo').dispatchChangeValue(result.data.type_bo)
                    if(result.data.status == 5){ //status started
                        _this.helper('is_disabled_field').dispatchChangeValue(true)
                    }

                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load the data")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/account_jury/list',
            data:      {
                '_sort_key': "account__name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var account_ids = []
                    result.data.records.map(function(val, key) {
                        account_ids.push({
                            "value": val.account_id,
                            "label": val.account_name == '' ? 'No name -'+ val.account_id : val.account_name
                        })
                    })
                    _this.helper("account_id").dispatchSetChoices(account_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (!values.name) {
            hasError = true
            this.helper('name').dispatchHasError('This field is mandatory')
        }
        if (!values.price_reward) {
            values.price_reward = 0
        }
        if (!hasError) {
            this.dispatchFormLoadStart()
            jQuery.ajax({
                headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/challenge/update?id=' + __params.query.id,
                dataType:     'json',
                data:         JSON.stringify(values),
                success:      function(result) {
                    if (result.status == 'ok') {
                        _this.dispatchFormAddMessage("info", "The record has been created successfully!")
                        _this.dispatchFormSubmitSuccess(values)
                        _this.resetForm()
                    }
                    else {
                        if (result.data.message != undefined) {
                            _this.dispatchFormAddMessage("error", result.data.message)
                        }
                        else {
                            _this.dispatchFormAddMessage("error", "Can't update the record")
                        }
                        _this.dispatchFormSubmitFail(values)
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage("error", "Can't communicate to API")
                    _this.dispatchFormSubmitFail(values)
                }
            })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
