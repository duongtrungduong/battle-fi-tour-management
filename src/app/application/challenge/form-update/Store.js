import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Hidden} from 'library-form-hidden'
import FormConfigStore from "./form-setting-config/StoreHelper"
import {Select} from 'library-form-select'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'name':               new Textbox.StoreHelper(this.formId, 'name'),
            'price_reward':       new Textbox.StoreHelper(this.formId, 'price_reward'),
            'stream_url':         new Textbox.StoreHelper(this.formId, 'stream_url'),
            'configs':            new FormConfigStore(this.formId, 'configs'),
            'config_id':          new Hidden.StoreHelper(this.formId, 'config_id'),
            'account_id':       new Select.StoreHelper(this.formId, 'account_id'),
            'type_bo':       new Select.StoreHelper(this.formId, 'type_bo'),
            'is_disabled_field':           new Hidden.StoreHelper(this.formId, 'is_disabled_field'),
        }
    }
}

export default Store;
