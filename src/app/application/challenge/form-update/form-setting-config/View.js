import React, { Component } from 'react';
import Select from 'react-select';
import './style.css'
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
class View extends Component {
    changeField(fieldName, index, data) {
        var _this = this
        var value = this.props.data.get('value').toJS();
        value[index][fieldName] = data;
        this.props.action.changeValue(this.props.data.get('id'), value);
    }

    changeFieldDefaultValue(fieldName, index, selectedValue, options) {
        var _this = this
        var value = this.props.data.get('value').toJS();
        const indexOption = options.findIndex(d => Number(d.value) === Number(selectedValue))
        const option = options[indexOption]
        value[index][fieldName] = option.label;
        this.props.action.changeValue(this.props.data.get('id'), value);
    }

    render() {
        var _this = this;
        const values = _this.props.data.get('value')
        const is_disabled = _this.props.disabled
        if (values.size <= 0) {
            return <div></div>
        }

        return (
            <div>
                {
                    values.map((d, index) => {
                        const options = _this.parseDefaulDataConfig(d.data)
                        return (
                            <div className="form-group">
                                <label className="control-label">{d.name}</label>
                                <select
                                    className={this.props.className ? this.props.className: "form-control"}
                                    id={`select-${d.fieldId}`}
                                    value={d.value}
                                    disabled={is_disabled}
                                    onChange={e => (
                                        this.changeField('value', index, e.target.value),
                                        this.changeFieldDefaultValue('defaultValue', index, e.target.value, options)
                                    )}
                                >
                                <option value=''></option>
                                    {
                                        options.map(function(choice, i) {
                                            return <option value={choice.value} key={d.fieldId + '-' + i} >{choice.label}</option>
                                        })
                                    }
                                </select>
                            </div>
                        )
                    })
                }
            </div>
        )
    }

    parseDefaulDataConfig(data) {
        const options = []
        const arr = data.split('\n')
        arr.forEach(ele => {
            const newObj = ele.split('|')
            options.push({
                'value': newObj[0],
                'label': newObj[1],
            })
        })
        return options
    }
}

export default View;
