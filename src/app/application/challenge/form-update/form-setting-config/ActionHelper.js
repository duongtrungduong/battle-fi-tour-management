import { Field } from 'library-field';

class ActionHelper extends Field.ActionHelper {
    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-value',
            value:                      value
        })
    }
}

export default ActionHelper;
