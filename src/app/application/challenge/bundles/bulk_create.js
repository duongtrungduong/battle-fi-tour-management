
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import BulkCreateContainer from '../BulkCreateContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <BulkCreateContainer />,
        document.getElementById('root')
    )
});
