
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import ListContainer from '../ListContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <ListContainer />,
        document.getElementById('root')
    )
});
