
import React from 'react';
import ReactDOM from 'react-dom';
import jQuery from 'jquery';
import CreateContainer from '../CreateContainer'

import 'app-container/css/default.css'

jQuery(document).ready(function() {
    ReactDOM.render(
        <CreateContainer />,
        document.getElementById('root')
    )
});
