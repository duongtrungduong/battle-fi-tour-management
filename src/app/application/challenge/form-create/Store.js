import Immutable from 'immutable';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'name':               new Textbox.StoreHelper(this.formId, 'name'),
            'game_kind_id':       new Select.StoreHelper(this.formId, 'game_kind_id'),
            'desc':               new Textarea.StoreHelper(this.formId, 'desc'),
            'start_date':         new DatetimePicker.StoreHelper(this.formId, 'start_date'),
            'price_reward':       new Textbox.StoreHelper(this.formId, 'price_reward'),
            'stream_url':         new Textbox.StoreHelper(this.formId, 'stream_url'),
            'account_id':       new Select.StoreHelper(this.formId, 'account_id'),
            'type':             new Select.StoreHelper(this.formId, 'type'),
            'type_bo':             new Select.StoreHelper(this.formId, 'type_bo'),
            'kind_reward':             new Select.StoreHelper(this.formId, 'kind_reward'),
        }
    }
}

export default Store;
