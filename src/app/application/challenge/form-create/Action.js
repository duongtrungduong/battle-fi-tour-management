import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import UserService from 'app-services/userServices'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
import moment from "moment"
import userServices from '../../../services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'name':               new Textbox.ActionHelper(this.dispatcher, this.formId, 'name'),
            'game_kind_id':       new Select.ActionHelper(this.dispatcher, this.formId, 'game_kind_id'),
            'desc':               new Textarea.ActionHelper(this.dispatcher, this.formId, 'desc'),
            'start_date':         new DatetimePicker.ActionHelper(this.dispatcher, this.formId, 'start_date'),
            'price_reward':       new Textbox.ActionHelper(this.dispatcher, this.formId, 'price_reward'),
            'stream_url':         new Textbox.ActionHelper(this.dispatcher, this.formId, 'stream_url'),
            'account_id':       new Select.ActionHelper(this.dispatcher, this.formId, 'account_id'),
            'type':             new Select.ActionHelper(this.dispatcher, this.formId, 'type'),
            'type_bo':             new Select.ActionHelper(this.dispatcher, this.formId, 'type_bo'),
            'kind_reward':             new Select.ActionHelper(this.dispatcher, this.formId, 'kind_reward'),
        }
    }

    load() {
        var _this = this
        _this.helper("type").dispatchSetChoices([
            {
                "value": "public",
                "label": "Public",
            },
            {
                "value": "private",
                "label": "Private",
            },
        ])
        _this.helper("type").dispatchChangeValue('public')
        _this.helper("price_reward").dispatchChangeValue(0)
        _this.helper("type_bo").dispatchSetChoices([
            {
                "value": "bo3",
                "label": "Bo3",
            },
            {
                "value": "bo5",
                "label": "Bo5",
            },
        ])
        
        _this.helper("type_bo").dispatchChangeValue('bo3')

        _this.helper("kind_reward").dispatchSetChoices([
            {
                "value": "direct",
                "label": "Direct (Token or BUSD)",
            },
            {
                "value": "battle_power",
                "label": "Battle Power",
            },
        ])

        _this.helper("kind_reward").dispatchChangeValue('battle_power')
        this.helper("start_date").dispatchChangeValue(new Date())
        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/game_kind/list',
            data:      {
                '_sort_key': "name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var game_kind_ids = []
                    result.data.records.map(function(val, key) {
                        game_kind_ids.push({
                            "value": val.id,
                            "label": val.name
                        })
                    })
                    _this.helper("game_kind_id").dispatchSetChoices(game_kind_ids)
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/account_jury/list',
            data:      {
                '_sort_key': "account__name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var account_ids = []
                    result.data.records.map(function(val, key) {
                        account_ids.push({
                            "value": val.account_id,
                            "label": val.account_email + ` (${val.account_name})`,})
                    })
                    _this.helper("account_id").dispatchSetChoices(account_ids)
                    _this.helper('account_id').dispatchChangeValue(userServices.getId())
                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })
    }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }

    submit(form, e, props) {
        var _this = this;
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (!values.name) {
            hasError = true
            this.helper('name').dispatchHasError('This field is mandatory')
        }
        if (!values.game_kind_id) {
            hasError = true
            this.helper('game_kind_id').dispatchHasError('This field is mandatory')
        }

        if (!values.account_id) {
            hasError = true
            this.helper('account_id').dispatchHasError('This field is mandatory')
        }
       
        if (!values.start_date) {
            hasError = true
            this.helper('start_date').dispatchHasError('This field is mandatory')
        }
        if (!values.type) {
            hasError = true
            this.helper('type').dispatchHasError('This field is mandatory')
        }
        if (!hasError) {
                var dataSubmit = {}
                dataSubmit['name'] = values.name
                dataSubmit['game_kind_id'] = values.game_kind_id
                dataSubmit['account_id'] = values.account_id
                dataSubmit['start_game_time'] = moment(values.start_date).format("X")
                dataSubmit['desc'] = values.desc
                dataSubmit['price_reward'] = values.price_reward
                dataSubmit['stream_url'] = values.stream_url
                dataSubmit['type'] = values.type
                dataSubmit['type_bo'] = values.type_bo
                this.dispatchFormLoadStart()
                jQuery.ajax({
                    headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                    type:         'POST',
                    url:          __params.config['api.endpoint'] + '/challenge/create',
                    dataType:     'json',
                    data:         dataSubmit,
                    success:      function(result) {
                        if (result.status == 'ok') {
                            _this.dispatchFormAddMessage("info", "The record has been created successfully!")
                            _this.dispatchFormSubmitSuccess(values)
                            _this.resetForm()
                        }
                        else {
                            if (result.data.message != undefined) {
                                _this.dispatchFormAddMessage("error", result.data.message)
                            }
                            else {
                                _this.dispatchFormAddMessage("error", "Can't update the record")
                            }
                            _this.dispatchFormSubmitFail(values)
                        }
                    },
                    error:        function(data) {
                        _this.dispatchFormAddMessage("error", "Can't communicate to API")
                        _this.dispatchFormSubmitFail(values)
                    }
                })
        }
        else {
            _this.dispatchFormAddMessage("error", "Validation failed")
        }
    }
}

export default Action;
