import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
import UserService from 'app-services/userServices'

class View extends Form.View {
    constructor(props) {
        super(props)
    }
    render() {

        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form className="form" onSubmit={e => e.preventDefault()}>
                    <div>
                        <div className="form-group">
                            <label className="control-label">Name<span className="required" aria-required="true">*</span></label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('name')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Game<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('game_kind_id')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Type<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('type')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Competition format</label>
                            <Select.View
                                data={this.props.form.get('fields').get('type_bo')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Start Date<span className="required" aria-required="true">*</span></label>
                            <DatetimePicker.View
                                data={this.props.form.get('fields').get('start_date')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Validator<span className="required" aria-required="true">*</span></label>
                            <Select.View
                                data={this.props.form.get('fields').get('account_id')}
                                action={this.props.action}
                                disabled={  UserService.getPermission() == 'jury'}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Reward</label>
                            <Select.View
                                data={this.props.form.get('fields').get('kind_reward')}
                                action={this.props.action}
                            />
                        </div>
                        {
                            this.props.form.get('fields').get('kind_reward').get("value") == "direct" ?
                            <div className="form-group">
                                <label className="control-label">Price Reward ($)</label>
                                <Textbox.View
                                    data={this.props.form.get('fields').get('price_reward')}
                                    action={this.props.action}
                                />
                            </div>: false
                        }
                        
                        <div className="form-group">
                            <label className="control-label">Description</label>
                            <Textarea.View
                                data={this.props.form.get('fields').get('desc')}
                                action={this.props.action}
                            />
                        </div>
                        <div className="form-group">
                            <label className="control-label">Stream</label>
                            <Textbox.View
                                data={this.props.form.get('fields').get('stream_url')}
                                action={this.props.action}
                            />
                        </div>
                        
                    </div>
                    <div>
                        {
                            this.props.form.get('messages').map(function(message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                                    }
                                    return (<div className="alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>

                    <div className="form-actions">
                        <div className="row">
                            <div className="col-md-9">
                                <button className="btn btn-sm btn-primary" onClick={ e => this.props.action.submit(this.props.form, e)}>Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </BlockUi>
        )
    }
}

export default View;
