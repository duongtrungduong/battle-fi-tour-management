import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {Checkbox} from 'library-form-checkbox'
import {Hidden} from 'library-form-hidden'


class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    initStoreHelpers() {
        this._storeHelpers = {
            'team_win_id'              : new Select.StoreHelper(this.formId, 'team_win_id'),
            'challenge_id':        new Hidden.StoreHelper(this.formId, 'challenge_id'),
            'team_1_score'          : new Textbox.StoreHelper(this.formId, 'team_1_score'),            
            'team_2_score'          : new Textbox.StoreHelper(this.formId, 'team_2_score'),   
            'team_1_label'          : new Textbox.StoreHelper(this.formId, 'team_1_label'),
            'team_2_label'          : new Textbox.StoreHelper(this.formId, 'team_2_label'),   

        }
    }
    getInitialState() {
        var state = super.getInitialState()
        return state
    }
    reduce(state, action) {
        var id = this.id
        state = super.reduce(state, action)
        switch (action.type) {
            case this.id + '/open-form':
                state = state.set('isOpenForm', true)
                return state
            case this.id + '/close-form':
                state = state.set('isOpenForm', false)
                return state
                
        }
        return state
    }
}

export default Store;
