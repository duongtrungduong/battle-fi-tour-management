import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Textarea } from 'library-form-textarea'
import {UploadFile} from 'library-form-upload-file'
import { ProgressBar } from 'react-bootstrap';
import {Modal} from 'react-bootstrap';
import "./style.css"
import {Select} from 'library-form-select'
import {InputHidden} from  'library-form-hidden'

class View extends Form.View {
    constructor(props) {
        super(props)
    }

    changeValue(id, e) {
        var files = e.target.files;
        var value = [];
        for (var i = 0; i < files.length; i++) {
            value.push(files[i])
        }

        this.props.action.changeValue(this.props, id, value );
    }

    render() {
        var _this = this;
        var isOpenForm = _this.props.form.get('isOpenForm')
        return (
                <Modal
                    id="modal-add-content"
                    style={{opacity:1 }}
                    fade={false}
                    show={isOpenForm}
                    onHide={() => _this.props.action.closeForm(this.props)}
                    dialogClassName="modal-dialog-form"
                >
                    <Modal.Header closeButton style={{display: 'block'}}>
                        <Modal.Title>
                            <h2>Challenge result</h2>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body >
                        { _this.renderForm() }
                        <div className="d-flex justify-content-start" style={{}}>
                        <button type="submit" onClick={e => this.props.action.submit(this.props.form, this.props, e)} className="btn btn-sm btn-primary">Save</button>
                            </div>
                            <br/>
                            <div style={{paddingLeft:15, paddingRight: 15}}>
                                {
                                    this.props.form.get('messages').map(function (message, i) {
                                        switch (message.type) {
                                            case 'error':
                                                return (<div key={`message-error-${i}`} className="alert alert-danger" style={{margin: 0, padding: "7px 15px"}} role="alert">{message.message}</div>)
                                        }
                                        return (<div key={`message-success-${i}`} className="alert alert-info" style={{margin: 0, padding: "7px 15px"}} role="alert">{message.message}</div>)
                                    })
                                }
                            </div>
                    </Modal.Body>
                </Modal>
        )
    }
    renderForm() {
        var _this = this;
        return (
        <form className="form" onSubmit={e => e.preventDefault()}>
            <div className="form-group">
                <label className="control-label">Team win<span className="required" aria-required="true">*</span></label>
                <div className="row" style={{marginTop: 5, marginLeft: 0, marginRight: 0}}>
                    {
                        this.props.form.get('fields').get('team_win_id').get("choices").map(function(val, key) {
                            var border = "1px solid #efefef"
                            if (val.value == _this.props.form.get('fields').get('team_win_id').get("value")) {
                                border = "2px solid rgb(57, 139, 57)"
                            }
                            return (
                                <div className="col-md-4 team-hover" 
                                    onClick={e => _this.props.action.changeValue("team_win_id", val.value)}
                                    style={{cursor: "pointer", marginRight: 5, padding: 10, border: border}}>
                                    {val.label}
                                </div>
                            )
                        })
                    }
                </div>
            </div>
            <div className="form-group">
                <label className="control-label">{this.props.form.get('fields').get('team_1_label').get('value')} score</label>
                <Textbox.View
                    data={this.props.form.get('fields').get('team_1_score')}
                    action={this.props.action}
                />
                <label className="control-label">{this.props.form.get('fields').get('team_2_label').get('value')} score</label>
                <Textbox.View
                    data={this.props.form.get('fields').get('team_2_score')}
                    action={this.props.action}
                />
            </div>
        </form>
           
        )
    }

}

export default View;
