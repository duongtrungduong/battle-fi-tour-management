import jQuery from 'jquery'
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {MultiSelect} from 'library-form-multiselect'
import {Checkbox} from 'library-form-checkbox'
import {UploadFile} from 'library-form-upload-file'
import UserService from 'app-services/userServices'
import {Hidden} from 'library-form-hidden'
class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'team_win_id'          : new Select.ActionHelper(this.dispatcher, this.formId, 'team_win_id'),
            'challenge_id':            new Hidden.ActionHelper(this.dispatcher, this.formId, 'challenge_id'),
            'team_1_label'          : new Textbox.ActionHelper(this.dispatcher, this.formId, 'team_1_label'),
            'team_2_label'          : new Textbox.ActionHelper(this.dispatcher, this.formId, 'team_2_label'),   
            'team_1_score'          : new Textbox.ActionHelper(this.dispatcher, this.formId, 'team_1_score'),            
            'team_2_score'          : new Textbox.ActionHelper(this.dispatcher, this.formId, 'team_2_score'),            

        }
    }

    load() {
        var _this = this
        _this.helper("team_1_score").dispatchChangeValue(0)
        _this.helper("team_2_score").dispatchChangeValue(0)
    }

    clear() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearError()
        }
        this.dispatchFormClearMessages()
    }

    changeValue(fieldId, newValue) {
        console.log(fieldId, newValue)
        var _this = this;
        super.changeValue(fieldId, newValue)

    }

    resetForm() {       
        var _this = this
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }
        this.load()
        this.clear()
    }
    
    openForm(e, challenge_id) {
        var _this = this
        _this.resetForm()

        jQuery.ajax({
            type:      'GET',
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            url:       __params.config['api.endpoint'] + '/challenge/get?id=' + challenge_id,
            data:      {
                '_sort_key': "account__name",
                "_sort_dir": "ASC",
                "page_size": 999999
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var team_datas = [
                        {
                            "value": result.data.team_1_id,
                            "label": result.data.team_1_name
                        },
                        {
                            "value": result.data.team_2_id,
                            "label": result.data.team_2_name
                        }
                    ]
                    _this.helper("team_1_label").dispatchChangeValue(result.data.team_1_name)
                    _this.helper("team_2_label").dispatchChangeValue(result.data.team_2_name)
                    _this.helper("team_win_id").dispatchSetChoices(team_datas)
                    _this.helper("challenge_id").dispatchChangeValue(challenge_id)

                }
                else {
                    _this.dispatchFormAddMessage('error', "Can't load game kind list")
                    _this.dispatchFormLoadFail()
                }
            },
            error: function(data) {
                _this.dispatchFormAddMessage('error', "Can't communicate to API")
                _this.dispatchFormLoadFail()
            }
        })




        this.dispatcher.dispatch({
            type:          _this.formId + "/open-form",
        })
    }

    closeForm() {
        var _this = this
        this.dispatcher.dispatch({
            type:          _this.formId + "/close-form",
        })
    }

    submit(form, props, value) {
        var _this = this;
        var values = this.getValues(form)
        var hasError = false
        _this.clear()
        _this.dispatchFormLoadStart()

        if (!values.team_win_id) {
            hasError = true
            this.helper('team_win_id').dispatchHasError('This field is mandatory')
        }
        if (!hasError) {
            var dataSubmit = {}
            dataSubmit['team_win_id'] = values.team_win_id
            dataSubmit['team_1_score'] = values.team_1_score
            dataSubmit['team_2_score'] = values.team_2_score
            jQuery.ajax({
                headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/challenge/end?id='+values.challenge_id,
                dataType:     'json',
                data:         dataSubmit,
                success:      function(result) {
                    if (result.status == 'ok') {
                        // _this.closeForm()
                        window.location.reload()
                    }
                    else {
                        if (result.data.message != undefined) {
                            _this.dispatchFormAddMessage("error", result.data.message)
                        }
                        else {
                            _this.dispatchFormAddMessage("error", "Submit failed")
                        }
                        _this.dispatchFormLoadFail({})
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage("error", "Can connect to API")
                    _this.dispatchFormLoadFail({})
                }
            })
        }
        else {
            // _this.clear()
            _this.dispatchFormAddMessage("error", "Please choose team win")
        }
    }

}

export default Action;
