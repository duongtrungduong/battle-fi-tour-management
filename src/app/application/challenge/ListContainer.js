import React, {Component} from 'react';
import {Container} from 'flux/utils';
import {Dispatcher} from 'flux';


import PageContainerView from 'app-container/View';
import PageContainerStore from 'app-container/Store';
import PageContainerAction from 'app-container/Action';
import DataTableView from './data-table/View';
import DataTableStore from './data-table/Store';
import DataTableAction from './data-table/Action';

import FormSearchView from './form-search/View';
import FormSearchStore from './form-search/Store';
import FormSearchAction from './form-search/Action';

import FormEndChallengeView from './form-modal-end/View';
import FormEndChallengeStore from './form-modal-end/Store';
import FormEndChallengeAction from './form-modal-end/Action';

const _dispatcher = new Dispatcher();
const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')
const _dataTableStore = new DataTableStore(_dispatcher, 'data-table')
const _dataTableAction = new DataTableAction(_dispatcher, 'data-table')
const _formSearchStore = new FormSearchStore(_dispatcher, 'form-search')
const _formSearchAction = new FormSearchAction(_dispatcher, 'form-search')
const _formModalEndChallengeStore = new FormEndChallengeStore(_dispatcher, 'form-modal-end')
const _formModalEndChallengeAction = new FormEndChallengeAction(_dispatcher, 'form-modal-end')

_pageContainerAction.setCategory("general")
_pageContainerAction.setMenu("challenge")

class ListContainer extends Component {
    static getStores() {
        return [
            _pageContainerStore,
            _dataTableStore,
            _formSearchStore,
            _formModalEndChallengeStore
        ];
    }

    static calculateState(prevState) {
        return {
            pageContainer:    _pageContainerStore.getState(),
            dataTable:        _dataTableStore.getState(),
            formSearch:       _formSearchStore.getState(),
            formModalEnd:       _formModalEndChallengeStore.getState()
        }
    }

    render() {
        return (
            <PageContainerView pageContainer={this.state.pageContainer} action={_pageContainerAction}>
                <div className="row mt-5">
                    <div className="col-xl-12 mb-5 mb-xl-0">
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <div className="row align-items-center">
                                    <div className="col-8">
                                        <FormSearchView
                                            form={this.state.formSearch}
                                            action={_formSearchAction}
                                            dataTableAction={_dataTableAction}
                                        />
                                    </div>
                                    <div className="col-4 text-right">
                                        <a href="/challenge/bulk_create" className="btn btn-sm btn-info">BULK CREATE</a>
                                        <a href="/challenge/create" className="btn btn-sm btn-primary">CREATE</a>
                                    </div>
                                </div>
                            </div>
                            <DataTableView
                                dataTable={this.state.dataTable}
                                action={_dataTableAction}
                                formModalEndChallenge = {_formModalEndChallengeAction}
                            />
                            <FormEndChallengeView
                                form={this.state.formModalEnd}
                                action={_formModalEndChallengeAction}
                            />
                        </div>
                    </div>
                </div>  
			</PageContainerView>
        )
    }
}

export default Container.create(ListContainer);
