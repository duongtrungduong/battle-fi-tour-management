import React, {Component} from 'react';
import {Container} from 'flux/utils';
import {Dispatcher} from 'flux';


import PageContainerView from 'app-container/View';
import PageContainerAction from 'app-container/Action';
import PageContainerStore from 'app-container/Store';
import FormView from './form-create/View';
import FormStore from './form-create/Store';
import FormAction from './form-create/Action';

import FormResultSpinView from './form-result-spin/View';
import FormResultSpinStore from './form-result-spin/Store';
import FormResultSpinAction from './form-result-spin/Action';

const _dispatcher = new Dispatcher();
const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')
const _formStore = new FormStore(_dispatcher, 'form')
const _formAction = new FormAction(_dispatcher, 'form')
const _formResultSpinStore = new FormResultSpinStore(_dispatcher, 'form-result-spin')
const _formResultSpinAction = new FormResultSpinAction(_dispatcher, 'form-result-spin')

_pageContainerAction.setCategory("general")
_pageContainerAction.setMenu("challenge")
class CreateContainer extends Component {
    static getStores() {
        return [
            _pageContainerStore,
            _formStore,
            _formResultSpinStore

        ];
    }

    static calculateState(prevState) {
        return {
            pageContainer:    _pageContainerStore.getState(),
            form:             _formStore.getState(),
            formResultSpin:       _formResultSpinStore.getState()
        }
    }

    render() {
        return (
            <PageContainerView pageContainer={this.state.pageContainer} action={_pageContainerAction}>
                <div className="row mt-5">
                    <div className="col-xl-12 mb-5 mb-xl-0">
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <center>
                                    <h1 style={{ fontWeight: "600" }}>LUCKY WHEEL</h1>
                                </center>
                            </div>
                            <div className="card-body">
                                <FormView
                                    form={this.state.form}
                                    action={_formAction}
                                    FormResultSpin = {_formResultSpinAction}
                                />
                                <FormResultSpinView
                                    form={this.state.formResultSpin}
                                    action={_formResultSpinAction}
                                    FormView={_formAction}
                                />
                            </div>
                        </div>
                    </div>
                </div>  
            </PageContainerView>
        )
    }
}

export default Container.create(CreateContainer);
