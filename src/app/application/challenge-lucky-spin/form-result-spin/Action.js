import jQuery from 'jquery'
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {MultiSelect} from 'library-form-multiselect'
import {Checkbox} from 'library-form-checkbox'
import {UploadFile} from 'library-form-upload-file'
import UserService from 'app-services/userServices'
import {Hidden} from 'library-form-hidden'
class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'lucky_email':            new Hidden.ActionHelper(this.dispatcher, this.formId, 'lucky_email'),
        }
    }

    load() {

    }

    clear() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearError()
        }
        this.dispatchFormClearMessages()
    }

    changeValue(fieldId, newValue) {
        var _this = this;
        super.changeValue(fieldId, newValue)

    }

    resetForm() {       
        var _this = this
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }
        this.load()
        this.clear()
    }
    
    openForm(email) {
        var _this = this
        _this.resetForm()
        _this.helper("lucky_email").dispatchChangeValue(email)
        this.dispatcher.dispatch({
            type:          _this.formId + "/open-form",
        })
    }

    closeForm(propFormView) {
        var _this = this
        propFormView.resetForm()
        this.dispatcher.dispatch({
            type:          _this.formId + "/close-form",
        })
    }

  
}

export default Action;
