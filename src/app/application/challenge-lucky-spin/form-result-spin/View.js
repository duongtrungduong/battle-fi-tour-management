import React, { Component } from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Textarea } from 'library-form-textarea'
import {UploadFile} from 'library-form-upload-file'
import { ProgressBar } from 'react-bootstrap';
import {Modal} from 'react-bootstrap';
import "./style.css"
import {Select} from 'library-form-select'
import {InputHidden} from  'library-form-hidden'

class View extends Form.View {
    constructor(props) {
        super(props)
    }

    changeValue(id, e) {
        var files = e.target.files;
        var value = [];
        for (var i = 0; i < files.length; i++) {
            value.push(files[i])
        }

        this.props.action.changeValue(this.props, id, value );
    }

    render() {
        var _this = this;
        var isOpenForm = _this.props.form.get('isOpenForm')
        return (
                <Modal
                    id="modal-add-content"
                    style={{opacity:1 }}
                    fade={false}
                    show={isOpenForm}
                    onHide={() => _this.props.action.closeForm(_this.props.FormView)}
                    dialogClassName="modal-dialog-form"
                >
                    <Modal.Header closeButton style={{display: 'block'}}>
                        <Modal.Title>
                            <h1 style={{ textAlign: 'center', paddingLeft: 45}}> Congratulations</h1>
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body >
                        <center>
                            <img style={{ width: 100, paddingBottom: 15}} alt="Image placeholder" src="/public/static/agon/img/gift.png" />
                            <h2>Account: <span style={{color: '#5e72e4'}}>{ this.props.form.get('fields').get('lucky_email').get('value') }</span> is winner</h2>
                        </center>
                    </Modal.Body>
                    <Modal.Footer style={{justifyContent: 'center'}}>
                    <div className="d-flex justify-content-start" style={{paddingLeft:15, paddingRight: 15}}>
                        <button type="submit" onClick={e => this.props.action.closeForm(_this.props.FormView)} className="btn btn-primary">Close</button>
                    </div>
                </Modal.Footer>
                </Modal>
        )
    }

}

export default View;
