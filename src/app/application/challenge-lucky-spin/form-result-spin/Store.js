import Immutable from 'immutable';
import {Form} from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {Checkbox} from 'library-form-checkbox'
import {Hidden} from 'library-form-hidden'


class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    initStoreHelpers() {
        this._storeHelpers = {
            'lucky_email':        new Hidden.StoreHelper(this.formId, 'lucky_email'),

        }
    }
    getInitialState() {
        var state = super.getInitialState()
        return state
    }
    reduce(state, action) {
        var id = this.id
        state = super.reduce(state, action)
        switch (action.type) {
            case this.id + '/open-form':
                state = state.set('isOpenForm', true)
                return state
            case this.id + '/close-form':
                state = state.set('isOpenForm', false)
                return state
                
        }
        return state
    }
}

export default Store;
