import React, {Component} from 'react';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
// import WheelComponent from "react-wheel-of-prizes"; 
import WheelComponent from "./WheelComponent";
import './style.css'
class View extends Form.View {
    constructor(props) {
        super(props)
    }
    
    onFinished = (lucky_email) => {
        var _this = this
        _this.props.action.submitResultLuckyAccount(lucky_email)
        // _this.props.action.resetTable(lucky_email)
        if (lucky_email != undefined){
            setTimeout(() => {
                _this.props.FormResultSpin.openForm(lucky_email)        
            }, 200);
        }
    };
    
    
    render() {
        let segments = this.props.form.get('fields').get('email_wheels').get('value')
        let listAccountWinning = this.props.form.get('fields').get('winning_accounts').get('value')
        const segColors = [
            "#EE4040",
            "#F0CF50",
            "#815CD1",
            "#3DA5E0",
            "#34A24F",
            "#F9AA1F",
            "#EC3F3F",
            "#FF9000"
        ];
        if (segments.length <= 0 && listAccountWinning.length <= 0){
            return (<div>
                <center>The challenge is not finished or there is no user prediction</center>
            </div>)
        }
        return (
            <div>
                <div className="row">
                <div className='col-md-1'></div>
                    <div className='col-md-6'>
                        <div className="round" style={{position: 'relative'}}>
                        <div id="wheelCircle">
                            <div id='button-spin'></div>
                            <WheelComponent
                                segments={segments}
                                segColors={segColors}
                                winningSegment="s"
                                onFinished={(winner) => this.onFinished(winner)}
                                primaryColor="#f7f70e"
                                contrastColor="white"
                                buttonText="SPIN"
                                isOnlyOnce={false}
                                size={290}
                                upDuration={500}
                                downDuration={1000}
                            />
                        </div>
                        </div>
                    </div>
                    <div className='col-md-4' style={{ paddingLeft: 20}}>
                        <div className="card shadow">
                            <div className="card-header border-0">
                                <h3 className="mb-0">List of winning accounts</h3>
                            </div>
                                <div className="table-responsive">
                                    <table className="table align-items-center table-flush">
                                        <thead className="thead-light">
                                            <tr>
                                                <th scope="col">Account</th>
                                                <th scope="col">Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {
                                                listAccountWinning.length > 0 ?  (listAccountWinning.map(function(record, i) {
                                                    return(
                                                        <tr>
                                                            <th scope="row">
                                                                <div className="media align-items-center">
                                                                    <a href="#" className="avatar rounded-circle mr-3">
                                                                        <img style={{height:'100%'}}alt="Image placeholder" src={ record.avatar_url != '' ? record.avatar_url : '/public/static/agon/img/logo-icon.png' }/>                                                                
                                                                    </a>
                                                                    <div className="media-body">
                                                                    <span className="mb-0 text-sm">{ record.name }</span>
                                                                    <p className="mb-0 text-sm">{ record.email }</p>

                                                                    </div>
                                                                </div>
                                                            </th>
                                                            <td>{ (new Date(parseInt(record.timestamp*1000))).toLocaleString()}</td>
                                                        </tr> 
                                                    ) 
                                                    
                                                })) 
                                                : 
                                                <tr>
                                                    <td colSpan={2} style={{textAlign: 'center'}}></td>
                                                </tr>
                                            }

                                        </tbody>
                                    </table>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default View;
