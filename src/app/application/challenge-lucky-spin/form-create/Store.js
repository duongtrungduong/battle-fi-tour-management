import Immutable from 'immutable';
import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
import {Hidden} from 'library-form-hidden'

class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }
    
    initStoreHelpers() {
        this._storeHelpers = {
            'email_wheels':        new Hidden.StoreHelper(this.formId, 'email_wheels'),
            'winning_accounts':        new Hidden.StoreHelper(this.formId, 'winning_accounts'),
        }
    }
    
}

export default Store;
