import {Form}  from 'library-form'
import {Textbox} from 'library-form-textbox'
import {Textarea} from 'library-form-textarea'
import UserService from 'app-services/userServices'
import {Select} from 'library-form-select'
import {DatetimePicker} from 'library-form-datetimepicker'
import moment from "moment"
import {Hidden} from 'library-form-hidden'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
    }
    initActionHelpers() {
        this._actionHelpers = {
            'email_wheels':            new Hidden.ActionHelper(this.dispatcher, this.formId, 'email_wheels'),
            'winning_accounts':          new Hidden.ActionHelper(this.dispatcher, this.formId, 'winning_accounts'),
        }
    }

    load() {
        var _this = this;
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/account_predict/list?challenge_id='+__params.query.id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    var segments = []
                    result.data.records.map(function(record, i) {
                        segments.push(record.account_email)
                        
                    })
                    _this.helper("email_wheels").dispatchChangeValue(segments)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })

        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/account_predict/lucky_wheel_winning_list?challenge_id='+__params.query.id,
            data:      {},
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.helper("winning_accounts").dispatchChangeValue(result.data.records)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
     
    }

    // resetTable(){
    //     var _this = this;
    //     this.helper('winning_accounts').dispatchClearValue()
    //     jQuery.ajax({
    //         headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
    //         type:      'GET',
    //         url:       __params.config['api.endpoint'] + '/account_predict/lucky_wheel_winning_list?challenge_id='+__params.query.id,
    //         data:      {},
    //         dataType:  'json',
    //         success:   function(result) {
    //             if (result.status == 'ok') {
    //                 _this.helper("winning_accounts").dispatchChangeValue(result.data.records)
    //             }
    //             else {
    //                 _this.dispatchLoadRecordsFail("Fail to load records")
    //             }
    //         },
    //         error: function(data) {
    //             _this.dispatchLoadRecordsFail("Fail to connect to API")
    //         }
    //     })

    // }

    resetForm() {
        for (var fieldId in this.getActionHelpers()) {
            this.helper(fieldId).dispatchClearValue()
        }

        this.load()

    }
    submitResultLuckyAccount(lucky_email) {
        var _this = this
        var dataSubmit = {}
        dataSubmit['lucky_email'] = lucky_email
        if(lucky_email == undefined){
            return false
        }
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'POST',
            url:       __params.config['api.endpoint'] + '/challenge/create_lucky_wheel_log?challenge_id=' + __params.query.id,
            data:     dataSubmit,
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    // _this.resetForm()
                }
                else {
                    alert('Error. Please try again')
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
}

export default Action;
