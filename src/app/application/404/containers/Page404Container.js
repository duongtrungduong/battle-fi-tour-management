import React, { Component } from 'react'
import { Container } from 'flux/utils'
import { Dispatcher } from 'flux'

const _dispatcher = new Dispatcher()
import PageContainerView from 'app-container/ViewBlank';
import PageContainerAction from 'app-container/Action';
import PageContainerStore from 'app-container/Store';

const _pageContainerStore = new PageContainerStore(_dispatcher, 'page-container')
const _pageContainerAction = new PageContainerAction(_dispatcher, 'page-container')

class Page404Container extends Component {
    static getStores() {
        return [
            _pageContainerStore
        ]
    }

    static calculateState(prevState) {
        return {
            pageContainer:         _pageContainerStore.getState(),
        };
    }

    render() {
        return (
            <PageContainerView action={_pageContainerAction} pageContainer={this.state.pageContainer}>
                <div style={{ marginTop: 50, marginLeft: 140, marginRight: 80, backgroundColor: '#FFFFFF', borderRadius: 0 }}>
                    <div className="container-fluid pd-x-0">
                        <div className="row" style={{  }}>
                            <div className='well' style={{fontSize:28}}>
                                404 Not Found
                            </div>
                            <a href={__params.config['ui.endpoint']}>Back</a>
                        </div>
                    </div>
                </div>
            </PageContainerView>
        )
    }
}

export default Container.create(Page404Container);
