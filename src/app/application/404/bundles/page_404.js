import React from 'react'
import { render } from 'react-dom'

import Page404Container from '../containers/Page404Container'
import 'app-container/css/default.css'
jQuery(document).ready(function() {
    render(
        <Page404Container />,
        document.getElementById('root')
    )
});
