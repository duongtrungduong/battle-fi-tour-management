import React from 'react'
import { render } from 'react-dom'

import OpenContainer from '../containers/OpenContainer'
import './dashforge_1_0_0.css'

jQuery(document).ready(function() {
    render(
        <OpenContainer />,
        document.getElementById('root')
    )
});
