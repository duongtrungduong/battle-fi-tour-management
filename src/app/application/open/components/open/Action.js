import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Password } from 'library-form-password'
import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
        this.id = formId
    }

    initActionHelpers() {
        this._actionHelpers = {
        }
    }

    load() {
        var _this = this
        var callBack=__params.query.call_back
        var token = __params.query.token
        jQuery.ajax({
            type:         'POST',
            url:          __params.config['api.endpoint'] + '/user/validate',
            dataType:     'json',
            headers: { 'TOKEN': __params.query.token},
            data:         {},
            success:      function(result) {
                if (result.status == 'ok') {
                    UserService.logIn(result.data.token, JSON.stringify(result.data.info))
                    location.href=callBack
                }
                else {
                    location.href='/404'
                }
            },
            error:        function(data) {
                location.href='/404'
            }
        })  

    }

  

    
    submit(form, e) {
        
    }
}

export default Action;
