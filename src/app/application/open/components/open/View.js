import React from 'react'
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Password } from 'library-form-password'
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import "./style.css"

class View extends Form.View {
    render () {
        
        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
              
            </BlockUi>
        )
    }
}

export default View;
