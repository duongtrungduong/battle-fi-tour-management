import Immutable from 'immutable';
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Password } from 'library-form-password'


class Store extends Form.Store {
    constructor(dispatcher, id='form') {
        super(dispatcher, id)
    }

    initStoreHelpers() {
        this._storeHelpers = {
        }
    }
}

export default Store;