import React, { Component } from 'react'
import { Container } from 'flux/utils'
import { Dispatcher } from 'flux'

import FormView from '../components/open/View'
import FormAction from '../components/open/Action'
import FormStore from '../components/open/Store'

const _dispatcher = new Dispatcher()
const _formStore = new FormStore(_dispatcher, 'form')
const _formAction = new FormAction(_dispatcher, 'form')


class OpenContainer extends Component {
    static getStores() {
        return [
            _formStore,
        ]
    }

    static calculateState(prevState) {
        return {
            form: _formStore.getState(),
        };
    }

    render() {
        return (
            <FormView form={ this.state.form } action={ _formAction }/>
        )
    }
}

export default Container.create(OpenContainer);
