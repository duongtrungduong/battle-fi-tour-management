import React from 'react'
import { render } from 'react-dom'

import HomeContainer from '../containers/HomeContainer'
import 'app-container/css/default.css'
jQuery(document).ready(function() {
    render(
        <HomeContainer />,
        document.getElementById('root')
    )
});
