import React, {Component} from 'react';
import LibraryDataTable from 'library-data-table';
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import moment from "moment"
import UserService from 'app-services/userServices'
import userServices from '../../../services/userServices';
import './style.css';
class View extends LibraryDataTable.View {
    //jury
    checkRoleDoAction(record){
        var permission = UserService.getPermission()
        var id = UserService.getId()
        if ( permission == 'admin' || permission == 'mod')
            return true
        if ( permission == 'jury' && record.account_id == id)
            return true
        return false
    }
    renderTableHeader() {
        return <thead className='thead-light'>
            <tr>
                <th scope="col" width="20%">Name</th>
                <th scope="col" width="20%">Team 1</th>
                <th scope="col" width="20%">Team 2</th>
                <th scope="col" width="10%">Status</th>
                {/* <th scope="col" width="10%"></th> */}
            </tr>
        </thead>
    }
    renderLableWinLose(team_id, team_win){
        if ( team_win == '')
            return ''
        if (team_id == team_win){
            return <strong style={{fontSize: '8px',  color:'green'}}>VICTORY</strong>
        }else{
            return <strong style={{fontSize: '8px', color: 'red'}}>DEFEAT</strong>
        }
    }
    renderTableBody() {
        var _this = this;
        var permission = UserService.getPermission()
        var profile = UserService.getProfile()
        return (
            <tbody>
                {
                    this.props.dataTable.get('records').map(function(record, i) {
                        var tmp = new Date(record.start_game_time*1000)
                        var start_time = moment(tmp).format("HH:mm DD/MM/Y")
                        return(
                      
                            <tr key={"record-" + i}>
                                <td>
                                {'Game ' + (i+1)}                                
                                </td>
                                <td>
                                    <img className="team-icon" src={record.team_1_icon} width="30px" height="30px"/>
                                    &nbsp;
                                    &nbsp;
                                    { record.team_1_name } &nbsp;
                                    { _this.renderLableWinLose(record.team_1_id, record.winner) }
                                    <br/>
                                    {
                                        _this.renderPredictCount(record.team_1_predict)
                                    }
                                </td>
                                <td>
                                    <img className="team-icon" src={record.team_2_icon} width="30px" height="30px"/>
                                    &nbsp;
                                    &nbsp;
                                    { record.team_2_name} &nbsp;
                                    { _this.renderLableWinLose(record.team_2_id, record.winner) }
                                    <br/>
                                    {
                                        _this.renderPredictCount(record.team_2_predict)
                                    }
                                </td>
                                <td>{ _this.renderStatusData(record.status) }</td>
                                {/* <td style={{textAlign: "right"}}>
                                    {
                                        _this.renderButtonStatus(record)
                                    }
                                </td> */}
                            </tr>
                 
                    )

                    })
                }
            </tbody>
        )
    }
    renderButtonStatus(record) {
        var _this = this
        if (record.status == 'open' &&  _this.checkRoleDoAction(record)) {
            // start challenge
            return (
                <a className={`btn btn-sm btn-info`}
                    style={{cursor: ` ${record.is_disabled ? "not-allowed" : "pointer"} `}}
                    onClick={ e => _this.props.action.start(e, record.id, record.is_disabled)} href="javascript:;">Start
                </a>
            )
        }
        if (record.status == 'processing' &&  _this.checkRoleDoAction(record)) {
            // start challenge
            return (
                <a className={`btn btn-sm btn-info`}
                    style={{cursor: ` ${record.is_disabled ? "not-allowed" : "pointer"} `}}
                    onClick={ e => _this.props.action.start(e, record.id, record.is_disabled)} href="javascript:;">Completed
                </a>
            )
        }

    }

    renderStatusData(status) {
        var _this = this
        if (status == 'open') {
            return <span className='badge badge-info' style={{fontSize: 13, fontWeight: 600}}>Open</span>        
        } else if (status == 'processing') {
            return <span className='badge badge-primary' style={{fontSize: 13, fontWeight: 600}}>Processing</span>
        } 
        else if (status == 'closed') {
            return <span className='badge badge-secondary' style={{fontSize: 13, fontWeight: 600}}>Closed</span>
        }else {
            return <span className='badge badge-success' style={{fontSize: 13, fontWeight: 600}}>Completed</span>
        }
    }
    
    renderPredictCount(predict) {
        return <span style={{paddingTop: 10}}><strong>Total predict: </strong> {predict}</span>
    }


}

export default View;
