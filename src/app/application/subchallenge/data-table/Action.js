
import LibraryDataTable from 'library-data-table';
import jQuery from 'jquery';
import UserService from 'app-services/userServices'

class Action extends LibraryDataTable.Action {
    extractTableParams(dataTable) {
        var params = {}
        params['page_number'] = (dataTable.get('currentPage') - 1) *  dataTable.get('recordPerPage');
        params['page_size'] = dataTable.get('recordPerPage');

        var filters = dataTable.get('filters');
        for (var k in filters) {
            params[k] = filters.get(k);
        }

        if (__params.query.name != undefined) {
            params['name'] = __params.query.name
        }
        if (__params.query.game_kind_id != undefined) {
            params['game_kind_id'] = __params.query.game_kind_id
        }
        if (__params.query.tour_id != undefined) {
            params['tour_id'] = __params.query.tour_id
        }
        return params;
    }

    loadRecords(props) {
        this.dispatchLoadRecordsStart();
        this.dispatchClearMessage()

        var _this = this;
        var params = this.extractTableParams(props.dataTable)
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/subchallenge/list_by_challenge?challenge_id='+__params.query.challenge_id+"&_sort_key=id&_sort_dir=asc",
            data:      params,
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatchLoadRecordsSuccess(result.data.records, result.data.total_matched)
                }
                else {
                    _this.dispatchLoadRecordsFail("Fail to load records")
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
    start(e, id, is_disabled) {
        e.preventDefault()
        this.dispatchClearMessage()
        if (is_disabled) {
            return
        }

        var _this = this
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'POST',
            url:       __params.config['api.endpoint'] + '/challenge/start?id=' + id,
            data:      {
                
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    location.reload()
                }
                else {
                    _this.dispatchLoadRecordsFail(result.data.message)
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }
    delete(e, id) {
        e.preventDefault()
        this.dispatchClearMessage()
        var _this = this
        jQuery.ajax({
            headers: { 'TOKEN':   UserService.getCookie('__battlefimanagement_jwt')},
            type:      'POST',
            url:       __params.config['api.endpoint'] + '/challenge/delete?id=' + id,
            data:      {
                
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    location.reload()
                }
                else {
                    _this.dispatchLoadRecordsFail(result.data.message)
                }
            },
            error: function(data) {
                _this.dispatchLoadRecordsFail("Fail to connect to API")
            }
        })
    }

    copylink(id, uuid) {
        var url = `${__params.config['ui.endpoint']}/challenge/get?id=${id}&uuid=${uuid}`
        navigator.clipboard.writeText(url);
        alert("Link copied: " + url);
      }
}

export default Action;
