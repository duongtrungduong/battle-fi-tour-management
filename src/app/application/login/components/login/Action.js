import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Password } from 'library-form-password'
import UserService from 'app-services/userServices'

class Action extends Form.Action {
    constructor(dispatcher, formId='form') {
        super(dispatcher, formId)
        this.id = formId
    }

    initActionHelpers() {
        this._actionHelpers = {
            'email':                  new Textbox.ActionHelper(this.dispatcher, this.formId, 'email'),
            'password':               new Password.ActionHelper(this.dispatcher, this.formId, 'password'),
        }
    }

    load() {
        var _this = this

    }

    validateEmail(email) {
        const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    resetForm() {
        for( var fieldId in this.getActionHelpers() ) {
            this._actionHelper(fieldId).dispatchClearValue()
        }

        this.load()
    }

    submit(form, e) {
        this.dispatchFormLoadStart()

        var _this = this
        var values = this.getValues(form)
        this.clear()
        var hasError = false

        if (values.email != '' && !_this.validateEmail(values.email)) {
            hasError = true
            this.helper('email').dispatchHasError("Email invalid")
        }

        if (values.email == '') {
            hasError = true
            this.helper('email').dispatchHasError('This field can not be empty')
        }

        if (values.password == '') {
            hasError = true
            this.helper('password').dispatchHasError('This field can not be empty')
        }

        if (!hasError) {

            jQuery.ajax({
                type:         'POST',
                url:          __params.config['api.endpoint'] + '/user/login',
                dataType:     'json',
                data:         values,
                success:      function(result) {
                    if (result.status == 'ok') {
                        _this.dispatchFormAddMessage('info', "Login successfully.")
                        _this.dispatchFormSubmitSuccess(values);

                        UserService.logIn(result.data.token, JSON.stringify(result.data.info))
                        console.log(result.data.info)
                        setTimeout( () => {
                            window.location = '/'
                        })
                    }
                    else {
                        _this.dispatchFormAddMessage('error', 'Login failed.')
                        _this.dispatchFormSubmitFail(values)
                    }
                },
                error:        function(data) {
                    _this.dispatchFormAddMessage('error', "Internal server.")
                    _this.dispatchFormSubmitFail(values)
                }
            })  
            
        } else {
            _this.dispatchFormSubmitFail(values)
        }
    }
}

export default Action;
