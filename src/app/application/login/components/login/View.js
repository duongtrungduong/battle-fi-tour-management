import React from 'react'
import { Form } from 'library-form'
import { Textbox } from 'library-form-textbox'
import { Password } from 'library-form-password'
import BlockUi from 'react-block-ui';
import 'react-block-ui/style.css';
import "./style.css"

class View extends Form.View {
    render () {
        
        return (
            <BlockUi tag="div" blocking={this.props.form.get('isBlocked')}>
                <form>
                    <div className="form-group mb-50 position-relative">
                        <label className="text-bold-600">Email <span className="text-danger">*</span></label>
                        <i className="mdi mdi-account ml-3 icons"></i>
                        <Textbox.View
                            data={this.props.form.get('fields').get('email')}
                            action={this.props.action}
                            className="form-control"
                        />
                    </div>
                    <div className="form-group mb-50 position-relative">
                        <label className="text-bold-600">Password <span className="text-danger">*</span></label>
                        <i className="mdi mdi-key ml-3 icons"></i>
                        <Password.View
                            data={this.props.form.get('fields').get('password')}
                            action={this.props.action}
                            className="form-control"
                        />
                    </div>
                    <p className="btn-no-radius btn btn-green glow position-relative w-100 " style={{ cursor: "pointer" }} onClick={(e) => this.props.action.submit(this.props.form)}>Login<i id="icon-arrow" className="bx bx-right-arrow-alt"></i></p>
                    <div>
                        {
                            this.props.form.get('messages').map(function (message, i) {
                                switch (message.type) {
                                    case 'error':
                                        return (<div className="no-border-radius alert alert-danger" role="alert">{message.message}</div>)
                                }
                                return (<div className="no-border-radius alert alert-info" role="alert">{message.message}</div>)
                            })
                        }
                    </div>
                </form>
            </BlockUi>
        )
    }
}

export default View;
