import React from 'react'
import { render } from 'react-dom'

import LoginContainer from '../containers/LoginContainer'
import './dashforge_1_0_0.css'

jQuery(document).ready(function() {
    render(
        <LoginContainer />,
        document.getElementById('root')
    )
});
