import React, { Component } from 'react'
import { Container } from 'flux/utils'
import { Dispatcher } from 'flux'

import FormView from '../components/login/View'
import FormAction from '../components/login/Action'
import FormStore from '../components/login/Store'

const _dispatcher = new Dispatcher()
const _formStore = new FormStore(_dispatcher, 'form')
const _formAction = new FormAction(_dispatcher, 'form')


class UserLoginContainer extends Component {
    static getStores() {
        return [
            _formStore,
        ]
    }

    static calculateState(prevState) {
        return {
            form: _formStore.getState(),
        };
    }

    render() {
        return (
            <div className="section border-bottom" style={{display: 'flex', minHeight: '100vh', background: 'linear-gradient(to bottom, #F3F6F9, #FCFCFC)'}}>
                <div className="container pd-x-0" style={{width: 400, margin: 'auto', background: '#fff' }}>
                    <div className="card" >
                        <div className="card-body">
                            {/* <img width="100%" style={{ padding: 20 }} src="/public/static/battleficms_logo.jpg" /> */}
                            <h5 className="main-title mb-4" style={{ textAlign: 'center' }}>Login to Battlefi</h5>
                            <FormView form={ this.state.form } action={ _formAction }/>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default Container.create(UserLoginContainer);
