
var fs = require("fs");

class Plugin {
    constructor() {}

    build(server) {
        server.serverinfo().registerPluginInfo({
            id: 'battle-fi-management',
            name: 'BattleFi Management',
            group: 'application',
            description: '',
            dependencies: [],
            links: []
        })

        server.webpack().addAlias('app-container',                              __dirname + '/page_container')
        server.webpack().addAlias('app-services',                               __dirname + '/services')
        /***********************************************/
        // open
        server.webpack().addBundle('app-open',                                 __dirname + '/application/open/bundles/open.js')
        server.express().get('/open',                                          server.helper().serveBundlePage('app-open'))

        /*************BACKEND*****************/
        // User
        server.express().get('/user/update',                                    server.helper().serveBundlePage('app-user-update', 'app'))
        // setting
        server.webpack().addBundle('app-home',                              __dirname + '/application/home/bundles/home.js')
        server.express().get('/',                                            server.helper().serveBundlePage('app-home', 'app'))
        
        // Tour
        server.webpack().addBundle('tournament-list',                        __dirname + '/application/tournament/bundles/list.js')
        server.webpack().addBundle('tournament-view',                        __dirname + '/application/tournament/bundles/list_round.js')
        server.webpack().addBundle('tournament-update',                      __dirname + '/application/tournament/bundles/update.js')
        server.webpack().addBundle('tournament-create',                      __dirname + '/application/tournament/bundles/create.js')
        server.webpack().addBundle('tournament-setting',                      __dirname + '/application/tournament/bundles/setting.js')
        server.express().get('/tournament',                                  server.helper().serveBundlePage('tournament-list'))
        server.express().get('/tournament/update',                           server.helper().serveBundlePage('tournament-update'))
        server.express().get('/tournament/view',                             server.helper().serveBundlePage('tournament-view'))
        server.express().get('/tournament/create',                           server.helper().serveBundlePage('tournament-create'))
        server.express().get('/tournament/setting',                          server.helper().serveBundlePage('tournament-setting'))

        // Challenge
        server.webpack().addBundle('challenge-list',                        __dirname + '/application/challenge/bundles/list.js')
        server.webpack().addBundle('challenge-update',                      __dirname + '/application/challenge/bundles/update.js')
        server.webpack().addBundle('challenge-create',                      __dirname + '/application/challenge/bundles/create.js')
        server.webpack().addBundle('challenge-bulk-create',                 __dirname + '/application/challenge/bundles/bulk_create.js')
        server.express().get('/challenge',                                  server.helper().serveBundlePage('challenge-list'))
        server.express().get('/challenge/update',                           server.helper().serveBundlePage('challenge-update'))
        server.express().get('/challenge/create',                           server.helper().serveBundlePage('challenge-create'))
        server.express().get('/challenge/bulk_create',                      server.helper().serveBundlePage('challenge-bulk-create'))
        // Subchallenge
        server.webpack().addBundle('subchallenge-list',                        __dirname + '/application/subchallenge/bundles/list.js')
        server.express().get('/subchallenge',                                  server.helper().serveBundlePage('subchallenge-list'))

        // 404 Page
        server.webpack().addBundle('app-404-page',                                 __dirname + '/application/404/bundles/page_404.js')
        server.express().get('/404',                                           server.helper().serveBundlePage('app-404-page'))

        // Account login
        server.webpack().addBundle('app-account-login',                     __dirname + '/application/login/bundles/login.js')
        server.express().get('/login',                                           server.helper().serveBundlePage('app-account-login'))
        
        server.webpack().addBundle('app-challenge-lucky-spin',                     __dirname + '/application/challenge-lucky-spin/bundles/create.js')
        server.express().get('/challenge/lucky-spin',                                           server.helper().serveBundlePage('app-challenge-lucky-spin'))
        
        // Tournament list request to join
        server.webpack().addBundle('list-of-request-to-join',                        __dirname + '/application/tournament/bundles/list_request_to_join.js')
        server.express().get('/tournament/list_of_request_to_join',                                  server.helper().serveBundlePage('list-of-request-to-join'))

        // Tour party
        server.webpack().addBundle('tournament-party',                        __dirname + '/application/tournament/bundles/list_party.js')
        server.express().get('/tournament/party',                             server.helper().serveBundlePage('tournament-party'))

    }
}

module.exports = Plugin;
