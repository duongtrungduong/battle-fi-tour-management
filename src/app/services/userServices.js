class UserServices {
    setCookie(cname,cvalue,exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires=" + d.toGMTString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path/";
    }
  
    getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }
  
    checkCookie(cname) {
        var ck=this.getCookie(cname);
        if (ck != "") {
            return ck
        } else {
            return false
        }
    }
  
    deleteAllCookies(domain) {
        var cookies = document.cookie.split(";");
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i];
            var eqPos = cookie.indexOf("=");
            var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
            var d = new Date();
            d.setTime(d.getTime() - 1);
            var expires = "expires=" + d.toGMTString();
            console.log(name)
            console.log(expires)
            document.cookie = name + "=;" + expires + ";path=/;domain="+domain;
        }
    }
  
    deleteCookie(cname) {
      var ck=this.getCookie(cname);
      if (ck != "") {
          var d = new Date();
          d.setTime(d.getTime() - 1);
          var expires = "expires=" + d.toGMTString();
          document.cookie = cname + "=;" + expires + ";path=/";
      }
    }
  
    logIn(token, profile, domain="") {
        this.setCookie('__battlefimanagement_jwt', token, 15);
        this.setCookie('__battlefimanagement_profile', profile, 15);
    }
  
    logOut(domain = "") {
        this.deleteAllCookies(domain)
    }
  
    getToken() {
        return this.checkCookie('__battlefimanagement_jwt')
    }
  
    getProfile() {
        let profile = JSON.parse(this.checkCookie('__battlefimanagement_profile'));
  
        return profile
    }
    parseJwt (token) {
        var base64Url = token.split('.')[1];
        var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
        var jsonPayload = decodeURIComponent(window.atob(base64).split('').map(function(c) {
            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
        }).join(''));
    
        return JSON.parse(jsonPayload);
    };

    getPermission() {
        let profile = this.parseJwt(this.checkCookie('__battlefimanagement_jwt'))
        if(profile.permission != undefined){
            return profile.permission;
        }
        return null    
    }

    getId() {
        let profile = this.parseJwt(this.checkCookie('__battlefimanagement_jwt'));
        if(profile.id != undefined){
            return profile.id;
        }
        return null
    }
  
    getUserName() {
        let profile = JSON.parse(this.checkCookie('__battlefimanagement_profile'));
        if(profile.username != undefined){
            return profile.username;
        }
        return this.checkCookie('__battleficms_username')
    }
  
    getEmail() {
        let profile = JSON.parse(this.checkCookie('__battlefimanagement_profile'));
        if(profile.email != undefined){
            return profile.email;
        }
        return this.checkCookie('__battleficms_email')
    }
  
    isLoggedIn() {
        return this.checkCookie('__battlefimanagement_jwt')
    }
  }
  
  export default new UserServices();
  
