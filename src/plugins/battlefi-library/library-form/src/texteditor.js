module.exports = {
	'Texteditor': {
	    StoreHelper:  require('./common/texteditor/StoreHelper'),
	    ActionHelper: require('./common/texteditor/ActionHelper'),
	    View:         require('./common/texteditor/View')
	}
}