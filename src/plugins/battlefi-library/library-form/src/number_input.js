module.exports = {
	'NumberInput': {
	    StoreHelper:  require('./common/number_input/StoreHelper'),
	    ActionHelper: require('./common/number_input/ActionHelper'),
	    View:         require('./common/number_input/View')
	}
}