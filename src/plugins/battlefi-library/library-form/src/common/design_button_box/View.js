import React, {Component} from 'react';

class View extends Component {
    changeField(key, newValue) {
        var value = this.props.data.get('value').toJS()
        value[key] = newValue
        this.props.action.changeValue(
            this.props.data.get('id'),
            value
        );
    }
    render() {
        var _this = this;
        var value = this.props.data.get('value').toJS()
        return (
            <div className={this.props.data.get('error') != '' ? "has-error" : ''}>
                <div className="row">
                    <div className="col-md-3" style={{paddingRight: 2}}>
                        <label>Width</label>
                        <input type="text" className="form-control" value={value.width} onChange={e => _this.changeField("width", e.target.value)}></input>
                    </div>
                    <div className="col-md-3" style={{paddingRight: 2, paddingLeft: 2}}>
                        <label>Height</label>
                        <input type="text" className="form-control" value={value.height} onChange={e => _this.changeField("height", e.target.value)}></input>
                    </div>
                    <div className="col-md-6" style={{paddingLeft: 2}}>
                        <label>Color</label>
                        <input type="text" className="form-control" value={value.color} onChange={e => _this.changeField("color", e.target.value)}></input>
                    </div>
                </div>
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
