import React, {Component} from 'react';

class View extends Component {
    changeValue(e) {
        this.props.action.changeValue(
            this.props.data.get('id'),
            e.target.value
        );
    }
    render() {
        return (
            <div 
                className={this.props.data.get('error') != '' ? "has-error" : ''} 
                style = { this.props.style }>
                <input
                    className={this.props.className ? this.props.className: "form-control"}
                    type="text"
                    id={this.props.data.get('id')}
                    value={this.props.data.get('value')}
                    placeholder={this.props.placeholder}
                    onChange={e => this.changeValue(e)}
                    disabled={this.props.disabled}
                    onBlur={this.props.onBlur}
                    onFocus={this.props.onFocus}
                />
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
