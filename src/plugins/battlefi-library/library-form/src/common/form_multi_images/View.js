import React, {Component} from 'react';
import jQuery from 'jquery';
import {Modal} from 'react-bootstrap';
import moment from 'moment';
import './style.css'

class View extends Component {
    changeValue(e) {
        var _this = this

        var files = e.target.files;
        var value = [];
        for (var i = 0; i < files.length; i++) {
            value.push(files[i])
        }
        var tab = this.props.data.get('tab')

        this.props.action.helper(this.props.data.get('id')).uploadImage(tab, value)

    }

    openModal(index) {
        var _this = this
        var tab = this.props.data.get('tab')
        console.log(index)
        _this.props.action.helper(_this.props.data.get('id')).dispatchOpenModal(tab)
        _this.props.action.helper(_this.props.data.get('id')).dispatchChangeIndex(index)
    }

    closeModal() {
        var _this = this

        _this.props.action.helper(_this.props.data.get('id')).dispatchCloseModal()
    }

    setSelectedImage(image) {
        var _this = this
        var selectedImage = this.props.data.get('selected_image')

        if (image.id === selectedImage.id) {
            _this.props.action.helper(_this.props.data.get('id')).dispatchSetSelectedImage("")
        } else {
            _this.props.action.helper(_this.props.data.get('id')).dispatchSetSelectedImage(image)
        }


    }

    setSelectedTab(value) {
        var _this = this

        _this.props.action.helper(_this.props.data.get('id')).dispatchSetTab(value)
    }

    addImage(image_url) {
        var value = this.props.data.get('value').toJS()
        var index = this.props.data.get('index')
        value[index] = image_url
        this.props.action.helper(this.props.data.get('id')).dispatchChangeValue(value)
        this.props.action.changeValue(this.props.data.get('id'),value)

    }
    addRow(image_url) {
        var value = this.props.data.get('value').toJS()
        value.push("")
        this.props.action.helper(this.props.data.get('id')).dispatchChangeValue(value)
        this.props.action.changeValue(this.props.data.get('id'),value)
    }
    removeRow(index) {
        var value = this.props.data.get('value').toJS()
        value.splice(index,1)
        this.props.action.helper(this.props.data.get('id')).dispatchChangeValue(value)
        this.props.action.changeValue(this.props.data.get('id'),value)
    }

    render() {
        var _this = this
        var images = this.props.data.get('value')

        return (
            <div id="image-picker">
                {
                    images.map(function(img, key) {
                        return (
                            <div>
                                <div
                                    type="submit"
                                    style={{ fontWeight: 500, width: '100%', fontSize: 14 }}
                                    className="btn btn-sm pd-x-15 btn-white"
                                    onClick={e => _this.openModal(key)}>
                                    Chọn ảnh
                                </div>
                                <div className="image-value">
                                    <a href="javascript:;" className="btn-remove" onClick={e => _this.removeRow(key)} style={{}}>
                                        <i className="fas fa-times" style={{"color": "#fff", fontSize: "17px", padding: "4px 6px", backgroundColor: "red"}}></i>
                                    </a>
                                    {
                                        img
                                        ?
                                        <img src={img} style={{ height: '100%' }} />
                                        : null
                                    }
                                </div>
                            </div>
                        )
                    })
                }
                <a href="javascript:;" onClick={e => _this.addRow()} style={{padding: "15px 0px", float:"right", color: "#2CB587"}}> + Thêm ảnh</a>
                { _this.renderModal() }
            </div>
        )
    }

    renderModal() {
        var _this = this

        return (
            <Modal
                id="modal-image-picker"
                style={{opacity:1, padding: 0 }}
                fade={false}
                show={_this.props.data.get('is_show_modal')}
                onHide={() => this.closeModal()}
                dialogClassName="modal-dialog-form"
            >
                <Modal.Header closeButton>
                    <Modal.Title>
                        Ảnh
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body closeButton>
                    { _this.renderForm() }
                </Modal.Body>
            </Modal>
        )
    }

    renderForm() {
        var _this = this
        var tab_list = this.props.data.get('tab_list')
        var tab = this.props.data.get('tab')
        var tab_label = ""
        for (let index = 0; index < tab_list.length; index++) {
            if (tab === tab_list[index].value) {
                tab_label = tab_list[index].label
            }
        }

        var selectedImage = this.props.data.get('selected_image')

        return(
            <div className="image-picker-container" style={{ height: "100%" }}>
                <div className="row" style={{ margin: 0, padding: 0, height: "100%" }}>
                    <div className="col-md-2" style={{ margin: 0, padding: 0, borderRight: '1px solid #ddd' }}>
                        { this.renderTab(tab_list, tab) }
                    </div>
                    <div className="col-md-10" style={{ margin: 0, padding: 0 }}>
                        <div className="row" style={{ margin: 0, padding: 0, minHeight: 600 }}>
                            <div className="col-md-7" style={{ margin: 0, padding: 0, borderRight: '1px solid #ddd' }}>
                                <div style={{ padding: "20px 15px", fontSize: 24, fontWeight: 500, borderBottom: '1px solid #ddd' }}>
                                    { tab_label }
                                </div>
                                { this.renderListImages(selectedImage) }
                                { this.renderMessage() }
                            </div>
                            <div className="col-md-5" style={{ margin: 0, padding: 0 }}>
                                { this.renderView(selectedImage, tab_label) }

                                { selectedImage.id ? this.renderImageInfo(selectedImage) : false }
                            </div>
                        </div>
                        <div className="row" style={{ margin: 0, padding: 0, borderTop: '1px solid #ddd'}}>
                            <div className="col-md-12" style={{ margin: 0, padding: 15 }}>
                                <div className={ selectedImage ? "btn btn-primary" : "btn btn-primary disabled"}
                                    style={{ float: 'right' }}
                                    onClick={ e => _this.addImage(selectedImage.image_url) }>
                                    Thêm
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

    renderTab(tab_list, tab) {
        var _this = this

        return (
            <div>
                <div style={{ padding: 15 }}>
                    <label
                        htmlFor="image-picker-file-input"
                        type="submit"
                        style={{ fontWeight: 500, width: '100%', fontSize: 16 }}
                        className="btn btn-sm pd-x-15 btn-primary">
                        Upload ảnh
                    </label>
                    <input
                        type="file"
                        id={ "image-picker-file-input" }
                        key={ "image-picker-file-input" }
                        accept="image/*"
                        multiple={ this.props.multiple ? this.props.multiple : true }
                        onChange={ e => _this.changeValue(e) }
                        style={{ display: 'none' }}
                    />
                </div>
                <div style={{ fontSize: 20 }}>
                {
                    tab_list.map(function(t, j ) {
                        var className = "image-picker-tab"
                        if (tab === t.value) {
                            className = "image-picker-tab-selected"
                        }
                        return (
                            <div className={className} key={j}
                                onClick={e => _this.setSelectedTab(t.value)}>
                                {t.label}
                            </div>
                        )
                    })
                }
                </div>
            </div>
        )
    }

    renderListImages(selectedImage) {
        var _this = this
        var image_list = this.props.data.get('image_list')

        return (
            <div className="images-list">
                <div className={ "images-container" } >
                    {
                        image_list.size > 0
                        ?
                        image_list.toJS().map(function(img, i) {
                            var className = "image-bounding"
                            if (img.id === selectedImage.id) {
                                className = "image-bounding-selected"
                            }
                            return (
                                <div className={ className } key={i} style={{  }}>
                                    <div className="image-wrapper">
                                        <div className="image" style={{ backgroundImage: `url(${img.image_url})` }}
                                            onClick={ e => _this.setSelectedImage(img) }></div>
                                    </div>
                                </div>
                            )
                        })
                        : null
                    }
                </div>
            </div>
        )
    }

    renderMessage() {
        return (
            <div>
                {
                    this.props.data.get('messages').map(function (message, i) {
                        switch (message.type) {
                            case 'error':
                                return (<div className="alert alert-danger" role="alert">{message.message}</div>)
                        }
                        return (<div className="alert alert-info" role="alert">{message.message}</div>)
                    })
                }
            </div>
        )
    }

    renderView(selectedImage, tab_label) {
        var _this = this

        return(
            <div>
                <div className="image-picker-selected" >
                    <div className="image-bounding"
                        style={{ display: 'flex', justifyContent: 'center', width: 450, height: 300, padding: 15, backgroundColor: "#eee" }}>
                        <img
                            src={ selectedImage.image_url }
                            loading="lazy"
                            style={{ maxWidth: "100%", cursor: 'pointer' }}
                        />
                    </div>
                </div>
                <div style={{ padding: 15, fontSize: 22, borderBottom: '1px solid #ddd' }}>{ tab_label }</div>
            </div>
        )
    }

    renderImageInfo(selectedImage) {
        var _this = this

        return (
            <div style={{ padding: 15, fontSize: 18, wordBreak: 'break-word' }}>
                <div>{ selectedImage.image_name }</div>
                <div><strong>Người đăng:</strong> { selectedImage.teacher_name }</div>
                <div><strong>Ngày đăng:</strong> { moment.utc(selectedImage.created_date).format('DD/MM/YYYY') } </div>
            </div>
        )
    }

}

export default View;
