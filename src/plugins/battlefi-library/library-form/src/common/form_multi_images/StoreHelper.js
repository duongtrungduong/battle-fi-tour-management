import Immutable from 'immutable';
import {Field} from 'library-field'
import Constants from './Constants'


class StoreHelper extends Field.StoreHelper {

    getInitialState() {
        return Immutable.Map({
            'id':       this.fieldId,
            'value':    Immutable.List([]),
            'index':    0,    
            'selected_image': {},
            'image_list':  Immutable.List([]),
            'file_name':    'Chọn file',
            'is_show_modal':  false,
            'tab_list' : [ 
                {"label": "Ảnh của tôi", "value": "my-images"}, 
                {"label": "Thư viện ảnh", "value": "all-images"} 
            ],
            'tab':  'my-images',
            'messages':          Immutable.List([]),
        })
    }

    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case Constants.ACTION_SET_OPEN_MODAL:
                    state = state.set('is_show_modal', true)
                    return state
                case Constants.ACTION_SET_CLOSE_MODAL:
                    state = state.set('is_show_modal', false)
                    return state
                case Constants.ACTION_SET_FILE_NAME:
                    state = state.set('file_name', action.value)
                    return state
                case Constants.ACTION_SET_VALUE:
                    console.log(action.value)
                    state = state.set('value', Immutable.List(action.value))
                    return state
                case Constants.ACTION_SET_TAB:
                    state = state.set('tab', action.value)
                    return state
                case Constants.ACTION_SET_IMAGE_LIST:
                    state = state.set('image_list', Immutable.fromJS(action.value))
                    return state
                case Constants.ACTION_SET_SELECTED_IMAGE:
                    state = state.set('selected_image', action.value)
                    return state
                case Constants.ACTION_ADD_MESSAGE:
                    var messages = state.get('messages');
                    messages = messages.push({'type': messageType, 'message': message});
                    state = state.set('messages', messages);
                    return state;
                case Constants.ACTION_CLEAR_MESSAGES:
                    state = state.set('messages', Immutable.List([]))
                    return state;
                case Constants.ACTION_CHANGE_INDEX:
                    state = state.set('index', action.index)
                    return state;
                default:
                    state = super.reduce(state, action)
            }
        }

        return state
    }
}

export default StoreHelper;
