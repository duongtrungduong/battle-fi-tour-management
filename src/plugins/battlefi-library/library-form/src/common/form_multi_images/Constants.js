module.exports = {
    ACTION_SET_IMAGE_LIST       : 'set-image-list',
    ACTION_SET_VALUE            : 'set-value',
    ACTION_SET_FILE_NAME        : 'set-file-name',
    ACTION_SET_TAB              : 'set-tab',
    ACTION_SET_OPEN_MODAL       : 'open-modal',
    ACTION_SET_CLOSE_MODAL      : 'close-modal',
    ACTION_SET_SET_MESSAGE      : 'set-message',
    ACTION_SET_SELECTED_IMAGE   : 'set-selected-image',
    ACTION_ADD_MESSAGE          : 'add-message',
    ACTION_CLEAR_MESSAGES       : 'clear-messages',
    ACTION_CHANGE_INDEX         : 'change-index-images',
}
