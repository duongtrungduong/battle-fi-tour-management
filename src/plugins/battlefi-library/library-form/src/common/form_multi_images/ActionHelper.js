import Immutable from 'immutable';
import {Field} from 'library-field'
import Constants from './Constants'


class ActionHelper extends Field.ActionHelper {

    load(tab) {
        if (tab === "my-images") {
            this.loadMyImages()
        } else if (tab === "all-images") {
            this.loadAllImages()
        }
    }

    dispatchOpenModal(tab) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_OPEN_MODAL,
        })

        this.load(tab)
    }

    dispatchCloseModal() {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_CLOSE_MODAL,
        })

        this.dispatchSetSelectedImage("")
    }

    loadMyImages() {
        var _this = this

        jQuery.ajax({
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/web/teacher_image/list',
            data:      {
                token: __jwt,
                teacher_id: __profile.id
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatchSetImageList(result.data.records)
                }
                else {
                    _this.dispatchAddMessage("error", "Không thể tải thông tin")
                }
            },
            error: function(data) {
                _this.dispatchAddMessage("error", "Không thể kết nối")
            }
        })
    }

    loadAllImages() {
        var _this = this

        jQuery.ajax({
            type:      'GET',
            url:       __params.config['api.endpoint'] + '/web/teacher_image/list',
            data:      {
                token: __jwt,
                organization_id: __profile.organization_id,
            },
            dataType:  'json',
            success:   function(result) {
                if (result.status == 'ok') {
                    _this.dispatchSetImageList(result.data.records)
                }
                else {
                    _this.dispatchAddMessage("error", "Không thể tải thông tin")
                }
            },
            error: function(data) {
                _this.dispatchAddMessage("error", "Không thể kết nối")
            }
        })
    }

    uploadImage(tab, values) {
        var _this = this

        var formData = new FormData();
        if (values) {
            values.forEach((file) => {
                formData.append('file', file)
            })
        }
        formData.append('teacher_id', __profile.id);
        formData.append('organization_id', __profile.organization_id);

        jQuery.ajax({
            type:         'POST',
            url:          __params.config['api.endpoint'] + '/web/teacher_image/create',
            dataType:     'json',
            data:         formData,
            processData:    false,
            contentType:    false,
            success:      function(result) {
                if (result.status == 'ok') {
                    _this.dispatchAddMessage("ok", result.data.message)
                    _this.load(tab)
                }
                else {
                    _this.dispatchAddMessage("error", result.data.message)
                }
            },
            error:        function(data) {
                _this.dispatchAddMessage("error", "Không thể kết nối")
            }
        })
            
    }

    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_VALUE,
            value:        value
        })

        this.dispatchCloseModal()
    }

    dispatchSetImageList(value) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_IMAGE_LIST,
            value:        value
        })
    }

    dispatchSetSelectedImage(value) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_SELECTED_IMAGE,
            value:        value
        })
    }

    dispatchSetTab(value) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        Constants.ACTION_SET_TAB,
            value:        value
        })

        this.load(value)
    }

    dispatchAddMessage(messageType, message) {
        this.dispatcher.dispatch({
            formId:        this.formId,
            type:          Constants.ACTION_ADD_MESSAGE,
            messageType:   messageType,
            message:       message
        })

        var _this = this
        setTimeout(() => {
            _this.dispatchClearMessages()
        }, 1500);
    }

    dispatchClearMessages() {
        this.dispatcher.dispatch({
            formId:        this.formId,
            type:          Constants.ACTION_CLEAR_MESSAGES,
        })
    }
    dispatchChangeIndex(index) {
        this.dispatcher.dispatch({
            formId:        this.formId,
            fieldId:       this.fieldId,
            type:           'field-action',
            subtype:       Constants.ACTION_CHANGE_INDEX,
            index:         index 
        })
    }
}

export default ActionHelper;
