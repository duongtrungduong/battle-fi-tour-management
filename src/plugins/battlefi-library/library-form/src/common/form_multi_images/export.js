module.exports = {
	'ImagePicker': {
		StoreHelper:  require('./StoreHelper'),
		ActionHelper: require('./ActionHelper'),
		View:         require('./View'),
		Constants:    require('./Constants')
	}
}
