import React, {Component} from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, convertFromRaw, convertFromHTML,ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css"
import "./style.css"
class View extends Component {
    state = {
        editorState: EditorState.createEmpty(),
        isLoadDefault: true
    }
    onEditorStateChange(editorState){
        this.props.action.changeValue(
            this.props.data.get('id'),
            draftToHtml(convertToRaw(editorState.getCurrentContent()))
        );
        this.setState({
            editorState: editorState,
            isLoadDefault: false
        });
    };
    uploadCallback = (file, callback) =>  {
        console.log(file)
        var formData = new FormData();
        formData.append('file', file);
        jQuery.ajax({
            type:         'POST',
            url:          __params.config['api.endpoint'] + '/task_flag/update?id='+ __params.query.id,
            dataType:     'json',
            data:           formData,
            processData:    false,
            contentType:    false,
            success:      function(result) {
                if (result.status == 'ok') {
                }
            },
            error:        function(data) {
            }
        })
        return new Promise((resolve, reject) => {
            const reader = new window.FileReader();
            reader.onloadend = async () => {
                const form_data = new FormData();
                form_data.append("file", file);
                const res = await uploadFile(form_data);
                setValue("thumbnail", res.data);
                resolve({ data: { link: process.env.REACT_APP_API + res.data } });
            };
            reader.readAsDataURL(file);
        });
    }
    render() {
        var editorState = this.state.editorState
        var _this = this
        if (this.state.isLoadDefault) {
            editorState = EditorState.createWithContent(
                ContentState.createFromBlockArray(
                    htmlToDraft(this.props.data.get("value"))
                )
            )
        }
        const config = {
            image: { uploadCallback: this.uploadCallback },
        };
        return (
            <div>
                <Editor
                    editorState={editorState}
                    toolbar={config}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor custom-css"
                    onEditorStateChange={e => _this.onEditorStateChange(e)}
                />
            </div>
        );
    }
}

export default View;
