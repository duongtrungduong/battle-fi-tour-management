import Immutable from 'immutable';
import _StoreHelper from './../../field/StoreHelper'

class StoreHelper extends _StoreHelper {
    getInitialState() {
        var state = super.getInitialState()
        state = state.set("isLoad", true)
        return state
    }

    reduce(state, action) {
        state = super.reduce(state, action)
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case "is-load":
                    state = state.set("isLoad", action.isLoad)
                    return state

            }
        }
        return state;
    }
}

export default StoreHelper;
