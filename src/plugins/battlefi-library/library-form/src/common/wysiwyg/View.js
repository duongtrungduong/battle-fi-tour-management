import React, {Component} from 'react';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw, convertFromRaw, convertFromHTML,ContentState } from 'draft-js';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css"
import "./style.css"
class View extends Component {
    state = {
        editorState: EditorState.createEmpty(),
        isLoadDefault: true
    }
    onEditorStateChange(editorState){
        this.props.action.changeValue(
            this.props.data.get('id'),
            draftToHtml(convertToRaw(editorState.getCurrentContent()))
        );
        this.setState({
            editorState: editorState,
            isLoadDefault: false
        });
    };
    render() {
        var editorState = this.state.editorState
        var _this = this
        if (this.state.isLoadDefault) {
            editorState = EditorState.createWithContent(
                ContentState.createFromBlockArray(
                    htmlToDraft(this.props.data.get("value"))
                )
            )
        }
        return (
            <div>
                <Editor
                    editorState={editorState}
                    wrapperClassName="demo-wrapper"
                    editorClassName="demo-editor custom-css"
                    onEditorStateChange={e => _this.onEditorStateChange(e)}
                    toolbar={{
                        options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign', 'history', "image", "link"],
                        inline: { inDropdown: true },
                        list: { inDropdown: false },
                        textAlign: { inDropdown: false },
                        link: { inDropdown: true },
                        history: { inDropdown: true },
                    }}
                />
            </div>
        );
    }
}

export default View;
