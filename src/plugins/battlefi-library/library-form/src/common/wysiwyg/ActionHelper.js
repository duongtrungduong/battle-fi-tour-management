import Immutable from 'immutable';
import _ActionHelper from './../../field/ActionHelper'

class ActionHelper extends _ActionHelper {

    dispatchIsLoad(isLoad) {
        this.dispatcher.dispatch({
            formId:         this.formId,
            fieldId:        this.fieldId,
            type:           'field-action',
            subtype:        "is-load",
            isLoad:          isLoad
        })
    }
}

export default ActionHelper;
