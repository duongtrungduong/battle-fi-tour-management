import React, {Component} from 'react';

class View extends Component {
    changeValue(e) {
        var files = e.target.files;
        var value = [];
        for (var i = 0; i < files.length; i++) {
            value.push(files[i])
        }

        this.props.action.changeValue(
            this.props.data.get('id'),
            value
        );

    }
    render() {
        return (
            <div className={this.props.data.get('error') != '' ? "has-error" : ''}>
                <input
                    className={this.props.class ? this.props.class: "form-control"}
                    type="file"
                    id={this.props.data.get('id')}
                    onChange={e => this.changeValue(e)}
                    accept={this.props.accept}
                    multiple={this.props.multiple}
                    style={this.props.style}
                />
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;