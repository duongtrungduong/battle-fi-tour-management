import Immutable from 'immutable';
import _StoreHelper from './../../field/StoreHelper'
import Constants from './Constants'

class StoreHelper extends _StoreHelper {
    getInitialState() {
        return Immutable.Map({
            'id':       this.fieldId,
            'value':    Immutable.Map({
                "font_size": "",
                "font_family": "",
                "font_color": "",
                "font_style": "",
            }),
            'error':    '',
        })
    }
    reduceFieldChangeValue(state, value) {
        return state.set('value', Immutable.Map(value))
    }
    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                default:
                    state = super.reduce(state, action)
            }
        }

        return state
    }
}

export default StoreHelper;
