import React, {Component} from 'react';

class View extends Component {
    changeField(key, newValue) {
        var value = this.props.data.get('value').toJS()
        value[key] = newValue
        this.props.action.changeValue(
            this.props.data.get('id'),
            value
        );
    }
    render() {
        var _this = this;
        var value = this.props.data.get('value').toJS()
        return (
            <div className={this.props.data.get('error') != '' ? "has-error" : ''}>
                <div className="row">
                    <div className="col-md-4" style={{paddingRight: 2}}>
                        <label>Font</label>
                        <select className="form-control" value={value.font_family} onChange={e => _this.changeField("font_family", e.target.value)}>
                            <option></option>
                            <option value="Arial">Arial</option>
                            <option value="monospace">Monospace</option>
                            <option value="IBM Plex Sans">IBM Plex Sans</option>
                        </select>
                    </div>
                    <div className="col-md-2" style={{paddingRight: 2, paddingLeft: 2}}>
                        <label>Size</label>
                        <input type="text" className="form-control" value={value.font_size} onChange={e => _this.changeField("font_size", e.target.value)}></input>
                    </div>
                    <div className="col-md-2" style={{paddingRight: 2, paddingLeft: 2}}>
                        <label>Effect</label>
                        <select className="form-control" value={value.font_style} onChange={e => _this.changeField("font_style", e.target.value)}>
                            <option></option>
                            <option value="Normal">Normal</option>
                            <option value="Oblique">Oblique</option>
                            <option value="Italic">Italic</option>
                        </select>
                    </div>
                    <div className="col-md-4" style={{paddingLeft: 2}}>
                        <label>Color</label>
                        <input type="text" className="form-control" value={value.font_color} onChange={e => _this.changeField("font_color", e.target.value)}></input>
                    </div>
                </div>
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
