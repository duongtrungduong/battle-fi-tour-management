import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment"
class View extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(value) {
        this.props.action.changeValue(
            this.props.data.get('id'),
            value
        );
    }

    render() {
        return (
            <div className={this.props.data.get('error') != '' ? "has-error" : ''} style={{ width: 190}}>
                <DatePicker
                    className="form-control"
                    id={this.props.data.get('id')}
                    onChange={this.onChange}
                    selected={this.props.data.get('value')}
                    dateFormat={this.props.dateFormat ? this.props.dateFormat : "dd/MM/yyyy"}
                    showTimeSelectOnly={this.props.timeOnly}
                    showTimeSelect={this.props.showTimeSelect}
                    timeCaption={this.timeCaption}
                />
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
