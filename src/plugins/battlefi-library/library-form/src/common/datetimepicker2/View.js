import React, {Component} from 'react';
import moment from 'moment';
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './style.css'

class View extends Component {
    constructor(props) {
        super(props);

        this.onChange = this.onChange.bind(this);
    }

    onChange(value) {
        console.log(value)
        this.props.action.changeValue(
            this.props.data.get('id'),
            value
        );
    }

    render() {
        return (
            <div className={this.props.data.get('error') != '' ? "has-error datepicker-layout" : 'datepicker-layout'}>
                <DatePicker
                    id={this.props.data.get('id')}
                    selected={this.props.data.get('value')}
                    onChange={this.onChange}
                    showTimeSelect={false}
                    timeFormat="HH:mm"
                    timeIntervals={1}
                    dateFormat='dd/MM/yyyy'
                    className={"form-control"}
                />
                <br/>
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
