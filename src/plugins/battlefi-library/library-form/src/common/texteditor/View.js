import React, {Component} from 'react';
import FroalaEditor from 'react-froala-wysiwyg'
import 'froala-editor/css/froala_style.min.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
// Require Editor JS files.
import "froala-editor/js/plugins.pkgd.min.js";
// Require Editor CSS files.
import "froala-editor/css/third_party/embedly.min.css";

class View extends Component {
    // constructor () {
    //     super();

    //     // this.handleModelChange = this.handleModelChange.bind(this);
    //     // this.state = {
    //     //   model: ''
    //     // };
    //   }
      componentWillMount() {
          this.setState({
            model: this.props.data.get('id')
          });

    }
      handleModelChange(model) {
        
        // this.setState({
        //   model: model
        // });
        // this.props.action.changeValue(
        //   this.props.data.get('id'),
        //   this.state.model
        // );
      }
    
    
    // changeValue(e) {
    //     this.props.action.changeValue(
    //         this.props.data.get('id'),
    //         this.state.model
    //     );

    // }
    
    render() {
        console.log('--', this.props.data.get('value'))
        return (
            <div>
                <textarea
                    className={this.props.class ? this.props.class: "form-control"}
                    id={this.props.data.get('id')}
                    type="text"
                    rows={5}
                    value={this.props.data.get('value')}
                    onChange={e => this.changeValue(e)}
                    onBlur={this.props.onBlur}
                    onFocus={this.props.onFocus}
                />
                <FroalaEditor
                    tag='textarea'
                    config={{ attribution: false, height: 300, charCounterCount: false}}
                    onModelChange={e => this.handleModelChange(e)}
                    model={this.state.model}
                />
            </div>
        )
    }
}

export default View;
