import Immutable from 'immutable';
import _ActionHelper from './../../field/ActionHelper'

class ActionHelper extends _ActionHelper {
    dispatchChangeValue(value) {
        this.dispatcher.dispatch({
            formId:                     this.formId,
            fieldId:                    this.fieldId,
            type:                       'field-action',
            subtype:                    'set-value',
            value:                      value
        })
    }
}

export default ActionHelper;
