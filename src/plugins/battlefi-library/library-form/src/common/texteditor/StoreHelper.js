import Immutable from 'immutable';
import _StoreHelper from './../../field/StoreHelper'

class StoreHelper extends _StoreHelper {
    getInitialState() {
        return Immutable.Map({
            'id':               this.fieldId,
            'value':           '',
            'error':            '',
        })
    }

    reduceFieldClearValue(state) {
        return state.set('value', '')
    }

    reduceFieldChangeValue(state, value) {
        return state.set('value',value)
    }

    reduce(state, action) {
        if (action.fieldId == this.fieldId) {
            switch (action.subtype) {
                case 'set-value':
                    state = state.set('value', action.value)
                    return state
                default:
                    state = super.reduce(state, action)
            }
        }
        return state
    }
}

export default StoreHelper;
