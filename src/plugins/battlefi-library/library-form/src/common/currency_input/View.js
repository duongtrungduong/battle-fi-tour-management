import React, {Component} from 'react';
import CurrencyFormat from 'react-currency-format';

class View extends Component {
    changeValue(values) {
        const {formattedValue, value} = values;
        this.props.action.changeValue(
            this.props.data.get('id'),
            formattedValue
        );
    }
    render() {
        return (
            <div 
                className={this.props.data.get('error') != '' ? "has-error" : ''} 
                style = { this.props.style }>
                <CurrencyFormat 
                    className={this.props.className ? this.props.className: "form-control"}
                    value={this.props.data.get('value')}
                    displayType={'input'}
                    thousandSeparator={this.props.thousandSeparator ? this.props.thousandSeparator: true}
                    prefix={this.props.prefix ? this.props.prefix: ''}
                    onValueChange={ (values) => this.changeValue(values) }
                />
                <span className="help-block" style={{ color: 'red'}}>{this.props.data.get('error')}</span>
            </div>
        )
    }
}

export default View;
