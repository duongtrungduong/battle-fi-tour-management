module.exports = {
	'MultiImages': {
	    StoreHelper:  require('./common/form_multi_images/StoreHelper'),
	    ActionHelper: require('./common/form_multi_images/ActionHelper'),
	    View:         require('./common/form_multi_images/View')
	}
}
