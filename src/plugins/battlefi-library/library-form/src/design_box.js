module.exports = {
	'DesignBox': {
	    StoreHelper:  require('./common/design_box/StoreHelper'),
	    ActionHelper: require('./common/design_box/ActionHelper'),
	    View:         require('./common/design_box/View'),
	    Constants:    require('./common/design_box/Constants')
	}
}