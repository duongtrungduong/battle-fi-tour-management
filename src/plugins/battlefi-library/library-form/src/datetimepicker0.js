module.exports = {
	'DatetimePicker': {
	    StoreHelper:  require('./common/datetimepicker/StoreHelper'),
	    ActionHelper: require('./common/datetimepicker/ActionHelper'),
	    View:         require('./common/datetimepicker/View')
	}
}