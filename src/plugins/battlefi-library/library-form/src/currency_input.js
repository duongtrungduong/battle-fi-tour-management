module.exports = {
	'CurrencyInput': {
	    StoreHelper:  require('./common/currency_input/StoreHelper'),
	    ActionHelper: require('./common/currency_input/ActionHelper'),
	    View:         require('./common/currency_input/View')
	}
}