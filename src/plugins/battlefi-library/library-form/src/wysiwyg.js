module.exports = {
	'Wysiwyg': {
	    StoreHelper:  require('./common/wysiwyg/StoreHelper'),
	    ActionHelper: require('./common/wysiwyg/ActionHelper'),
	    View:         require('./common/wysiwyg/View')
	}
}