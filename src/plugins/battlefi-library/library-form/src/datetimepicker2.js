module.exports = {
	'DatetimePicker2': {
	    StoreHelper:  require('./common/datetimepicker2/StoreHelper'),
	    ActionHelper: require('./common/datetimepicker2/ActionHelper'),
	    View:         require('./common/datetimepicker2/View')
	}
}
