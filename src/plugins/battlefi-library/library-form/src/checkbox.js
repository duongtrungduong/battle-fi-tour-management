module.exports = {
	'Checkbox': {
	    StoreHelper:  require('./common/checkbox/StoreHelper'),
	    ActionHelper: require('./common/checkbox/ActionHelper'),
	    View:         require('./common/checkbox/View')
	}
}