module.exports = {
	'Password': {
	    StoreHelper:  require('./common/password/StoreHelper'),
	    ActionHelper: require('./common/password/ActionHelper'),
	    View:         require('./common/password/View')
	}
}