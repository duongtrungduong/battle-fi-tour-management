module.exports = {
	'ImageUpload': {
	    StoreHelper:  require('./common/image_upload/StoreHelper'),
	    ActionHelper: require('./common/image_upload/ActionHelper'),
	    View:         require('./common/image_upload/View'),
	    Constants:    require('./common/image_upload/Constants')
	}
}