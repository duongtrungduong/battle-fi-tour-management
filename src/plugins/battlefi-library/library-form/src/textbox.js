module.exports = {
	'Textbox': {
	    StoreHelper:  require('./common/textbox/StoreHelper'),
	    ActionHelper: require('./common/textbox/ActionHelper'),
	    View:         require('./common/textbox/View')
	}
}