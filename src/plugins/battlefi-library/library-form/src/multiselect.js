module.exports = {
	'MultiSelect': {
	    StoreHelper:  require('./common/multiselect/StoreHelper'),
	    ActionHelper: require('./common/multiselect/ActionHelper'),
	    View:         require('./common/multiselect/View'),
	    Constants:    require('./common/multiselect/Constants')
	}
}