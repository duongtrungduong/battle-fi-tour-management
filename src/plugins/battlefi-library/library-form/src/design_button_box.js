module.exports = {
	'DesignButtonBox': {
	    StoreHelper:  require('./common/design_button_box/StoreHelper'),
	    ActionHelper: require('./common/design_button_box/ActionHelper'),
	    View:         require('./common/design_button_box/View'),
	    Constants:    require('./common/design_button_box/Constants')
	}
}