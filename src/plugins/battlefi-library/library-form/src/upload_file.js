module.exports = {
	'UploadFile': {
	    StoreHelper:  require('./common/upload_file/StoreHelper'),
	    ActionHelper: require('./common/upload_file/ActionHelper'),
	    View:         require('./common/upload_file/View')
	}
}