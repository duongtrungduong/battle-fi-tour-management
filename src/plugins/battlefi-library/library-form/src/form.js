module.exports = {
	'Form': {
	    Action:     require('./form/Action'),
	    Store:      require('./form/Store'),
	    View:       require('./form/View')
	}
}