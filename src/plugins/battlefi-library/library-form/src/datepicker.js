module.exports = {
	'DatePicker': {
	    StoreHelper:  require('./common/datepicker/StoreHelper'),
	    ActionHelper: require('./common/datepicker/ActionHelper'),
	    View:         require('./common/datepicker/View')
	}
}