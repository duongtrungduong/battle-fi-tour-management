module.exports = {
	'Textarea': {
	    StoreHelper:  require('./common/textarea/StoreHelper'),
	    ActionHelper: require('./common/textarea/ActionHelper'),
	    View:         require('./common/textarea/View')
	}
}