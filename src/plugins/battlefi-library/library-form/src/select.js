module.exports = {
	'Select': {
	    StoreHelper:  require('./common/select/StoreHelper'),
	    ActionHelper: require('./common/select/ActionHelper'),
	    View:         require('./common/select/View'),
	    Constants:    require('./common/select/Constants')
	}
}