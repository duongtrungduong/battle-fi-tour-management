module.exports = {
	'UploadFileCustom': {
	    StoreHelper:  require('./common/upload_file_custom/StoreHelper'),
	    ActionHelper: require('./common/upload_file_custom/ActionHelper'),
	    View:         require('./common/upload_file_custom/View')
	}
}
