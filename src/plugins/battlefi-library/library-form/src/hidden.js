module.exports = {
	'Hidden': {
	    StoreHelper:  require('./common/hidden/StoreHelper'),
	    ActionHelper: require('./common/hidden/ActionHelper'),
	    View:         require('./common/hidden/View')
	}
}