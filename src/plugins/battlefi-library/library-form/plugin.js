
class Plugin {
    constructor() {
    }

    build(server) {
        server.serverinfo().registerPluginInfo({
            id:            'library-form',
            name:          'Library Form',
            group:         'library',
            description:   'Library Form, version 0.0.1',
            dependencies:  [],
            links:         []
        })

        //server.webpack().addAlias('library-form', __dirname + '/export.js')

        server.webpack().addAlias('library-form',                   __dirname + '/src/form.js')
        server.webpack().addAlias('library-field',                  __dirname + '/src/field.js')
        server.webpack().addAlias('library-form-select',            __dirname + '/src/select.js')
        server.webpack().addAlias('library-form-checkbox',          __dirname + '/src/checkbox.js')
        server.webpack().addAlias('library-form-datepicker',        __dirname + '/src/datepicker.js')
        server.webpack().addAlias('library-form-datetimepicker',    __dirname + '/src/datetimepicker.js')
        server.webpack().addAlias('library-form-datetimepicker2',    __dirname + '/src/datetimepicker2.js')
        server.webpack().addAlias('library-form-number-input',      __dirname + '/src/number_input.js')
        server.webpack().addAlias('library-form-password',          __dirname + '/src/password.js')
        server.webpack().addAlias('library-form-textarea',          __dirname + '/src/textarea.js')
        server.webpack().addAlias('library-form-textbox',           __dirname + '/src/textbox.js')
        server.webpack().addAlias('library-form-upload-file',       __dirname + '/src/upload_file.js')
        server.webpack().addAlias('library-form-upload-file-custom',       __dirname + '/src/upload_file_custom.js')
        server.webpack().addAlias('library-form-currency-input',       __dirname + '/src/currency_input.js')

        server.webpack().addAlias('library-form-hidden',            __dirname + '/src/hidden.js')
        server.webpack().addAlias('library-form-image-upload',      __dirname + '/src/image_upload.js')
        server.webpack().addAlias('library-form-multiselect',       __dirname + '/src/multiselect.js')
        server.webpack().addAlias('library-form-design-box',        __dirname + '/src/design_box.js')
        server.webpack().addAlias('library-form-design-button-box',        __dirname + '/src/design_button_box.js')
        server.webpack().addAlias('library-form-multi-images',       __dirname + '/src/form_multi_images.js')
        server.webpack().addAlias('library-wysiwyg',                 __dirname + '/src/wysiwyg.js')

        server.webpack().addAlias('library-texteditor',           __dirname + '/src/texteditor.js')

    }
}

module.exports = Plugin;
