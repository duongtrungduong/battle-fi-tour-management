const REACT_APP_ENV = process.env.REACT_APP_ENV.trim().toLowerCase()

if (REACT_APP_ENV == 'dev')  {
    module.exports = {
        'env':                              'dev',
        'appTitle':                         'BF Management (dev)',
        'static.url':                       '/public/static',
        'static.dir':                       __dirname + '/../static',
        'api.endpoint':                     'http://127.0.0.1:8471/management',
        'ui.endpoint':                      'http://127.0.0.1:3000',

    }
}
else if (REACT_APP_ENV == 'prod') {
    module.exports = {
        'env':                              'prod',
        'appTitle':                         'BF Management',
        'static.url':                       '/public/static',
        'static.dir':                       __dirname + '/../static',
        'api.endpoint':                     'https://api.battlefi.io/management',
        'ui.endpoint':                      'https://alpha.battlefi.io',
    }
}
else if (REACT_APP_ENV == 'staging') {
    module.exports = {
        'env':                              'staging',
        'appTitle':                         'BF Management',
        'static.url':                       '/public/static',
        'static.dir':                       __dirname + '/../static',
        'api.endpoint':                     'https://staging-api.battlefi.io/management',
        'ui.endpoint':                      'https://staging2.battlefi.io',
    }
}

