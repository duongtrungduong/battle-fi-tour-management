const REACT_APP_ENV = process.env.REACT_APP_ENV.trim().toLowerCase()

const webpack = require('webpack')
const CompressionPlugin = require("compression-webpack-plugin");
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')


class ServerWebpackInfo {
    constructor(server) {
        this.alias = {}
        this.bundles = {}
        this.server = server;
    }

    addAlias(id, alias) {
        this.alias[id] = alias;
    }
    addBundle(id, bundle) {
        this.bundles[id] = bundle;
    }

    getAlias() {
        return this.alias;
    }
    getBundles() {
        return this.bundles;
    }

    getPlugins(){
        this.addBundle('vendor', [
            'jquery',
            'flux',
            'immutable',
            'moment',
            'uuid',
            'react',
            'react-dom',
            'react-block-ui',
            'react-bootstrap',
            'react-modal',
            'react-moment',
            'library-data-table',
            'library-form',
            'library-field',
            'library-form-select',
            'library-form-checkbox',
            'library-form-datepicker',
            'library-form-datetimepicker',
            'library-form-password',
            'library-form-textarea',
            'library-form-textbox',
            'library-form-upload-file',
            'library-form-upload-file-custom',
            'library-form-hidden',
            'library-form-image-upload',
            'library-form-multiselect',
            'react-brackets',
            'winwheel',
            'react-wheel-of-prizes',
            'react-froala-wysiwyg',
            'library-texteditor'
        ])

        const plugins = [
            new webpack.optimize.CommonsChunkPlugin({
               name: 'vendor',
                // minChunks: Infinity
            }),

            new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
            new webpack.optimize.ModuleConcatenationPlugin(),

            new webpack.ProvidePlugin({
                $: "jquery",
                jQuery: "jquery"
            })
        ]


        if(REACT_APP_ENV == 'prod' || REACT_APP_ENV == 'test'){
            /*plugins.push(
                new CompressionPlugin({
                    asset: '[path].gz[query]',
                    algorithm: 'gzip',
                    test: /\.js$|\.css$|\.html$|\.eot?.+$|\.ttf?.+$|\.woff?.+$|\.svg?.+$/,
                    threshold: 10240,
                    minRatio: 0.8
                })
            )*/
            plugins.push(
                new UglifyJsPlugin({
                    uglifyOptions: {
                        output: {
                            comments: false,
                            beautify: false,
                        }
                    }
                })
            )
        }

        return plugins
    }

    getConfig() {
        return {
            entry:   this.getBundles(),
            output: {
                path: this.server.config().get('webpack.build_dir', '/tmp'),
                filename: '[name]-bundle.js',
                chunkFilename: '[name]-chunk.js',
                publicPath: '/public/build/'
            },
            module: {
                loaders: [
                    {
                        test: /\.js$/,
                        exclude: /node_modules/,
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.css$/,
                        loader: "style-loader!css-loader"
                    },
                    {
                        test: /\.(svg|png|jpe?g|gif)(\?\S*)?$/,
                        loader: 'url?limit=100000&name=img/[name].[ext]'
                    },
                    {
                        test: /\.(eot|woff|woff2|ttf)(\?\S*)?$/,
                        loader: 'url?limit=100000&name=fonts/[name].[ext]'
                    }
                ]
            },

            resolve: {
                alias: this.getAlias(),
                extensions: ['.js', '.json']
            },

            //devtool: "source-map",
            plugins: this.getPlugins()
        }
    }

}

module.exports = ServerWebpackInfo
