const express = require('express');
var cookieParser = require('cookie-parser')
var fs = require("fs");


var jsonwebtoken = require('jsonwebtoken');

const jwt_secret = "DD52AFE1658490C7D5824E0E61F915D4AB104452"
class ServerHelper {
    constructor(server) {
        this.server = server
    }

    serveBundlePage(bundleId, pageType='front') {
        var _this = this;
        return function(req, res) {
            var params = {};
            params = {
                query:      req.query,
                config:     _this.server.config().all()
            }
            params.config['mode'] = 'edit'
            var applicationTitle = _this.server.config().get('appTitle', '')

            var bundleContent = fs.readFileSync('/tmp/build/' + bundleId + '-bundle.js', 'utf8');

            var jwt = ''
            var profile = {}
            if (req.url != "/404" && req.url != "/_debug" && req._parsedUrl.pathname != "/open" && req._parsedUrl.pathname != "/login") {
                if( req.cookies['__battlefimanagement_jwt'] != undefined) {
                    jwt = req.cookies['__battlefimanagement_jwt']
                }
    
                try {
                    profile = jsonwebtoken.verify(jwt, jwt_secret )
                } catch (err) {
                    console.log(err.message)
                    res.redirect("/login");
                }
            }
            
            res.send(`
                <!DOCTYPE html>
                <html lang="en">
                    <head>
                        <meta charset="UTF-8">
                        <title>` + applicationTitle + `</title>
                        <meta http-equiv="X-UA-Compatible" content="IE=edge">
                        <meta content="width=device-width, initial-scale=1" name="viewport" />
                        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet"/>
                        <script type="text/javascript">
                            var __params = ` + JSON.stringify(params) + `;
                        </script>

                        <script type="text/javascript">

                    </script>
                    </head>
                    <body class="page-profile">
                        <div id="root"></div>
                        <script type="text/javascript" src="/public/build/vendor-bundle.js?v=` + params.config.version + `"></script>
                        <script defer="defer" async="async" type="text/javascript" src="/public/build/` + bundleId + `-bundle.js?v=` + params.config.version + `" charset="utf-8"></script>
                        <script src="http://cdnjs.cloudflare.com/ajax/libs/gsap/latest/TweenMax.min.js"></script>
                    </body>
                </html>
            `)
        }
    }

    serveStatic(url, staticDir) {
        if (this.server.config().get('env') == 'dev') {
            this.server.express().use(url, express.static(staticDir, { maxAge: 0 })).use(cookieParser());
        }
        else if (this.server.config().get('env') == 'staging') { 
            this.server.express().use(url, express.static(staticDir, { maxAge: 60*60*4 })).use(cookieParser());
        }
        else if (this.server.config().get('env') == 'prod') { 
            this.server.express().use(url, express.static(staticDir, { maxAge: 60*60*4 })).use(cookieParser());
        }
    }

}

module.exports = ServerHelper
